<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use illuminate\notifications\notifiable;



class Associate extends Model
{
    use  notifiable;

    /**
     * the attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * the attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];



}
