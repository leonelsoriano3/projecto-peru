<?php
/**
 * Created by PhpStorm.
 * User: leonel
 * Date: 20/07/17
 * Time: 10:24 PM
 */

namespace App\Classes;


/**
 * Class GenerateInitAcountToken
 *  clase que genera un token aleatorio usado para
 *  crear el token de verificacion de cuenta por correo
 * @package App\Classes
 */
class GenerateInitAcountToken
{
    public function  __construct(){

    }

    /**
     * este metodo interno crea un token
     * @param $tokenLen cantida de caracteres que tendra el token
     * @return string token generado
     */
    private function _generateToken($tokenLen){

        $tokenLenght = $tokenLen;

        $seed = "qwertyuiopasdfghjklzxcvbn7894561230";

        $maxRando = strlen ( $seed);

        $token = "";

        for($i = 0; $i <  $tokenLenght; $i++){
            $token .= $seed[rand(0, $maxRando -1 )];
        }
        return $token;
    }

    /**
     * este token es para el olvido de contraseña
     * @return string token generado
     */
    public function generateTokenForgotten(){
        return $this->_generateToken(8);
    }

    /**
     * este token lo uso cunado se crea una cuenta por primera vez
     * @return string token generado
     */
    public function  generateToken(){
        return $this->_generateToken(6);
    }

}