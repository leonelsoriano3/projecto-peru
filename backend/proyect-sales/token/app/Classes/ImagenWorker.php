<?php
namespace App\Classes;


/**
 * Class para guardar las imagenes por el rest
 * @package App\Classes
 */
class ImagenWorker
{

    /**
     * guarda una imagen que llega en base 64
     * @param $base64String codiifcacion de la imagen
     * @param $preNamne string que se le puede poner al nombre dle archivo
     * @return string path generado
     */
    public function saveImg($base64String, $preNamne, $extention ){

        if($extention != null && $extention == 'image/jpeg'){
            $base64String = str_replace('data:image/jpeg;base64,', '', $base64String);
            $base64String = str_replace('data:image/jpg;base64,', '', $base64String);
            $extention = 'jpg';
        }if($extention != null && $extention == 'image/png'){
            $base64String = str_replace('data:image/png;base64,', '', $base64String);
            $extention = 'png';
        }

        $writeFolder = "./img"; 
        $outputFile = $this->_generateName($preNamne);


        $base64String = str_replace(' ', '+', $base64String);

        $file = fopen($writeFolder . "/" . $outputFile . '.'. $extention, "wb");
 
        //problem with open a file
        if (!$file)
            throw new \Exception('can able to open file');
 
        //write file from base64 string
       $writeStatus = fwrite($file, base64_decode($base64String));
 
        //problem with write
        if (!$writeStatus)
            throw new \Exception('file is not writable');
 
        //close the file
        fclose($file);
 
        //return file name
        return ($outputFile . '.'. $extention);

    }

    /**
    * genera un nombre unico para la imagen del usuario
    * @param $email email del usuario si es null no se agregan 
    */
    private function _generateName($preNamne){
        $tokenLenght = 16;

        $seed = "qwertyuiopasdfghjklzxcvbn7894561230";

        $maxRando = strlen ( $seed);

        $token = "";

        for($i = 0; $i <  $tokenLenght; $i++){
            $token .= $seed[rand(0, $maxRando -1 )];
        }
        return  $preNamne . $token;
    }




}
