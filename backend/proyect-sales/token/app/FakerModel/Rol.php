<?php
/**
 * User: leonelsoriano3@gmail.com
 * Date: 22/07/17
 * Time: 10:24 PM
 */

namespace App\FakerModel;


/**
 * Class Rol
 *  esta representa el item en base de datos de roles
 *  @package App\Classes
 */
class Rol
{
    private $id;

    private $name;

    private $level;


    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getLevel(){
        return $this->level;
    }

    public function setLevel($level){
        $this->level = $level;
    }

}
