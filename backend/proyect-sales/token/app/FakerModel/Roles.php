<?php
/**
 * User: leonelsoriano3@gmail.com
 * Date: 22/07/17
 * Time: 10:24 PM
 */

namespace App\FakerModel;

use App\FakerModel\Rol;

/**
 * Class Roles
 *  clase que simula el item de la tabla de roles
 *  @package App\Classes
 */
class Roles
{
    private $_arrayRol;

    public function __construct(){
        $this->_arrayRol = array();
        
        $item = null;
        $item = new Rol();
        $item->setId(1);
        $item->setName("SU");
        $item->setLevel(1);

        array_push($this->_arrayRol, $item);

        $item = null;
        $item = new Rol();
        $item->setId(2);
        $item->setName("Admin");
        $item->setLevel(2);

        array_push($this->_arrayRol, $item);

        $item = null;
        $item = new Rol();
        $item->setId(3);
        $item->setName("Vendedor");
        $item->setLevel(3);

        array_push($this->_arrayRol, $item);

        $item = null;
        $item = new Rol();
        $item->setId(4);
        $item->setName("Usuario");
        $item->setLevel(4);

        array_push($this->_arrayRol, $item);
        
        
    }

    /**
     * simula una busqueda por ide
     * @param integer $id id que se va a buscar
     * @return Rol que pertenece al ide pasado
    */
    public function findById($id){
        if($id > count($this->_arrayRol) || $id <= 0 ){
            return null;
        }else{
            for($i = 0; $i < count($this->_arrayRol) ; $i++){
                if($this->_arrayRol[$i]->getId() == $id){
                    return $this->_arrayRol[$i];
                }
            }
            return null;

        }
    }


    

}
