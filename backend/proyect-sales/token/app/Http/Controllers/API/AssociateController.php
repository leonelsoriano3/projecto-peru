<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Associate;
use App\ProductsAsociate;
use Validator;


class AssociateController extends Controller
{

   public function deleteById(Request $request){
        
        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $asociate = Associate::find($id_param);

        if($asociate != null){

            ProductsAsociate::where('asociate_id', $id_param )->delete();

            $asociate->delete();

            return response()->json(['success' => $asociate],
                200);
        }else{
            return response()->json(['error' => "No se encuentra el asociador"],
                200);
        }

    }//end deleteById


    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'max:255|min:3',
            'description' => 'max:1024|min:6',
            'products' => ''
        ]);

        $validatorQueryParam = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }else if($validatorQueryParam->fails() ){
            return response()->json(['error'=>$validatorQueryParam->errors()], 401);    
        }else{

            $asociate = Associate::find($request->route('id'));

            if($asociate != null){

                if($request->get('name')){
                    $asociate->name = $request->get('name');
                }

                if($request->get('description')){
                    $asociate->description = $request->get('description');
                }

                $asociate->save();

                if($asociate->id){
                    
                    if($request->get("products")){

                        ProductsAsociate::where('asociate_id', '=', $asociate->id)
                            ->delete();

                        for($i = 0; $i < count($request->get("products")); $i++ ){

                            try{
                                $productsAsociate = ProductsAsociate::create(
                                    array('asociate_id' => $asociate->id,
                                    'product_id' => $request->get('products')[$i]["id"])
                            ); 


                            }catch(\Illuminate\Database\QueryException $ex){
                    
                                return response()->json(['errror'=> $ex], 200);
                            }


                        }//end for
                    }//end if(products)
                
                }//end if asociate->id


                return response()->json(
                    ['success' => "Asociador actualizado de manera correcta" ],                    
                        200);
            }else{
                return response()->json(['error' => "No se encuentra el asociador buscado"],
                    200);
            
            }
        }
    }//end findAll


    public function findAll(Request $request){
        
        $validator = Validator::make(  $request->query(), [
            'name' => 'max:255|min:3',
            'description' => 'max:1024|min:6',
            'page' => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }else{

            $associates = null;
            
            $queryBuilder = Associate::query();

             if($request->get('name') !== null){
                $queryBuilder = $queryBuilder->where('name', 'like' , '%'. $request->get('name') . '%' );
            }         

            if($request->get('description') !== null){
                $queryBuilder = $queryBuilder->where
                    ('description', 'like' , '%'. $request->get('description') . '%' );
            }

            if($request->get('page')){
                $page = (int) $request->get('page');
                $queryBuilder->offset($page * 10 )->limit(10);
            }

            $associates = $queryBuilder->get();



            for($i = 0; $i < count($associates); $i++){
                $productsAsociate = ProductsAsociate::where(
                    'asociate_id',$associates[$i]->id)->
                    join('products', 'products.id', '=', 'products_asociates.product_id')->get();

                $associates[$i]->products = $productsAsociate; 
            }


            return response()->json(['success' => $associates],
                    200);            
        }




    }


    public function findById(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $asociate = Associate::find($id_param);

        if($asociate != null){
            unset($asociate->updated_at);
            return response()->json(['success' => $asociate],
                200);
        }else{
            return response()->json(['error' => "No se encuentra el asociador"],
                200);
        }
    }


    function create(Request $request){
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:3',
            'description' => 'required|max:1024|min:6',
            'products' => ''
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }

        $input = $request->all();

        $user = Associate::create($input);

        if($request->get("products")){

            for($i = 0; $i < count($request->get("products")); $i++ ){



                try{
                    $productsAsociate = ProductsAsociate::create(
                        array('asociate_id' => $user->id,
                               'product_id' => $request->get('products')[$i]["id"]
                            )
                    ); 


                }catch(\Illuminate\Database\QueryException $ex){
                    
                    return response()->json(['errror'=> $ex], 200);
                }


            }
        }//end for products
       

        return response()->json(['success'=>"Asociacion creada correctamente"], 200);
    } 


}
