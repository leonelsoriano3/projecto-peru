<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Province;
use App\Region;
use Validator;

class LocationController extends Controller
{

    /**
     * consigue todas las provincias guardadas 
     * en la base de datos
     */ 
    public function regionFindAll(){
       $regions = Region::all(); 
        
       return response()->json(['success' => $regions ],
            200);
    }


    public function provinceByRegion(Request $request){
        
        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|max:6'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $entity = Province::where('regions_id', $id_param)->get();

        if(count($entity) != 0 ){
            return response()->json(['success' => $entity],
                200);
        }else{
            return response()->json(['error' => "No se encuentran las provincias"],
                200);
        }

    }

    /**
    * buscar una region por su id
    */
    public function regionFindByid(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|max:6'
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $product = Region::find($id_param);

        if($product != null){
            unset($product->updated_at);
            return response()->json(['success' => $product],
                200);
        }else{
            return response()->json(['error' => "No se encuentra la region buscada"],
                200);
        }
    
    }



    /**
    * buscar una provincia por su id
    */
    public function provinceFindByid(Request $request){
        
        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|max:6'
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $product = Province::find($id_param);

        if($product != null){
            unset($product->updated_at);
            return response()->json(['success' => $product],
                200);
        }else{
            return response()->json(['error' => "No se encuentra la provincia buscada"],
                200);
        }
    
    }



}
