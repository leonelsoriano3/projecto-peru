<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Parameter;

class ParameterController extends Controller
{
    
    /**
    * metodo metodo de obtener todos en base de datos
    *
    * @param Request $request
    * @return \Illuminate\Http\JsonResponse
    */
    public function findAll(Request $request){

        $validator = Validator::make(  $request->query(), [
            'page' => 'integer',
            'text-search' => 'max:255',
            'active' => 'in:true,false',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }else{

          $transports = null;
          $queryBuilder = Parameter::query();

          if($request->get('active') != null){
            $paramActive = false;
            if($request->get('active') === 'true'){
              $paramActive = true;
            }  
            $queryBuilder->where('active', $paramActive );
          }//end request active


          
          if($request->get('text-search') !== null){
            $queryBuilder->where('name', 'like' , '%'. $request->get('text-search') . '%' );
            $queryBuilder->where('description', 'like' , '%'. $request->get('text-search') . '%' );
          }

          if($request->get('page') || $request->get('page') == 0 ){
            $page = (int) $request->get('page');
            $queryBuilder->offset($page * 10 )->limit(10);
          }

          $parameters = $queryBuilder->get();
          
          return response()->json(['success' => $parameters],
            200);  

        }//end else

    }//end find all



    /**
     * encargado de actualizar la entidad en base de datos
    */
    public function update(Request $request){

        $validator = Validator::make($request->all(), [
          'name' => 'max:255|min:3',
          'description' => 'max:1024|min:5',
          'level' => 'integer',
          'active' => 'in:false,true',
        ]);

        $validatorQueryParam = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }else if($validatorQueryParam->fails() ){
            return response()->json(['error'=>$validatorQueryParam->errors()], 401);    
        }else{
            $parameter = Parameter::find($request->route('id'));

            if($parameter != null){

             
              if($request->get('name')){
                $parameter->name = $request->get('name');
              }

              if($request->get('level')){
                $parameter->level = $request->get('leven');
              }

              if($request->get('description')){
                $parameter->delay = $request->get('description');
              }

              if($request->get('active')){
                $activeParam = ($request->get('active') == 'true')? true: false;
                $parameter->active = $activeParam;
              }


              $parameter->save();

              return response()->json(['success' => "Parametro actualizado de manera correcta" ],
                200);

            }else{

                return response()->json(['error' => "No se encuentra el parametro buscado"],
                    200);
            }
        
        }//end else

    }//end ipdate

}
