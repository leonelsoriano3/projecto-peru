<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Payment;
use Validator;

class PaymentController extends Controller
{

    public function findOne(Request $request){
        
        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $entity = Payment::find($id_param);

        if($entity != null){
            unset($entity->updated_at);
            return response()->json(['success' => $entity],
                200);
        }else{
            return response()->json(['error' => "No se encuentra el tipo de pago"],
                200);
        }

    }

    public function findAll(Request $request){

        $validator = Validator::make(  $request->query(), [
            'name' => 'max:255',
            'description' => 'max:1024',
            'page' => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }else{

            $entities = null;
            
            $queryBuilder = Payment::query();

             if($request->get('name') !== null){
                $queryBuilder = $queryBuilder->where('name', 'like' , '%'. $request->get('name') . '%' );
            }         

            if($request->get('description') !== null){
                $queryBuilder = $queryBuilder->where
                    ('description', 'like' , '%'. $request->get('description') . '%' );
            }

            if($request->get('page')){
                $page = (int) $request->get('page');
                $queryBuilder->offset($page * 10 )->limit(10);
            }

            $entities = $queryBuilder->get();

            return response()->json(['error' => $entities],
                    200);            
        }    
    }



    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'max:255|min:3',
            'description' => 'max:1024|min:6',
        ]);

        $validatorQueryParam = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }else if($validatorQueryParam->fails() ){
            return response()->json(['error'=>$validatorQueryParam->errors()], 401);    
        }else{

            $entity = Payment::find($request->route('id'));

            if($entity != null){

                if($request->get('name')){
                    $entity->name = $request->get('name');
                }

                if($request->get('description')){
                    $entity->description = $request->get('description');
                }

                $entity->save();

                return response()->json(
                    ['success' => "Tipo de pago actualizado de manera correcta" ],                    
                        200);
            }else{
                return response()->json(['error' => "No se encuentra el typo de pago buscado"],
                    200);
            
            }
        }

        
    }


    public function create(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:3',
            'description' => 'required|max:1024|min:6'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }

        $input = $request->all();

        $category = Payment::create($input);

        return response()->json(['success'=>"Forma de pago agregada correctamente"], 200);

    }



}
