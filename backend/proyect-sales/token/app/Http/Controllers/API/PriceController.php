<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Price;
use Validator;


class PriceController extends Controller
{

    function getPriceProduc(Request $request){

        $price = Price::where('product_id', $request->route('id'))
            ->orderBy('created_at', 'des')
            ->limit(1)->get();

        if($price != null){
            unset($price->updated_at);
            return response()->json(['success' => $price],
                200);
        }else{
            return response()->json(['error' => "No se encuentra el precio buscado"],
                200);
        } 
    }



    /**
     *  consigue todos los precios
     */
    function findAll(Request $request){
        
        $validator = Validator::make(  $request->query(), [
            'product_id' => 'integer',    
            'amount_less' => 'numeric|max:10000000000000000',
            'amount_more' => 'numeric|max:10000000000000000',
            'page' => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }else{
           
            $prices = null; 
            $queryBuilder = Price::query();

            if($request->get('product_id') != null){
                $queryBuilder = $queryBuilder->where
                    ('product_id', $request->get('product_id') );
            }

            if($request->get('amount_less') != null){
                $queryBuilder = $queryBuilder->where
                    ('amount', '<=', $request->get('amount_less'));
            }

            if($request->get('amount_more') != null){
                $queryBuilder = $queryBuilder->where
                    ('amount', '>=', $request->get('amount_more'));
            }

            if($request->get('page')){
                $page = (int) $request->get('page');
                $queryBuilder->offset($page * 10 )->limit(10);
            }

            $prices = $queryBuilder->get();

            return response()->json(['error' => $prices],
                    200);  

        }

    }//END findAll


    function findById(Request $request){
      
        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $price = Price::find($id_param);

        if($price != null){
            unset($price->updated_at);
            return response()->json(['success' => $price],
                200);
        }else{
            return response()->json(['error' => "No se encuentra el precio buscado"],
                200);
        } 
    }//END findById


    /**
     * encargado de crear un precio
     */
    function create(Request $request){
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer',
            'amount' => 'required|numeric|max:10000000000000000'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }

        $input = $request->all();


        $price = Price::create($input);

        return response()->json(['success'=>"Precio agregado correctamente"], 200);
    } 
}
