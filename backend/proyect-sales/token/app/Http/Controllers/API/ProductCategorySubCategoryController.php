<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductCaterorySubCategory;
use App\Product;
use App\Category;
use Validator;

class ProductCategorySubCategoryController extends Controller
{


    public function remove(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $entity = ProductCaterorySubCategory::find($id_param);

        if($entity != null){
            //ProductsAsociate::where('product_id', $id_param )->delete();
            $entity->delete();

            return response()->json(['success' => "Categorizador eliminado de manera correcta"],
                200);
        }else{
            return response()->json(['error' => "No se encuentra el categorizador"],
                200);
        }
    
    }

    /*
    * actualiza una asociacion
    */
    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'product_id' => 'integer',
            'category_id' => 'integer',
            'sub_category_id' => 'integer'
        ]);

        $validatorQueryParam = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200); 

        }else if($validatorQueryParam->fails() ){
            return response()->json(['error'=>$validatorQueryParam->errors()], 200 );

        }else{

            $entity = ProductCaterorySubCategory::find($request->route('id'));

            if($entity != null){

                if($request->get('product_id')){
                    $entity->product_id = $request->get('product_id');
                }

                if($request->get('category_id')){
                    $entity->category_id = $request->get('category_id');
                }

                if($request->get('sub_category_id')){
                    $entity->sub_category_id = $request->get('sub_category_id');
                }

                

                try{
                    $entity->save();

                    return response()->json(
                        ['success' => "El asociador se actualizado de manera correcta" ],                    
                        200);

                }catch(\Illuminate\Database\QueryException $ex){
                    return response()->json(['errror'=> $ex], 200);
                }

                         }else{
                return response()->json(['error' => "No se cuentra el asociador buscado"],
                    200);
            
            }
        }
    
    }


    public function findByCategory(Request $request){
    
        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $entity = ProductCaterorySubCategory::where('category_id', $id_param)->get();

        if(count($entity) != 0 ){
            unset($entity->updated_at);
            return response()->json(['success' => $entity],
                200);
        }else{
            return response()->json(['error' => "No se encuentra la busqueda por categoria"],
                200);
        }

    }//end  findByCategory


    /**
     * buscar por productos
    */
    public function findByProduct(Request $request){
        
        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $entitys = ProductCaterorySubCategory::where('product_id',$id_param)->get();

        if( count($entitys) == 1){
            unset($entitys[0]->updated_at);

            $productEntity = Product::find($entitys[0]->product_id); 

            if($productEntity){
                $entitys[0]->product_id = $productEntity;
            }

 
            $categoryEntity = Category::find($entitys[0]->category_id); 

            if($categoryEntity){
                $entitys[0]->category_id = $categoryEntity;            
            }

            return response()->json(['success' => $entitys],
                200);
        }else{
            return response()->json(['error' => "No se encuentra el categorizador"],
                200);
        }

    }

    /**
     * busca por id en la base de datos
     */
    public function findById(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $asociate = ProductCaterorySubCategory::find($id_param);

        if($asociate != null){
            unset($asociate->updated_at);
            return response()->json(['success' => $asociate],
                200);
        }else{
            return response()->json(['error' => "No se encuentra la asociacion"],
                200);
        }
    } 


    /**
     * crea una asociacion    
    */
    function create(Request $request){
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer',
            'category_id' => 'required|integer',
            'sub_category_id' => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }
 
        $entityFind = ProductCaterorySubCategory::where('product_id',
            $request->get('product_id'))->get(); 
        
        if(count($entityFind) == 0){

            $input = $request->all();
           
            try{
                ProductCaterorySubCategory::create($input);
                return response()->json(['success'=>"Relacion guardada correctamente"], 200);
            }catch(\Illuminate\Database\QueryException $ex){
                    return response()->json(['errror'=> $ex], 200);
            }

            
        }else{
            return response()->json(['error'=> "Relacion ya existe "], 200);
        }


    
    } 
}
