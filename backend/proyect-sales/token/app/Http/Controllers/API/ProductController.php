<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Product;
use App\Category;
use App\SubCategory;
use App\Price;
use App\Classes\ImagenWorker;
use App\ProductCaterorySubCategory;


class ProductController extends Controller
{

    /**
    * metodo para obtener todos los productos
    *
    * @param Request $request
    * @return \Illuminate\Http\JsonResponse
    */
    public function findAll(Request $request){

        $validator = Validator::make(  $request->query(), [
            'page' => 'integer',
            'text-search' => 'max:255',
            'active' => 'in:true,false',
            'category' => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }else{

            $products = null;
            
            $queryBuilder = Product::query();

            if($request->get('active') != null){
                $paramActive = false;
                if($request->get('active') === 'true'){
                  $paramActive = true;
                }  
                $queryBuilder->where('active', $paramActive );
            }//end request active


            if($request->get('text-search') !== null){
                $queryBuilder->where('name', 'like' , '%'. $request->get('text-search') . '%' );
            }

            if($request->get('category') != null){
                $queryBuilder->leftJoin('product_caterory_sub_categories','product_caterory_sub_categories.product_id','=' ,
                    'products.id');
                $queryBuilder->where('product_caterory_sub_categories.category_id',
                    $request->get('category'));

                $queryBuilder->select(['products.*', 'product_caterory_sub_categories.id AS product_caterory_sub_categories_idtable']);
            }

            if($request->get('page') || $request->get('page') == 0 ){

                $page = (int) $request->get('page');
   
                $queryBuilder->offset($page * 10 )->limit(10);
            }

            $products = $queryBuilder->get();

            if($products !== null){

                for($i = 0; $i < count($products); $i++){

                    $price = Price::where('product_id', $products[$i]->id)
                    ->orderBy('created_at', 'des')
                    ->limit(1)->get();

                    if($price !== null && count($price) == 1){
                        $products[$i]->price = (object) 
                            array('id' => $price[0]->id,
                            'product_id' => $price[0]->product_id,
                            'amount' => $price[0]->amount);
                    }


                    //find category

                    $productCaterorySubCategory = ProductCaterorySubCategory::where
                        ('product_id',  $products[$i]->id )->get();

                    if(count($productCaterorySubCategory) == 1){

                      
                        $category = Category::find( $productCaterorySubCategory[0]->category_id);

                        $productCaterorySubCategory[0]->category_name = $category->name;


                        $subCategory = SubCategory::find( $productCaterorySubCategory[0]->sub_category_id);
                        $productCaterorySubCategory[0]->subcategory_name = $subCategory->name;

                        $products[$i]->category = $productCaterorySubCategory[0];
                    }


                }//end for


            }//enf if products



            return response()->json(['success' => $products],
                    200);            
        }//end else
    }

    /**
     * encargado de actualizar el producdo en base de datos
    */
    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'max:255|min:3',
            'description' => 'max:1024|min:6',
            'active' => 'in:false,true',
            'img' => '',
            'type_img' => '',
            'price' => 'numeric|max:10000000000000000',
            'category_id'=> 'numeric',
            'sub_category_id' => 'numeric'

        ]);

        $validatorQueryParam = Validator::make( $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }else if($validatorQueryParam->fails() ){
            return response()->json(['error'=>$validatorQueryParam->errors()], 401);    
        }else{
            $product = Product::find($request->route('id'));

            if($product != null){

                if($request->get('name')){
                    $product->name = $request->get('name');
                }


                $imagenWorker = new ImagenWorker();
                if($request->get('img') && $request->get("type_img")){
                    $product->img = $imagenWorker->saveImg($request->get('img'),
                        "product",  $request->get("type_img"));
                }



                if($request->get('description')){
                    $product->description = $request->get('description');
                }

                if($request->get('active') && ($request->get('active') == 'true'
                    || $request->get('active') == 'false') ){

                    $activeParam = ($request->get('active')) === 'true'? true: false;
                    $product->active = $activeParam;
                }

                $product->save();


                if($request->get('price')){
                    if($request->get('price')){
                        $price = Price::create(
                        array('product_id' =>  $product->id , 'amount' => $request->get('price') ));
                    }

                }


                //agrego la categoria y sub categoria
                if($request->get('category_id') || $request->get('sub_category_id')){


                    ProductCaterorySubCategory::where('product_id', $product->id)->delete();

                    $categoryId = null;
                    if($request->get('category_id')){
                        $categoryId = $request->get('category_id');
                    }

                    $subCategoryId = null;

                    if($request->get('sub_category_id')){
                        $subCategoryId =  $request->get('sub_category_id');
                    }

                    $productCaterorySubCategory = ProductCaterorySubCategory::create(
                        array(
                            'product_id' => $product->id,
                            'category_id' => $categoryId,
                            'sub_category_id' => $subCategoryId
                        )
                    );

                }

                return response()->json(['success' => "Usuario actualizado de manera correcta" ],
                    200);

            }else{

                return response()->json(['error' => "No se encuentra el producto buscado"],
                    200);
            }
        
        }//end else

    }


    /**
     * buscar un producto por su id
     */
    public function findById(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $product = Product::find($id_param);

        if($product != null){
            unset($product->updated_at);
            return response()->json(['success' => $product],
                200);
        }else{
            return response()->json(['error' => "No se encuentra el producto buscado"],
                200);
        }
    
    }


    /**
     * encargado de crear productos
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:3',
            'description' => 'required|max:1024|min:6',
            'active' => 'in:false,true',
            'img' => '',
            'type_img' => '',
            'price' => 'numeric|max:10000000000000000',
            'category_id' => 'numeric',
            'sub_category_id' => 'numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }

        $input = $request->all();

        if($request->get('active') && ($request->get('active') == 'true'
                    || $request->get('active') == 'false') ){

            $activeParam = ($request->get('active')) === 'true'? true: false;
            $input["active"] = $activeParam;
        }

        $imagenWorker = new ImagenWorker();

        if($request->get('img') && $request->get("type_img")){
            $input["img"] = $imagenWorker->saveImg($request->get('img'),"product",  $request->get("type_img"));
        }


        $product = Product::create($input);


        if($request->get('price')){
           $price = Price::create(
            array('product_id' =>  $product->id , 'amount' => $request->get('price') ));
        }


        //agrego la categoria y sub categoria
        if($request->get('category_id') || $request->get('sub_category_id')){

            $categoryId = null;
            if($request->get('category_id')){
                $categoryId = $request->get('category_id');
            }

            $subCategoryId = null;

            if($request->get('sub_category_id')){
                $subCategoryId =  $request->get('sub_category_id');
            }

            $productCaterorySubCategory = ProductCaterorySubCategory::create(
                array(
                    'product_id' => $product->id,
                    'category_id' => $categoryId,
                    'sub_category_id' => $subCategoryId
                )
            );

        }

        return response()->json(['success'=>"Producto guardado correctamente"], 200);
    }



}
