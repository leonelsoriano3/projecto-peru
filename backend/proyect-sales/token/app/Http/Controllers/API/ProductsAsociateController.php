<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductsAsociate;
use Validator;
use App\Product;
use App\Associate;



class ProductsAsociateController extends Controller
{

    public function removeFromAsociate(Request $request){
    
        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');

        try{
            ProductsAsociate::where('asociate_id', $id_param)->delete();
            return response()->json(['success'=>"Relacion por asociador 
                eliminada correctamente"], 200);
        }catch(\Illuminate\Database\QueryException $ex){
            return response()->json(['errror'=> $ex], 200);
        }

    }


    public function removeFromId(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $productsAsociate = ProductsAsociate::find($id_param);

        if($productsAsociate){
           $productsAsociate->delete();
            return response()->json(['success'=>"Relacion eliminada correctamente"], 200);
        }else{
            return response()->json(['error'=> 'No existe la sociacion'],
                200);
        }
    }


    public function getByIdAsociate(Request $request){
        
        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $result = '';

        $productsAsociate = ProductsAsociate::where('asociate_id', $id_param )->get();
        
        if(count($productsAsociate) != 0){
            $asocciate = Associate::find($id_param);
            unset($asocciate->updated_at);
            unset($asocciate->created_at);
        
            //$result .= $productsAsociate[0]->product_id; 
            for($i = 0; $i < count($productsAsociate); $i++ ){

                $idProduct = $productsAsociate[$i]->product_id;
                $productFind = Product::find($productsAsociate[$i]->product_id);
                if($productFind){

                    unset($productFind->updated_at);
                    unset($productFind->created_at);

                    $productsAsociate[$i]->product = $productFind;
                }

                unset($productsAsociate[$i]->updated_at);
                unset($productsAsociate[$i]->created_at);
                unset($productsAsociate[$i]->product_id);
                unset($productsAsociate[$i]->asociate_id);

      
                $productsAsociate[$i]->asocciate = $asocciate;
            
            }

            return response()->json(['success'=> $productsAsociate], 200);
        
        }else{

            return response()->json(['error'=>
                "No se consigue este asociados con esta informacion"], 200);
        }


    }


    /**
     * encargado asociasiones entre productos y asociadores
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request){

        $validator = Validator::make($request->all(), [
            'asociate_id' => 'required|integer',
            'product_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }

        //verificar si ya existe

        $resultData = ProductsAsociate::where(
            'product_id', $request->get('asociate_id'))
                ->where('product_id', $request->get('product_id'))
                ->get();

        if(count($resultData) != 0){
            return response()->json(['error'=>"Ya existe esta relacion"], 200);

        }else{
            $input = $request->all();

            $productsAsociate = null;
            
            try{
                $productsAsociate = ProductsAsociate::create($input); 
            }catch(\Illuminate\Database\QueryException $ex){

                return response()->json(['errror'=> $ex], 200);
            }

            if($productsAsociate){
                return response()->json(['success'=>"Relacion creada correctamente"], 200);
            }else{
                return response()->json(['errror'=>"No se a podido generar la relacion"], 200);
            }
        
        }

    }




}
