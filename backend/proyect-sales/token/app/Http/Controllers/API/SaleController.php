<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sale;
use App\User;
use Validator;

class SaleController extends Controller
{


    public function findAll(Request $request){

        $validator = Validator::make(  $request->query(), [
            'product_id' => 'integer',
            'user_id' => 'integer',
            'seller_id' => 'integer', 
            'payment_id' => 'integer',   
            'tip_less' => 'numeric|max:10000000000000000',
            'tip_more' => 'numeric|max:10000000000000000',
            'page' => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }else{
           
            $sales = null; 
            $queryBuilder = Sale::query();

            if($request->get('tip_less') != null){
                $queryBuilder = $queryBuilder->where
                    ('tip', '<=', $request->get('tip_less'));
            }

            if($request->get('tip_more') != null){
                $queryBuilder = $queryBuilder->where
                    ('top', '>=', $request->get('tip_more'));
            }

            if($request->get('seller_id')){
                $queryBuilder = $queryBuilder->where
                    ('seller_id', $request->get('seller_id') );
            }

            if($request->get('user_id')){
                $queryBuilder = $queryBuilder->where
                    ('user_id', $request->get('user_id') );
            }

            if($request->get('payment_id')){
                $queryBuilder = $queryBuilder->where
                    ('payment_id', $request->get('payment_id') );
            }

            if($request->get('page')){
                $page = (int) $request->get('page');
                $queryBuilder->offset($page * 10 )->limit(10);
            }

            $sales = $queryBuilder->get();

            return response()->json(['error' => $sales],
                    200);  

        }


    }


    public function findById(Request $request){
        
        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $entity = Sale::find($id_param);

        if($entity != null){
            unset($entity->updated_at);
            return response()->json(['success' => $entity],
                200);
        }else{
            return response()->json(['error' => "No se encuentra la venta"],
                200);
        }

    }


    public function desActivate(Request $request){
        
        $validator = Validator::make($request->all(), [
            'active' => 'required|in:false,true'

        ]);

        $validatorQueryParam = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }else if($validatorQueryParam->fails() ){
            return response()->json(['error'=>$validatorQueryParam->errors()], 401);    
        }else{

            $entity = Sale::find($request->route('id'));

            if($entity != null){

                if($request->get('active')){
                    $activeParam = ($request->get('active')) === 'true'? true: false;
                    $entity->active = $activeParam;
                }

                $entity->save();

                return response()->json(
                    ['success' => "Venta actualizado de manera correcta" ],                    
                        200);
            }else{
                return response()->json(['error' => "No se encuentra la venta buscada"],
                    200);
            
            }
        }

    
    
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'tip' => 'numeric|max:10000000000000000',
            'discount' => 'numeric|max:100',
            'payment_id' => 'integer',
        ]);

        $validatorQueryParam = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }else if($validatorQueryParam->fails() ){
            return response()->json(['error'=>$validatorQueryParam->errors()], 401);    
        }else{

            $entity = Sale::find($request->route('id'));

            if($entity != null){

                if($request->get('tip')){
                    $entity->tip = $request->get('tip');
                }

                if($request->get('discount')){
                    $entity->discount = $request->get('discount');
                }

                if($request->get('payment_id')){
                    $entity->payment_id = $request->get('payment_id');
                }

                $entity->save();

                return response()->json(
                    ['success' => "Venta actualizado de manera correcta" ],                    
                        200);
            }else{
                return response()->json(['error' => "No se encuentra la venta buscada"],
                    200);
            
            }
        }

    
    }


    public function create(Request $request){
        
        $validator = Validator::make($request->all(), [
            'tip' => 'numeric|max:10000000000000000',
            'discount' => 'numeric|max:100',
            'seller_id' => 'required|integer',
            'user_id' =>  'required|integer',
            'payment_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }

        $user = User::find($request->get('user_id'));
        $seller = User::find($request->get('seller_id'));
        
        if( $seller->rol  == 4 ){
            return response()->json(['error'=> 'Error en el tipo de usuario para vendedor '], 200); 
        }else if( $user->rol != 4){
            return response()->json(['error'=> 'Error en el tipo de usuario para comprador'], 200);
        }else if($request->get('discount') && ($request->get('discount') < 0 
            || $request->get('discount')) == > 100){
            return response()->json(['error'=> 'descuento debe esta entre 0 y 100%'], 200);
        }else{
            $input = $request->all();

            $entity = Sale::create($input);

            return response()->json(['success'=>"Pago resgistrado correctamente"], 200);
        
        }


    }



}



