<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SalePrice;
use Validator;


class SalesPriceController extends Controller
{
    

    public function desactivate(Request $request){
    
        $validatorQueryParam = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if($validatorQueryParam->fails() ){
            return response()->json(['error'=>$validatorQueryParam->errors()], 200);

        }else{

            $entity = SalePrice::find($request->route('id'));

            if($entity != null){

                $entity->active = false;
                try{
                    $entity->save();

                    return response()->json(
                        ['success' => "Entidad eliminada correctamente" ],                    
                        200);

                }catch(\Illuminate\Database\QueryException $ex){
                    return response()->json(['errror'=> $ex], 200);
                }

            }else{
                return response()->json(['error' => "No se encuentra"],
                    200);
            }
        }
    }



    public function findAll(Request $request){


        $validator = Validator::make(  $request->query(), [
            'sale_id' => 'integer',    
            'price_id' => 'integer',
            'page' => 'integer',
            'active' => 'in:false,true'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }else{
           
            $salesPrices = null; 
            $queryBuilder = SalePrice::query();

            if($request->get('sale_id') != null){
                $queryBuilder = $queryBuilder->where
                    ('sale_id', $request->get('sale_id') );
            }

            if($request->get('price_id') != null){
                $queryBuilder = $queryBuilder->where
                    ('price_id', $request->get('price_id') );
            }

            if($request->get('page')){
                $page = (int) $request->get('page');
                $queryBuilder->offset($page * 10 )->limit(10);
            }

            if($request->get('active')){
                $activeParam = ($request->get('active')) === 'true'? true: false;
                $queryBuilder = $queryBuilder->where
                    ('active', $activeParam );

            }

            $salesPrices = $queryBuilder->get();

            return response()->json(['error' => $salesPrices],
                    200);  
        }

    }


    public function findById(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $entity = SalePrice::find($id_param);

        if($entity != null){
            unset($entity->updated_at);
            return response()->json(['success' => $entity],
                200);
        }else{
            return response()->json(['error' => 
                "No se el item buscado"], 200);
        } 
    }


    
    public function create(Request $request){

        $validator = Validator::make($request->all(), [
            'sale_id' => 'required|integer',
            'price_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }

        $input = $request->all();
        $entity = null;
           
        try{
            $entity = SalePrice::create($input);
            
            return response()->json(['success'=>
                "Asociacion creada correctamente"], 200);

        }catch(\Illuminate\Database\QueryException $ex){

            return response()->json(['errror'=> $ex], 200);
        }

    }
}
