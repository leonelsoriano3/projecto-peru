<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubCategory;
use Validator;
use App\ProductCaterorySubCategory;

class SubCategoriesController extends Controller
{
       public function deleteById(Request $request){
        
        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');

        $productCaterorySubCategory =  ProductCaterorySubCategory::where(
            'sub_category_id', '=', $id_param)->get();


        if(count($productCaterorySubCategory) > 0){
            return response()->json(['error' => 
                "Debe eliminar los productos de esta sub-categoria para poder eliminarla"],
                200);
        }  


        $category = SubCategory::find($id_param);

        if($category != null){
            
            $category->delete();

            return response()->json(['success' =>
                'Sub categoria eliminada correctamente id: ' . $id_param],
                200);
        }else{
            return response()->json(['error' => "No se encuentra la categoria"],
                200);
        }

    }//end deleteById


    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'max:255|min:3',
            'description' => 'max:1024|min:6',
        ]);

        $validatorQueryParam = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }else if($validatorQueryParam->fails() ){
            return response()->json(['error'=>$validatorQueryParam->errors()], 401);    
        }else{

            $category = SubCategory::find($request->route('id'));

            if($category != null){

                if($request->get('name')){
                    $category->name = $request->get('name');
                }

                if($request->get('description')){
                    $category->description = $request->get('description');
                }

                $category->save();

                return response()->json(
                    ['success' => "Categoria actualizado de manera correcta" ],                    
                        200);
            }else{
                return response()->json(['error' => "No se encuentra la cantegoria buscada"],
                    200);
            
            }
        }
    }


    public function findAll(Request $request){
        
        $validator = Validator::make(  $request->query(), [
            'name' => 'max:255',
            'description' => 'max:1024',
            'page' => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }else{

            $associates = null;
            
            $queryBuilder = SubCategory::query();


            if($request->get('text-search') !== null){
                $queryBuilder->where('name', 'like' , '%'. $request->get('text-search') . '%' );
            
            }else{
            
            
                if($request->get('name') !== null){
                    $queryBuilder = $queryBuilder->where('name', 'like' , '%'. $request->get('name') . '%' );
                }         

                if($request->get('description') !== null){
                    $queryBuilder = $queryBuilder->where
                        ('description', 'like' , '%'. $request->get('description') . '%' );
                }
            
            }


            if($request->get('page')){
                $page = (int) $request->get('page');
                $queryBuilder->offset($page * 10 )->limit(10);
            }

            $queryBuilder->orderBy('name', 'asc');

            $associates = $queryBuilder->get();
            

            return response()->json(['success' => $associates],
                    200);            
        }

    }


    public function findById(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $category = SubCategory::find($id_param);

        if($category != null){
            unset($category->updated_at);
            return response()->json(['success' => $category],
                200);
        }else{
            return response()->json(['error' => "No se encuentra la categoria"],
                200);
        }
    }


    function create(Request $request){
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:3',
            'description' => 'required|max:1024|min:6'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }

        $input = $request->all();

        $category = SubCategory::create($input);

        return response()->json(['success'=>"Sub categoria creada correctamente"], 200);
    } 
}
