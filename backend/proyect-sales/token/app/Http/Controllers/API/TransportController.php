<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Transport;

class TransportController extends Controller
{



    /**
    * metodo metodo de obtener todos en base de datos
    *
    * @param Request $request
    * @return \Illuminate\Http\JsonResponse
    */
    public function findAll(Request $request){

        $validator = Validator::make(  $request->query(), [
            'page' => 'integer',
            'text-search' => 'max:255',
            'active' => 'in:true,false',
            'free_sending' => 'in:false,true',
            'delay' => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }else{

          $transports = null;
          $queryBuilder = Transport::query();

          if($request->get('active') != null){
            $paramActive = false;
            if($request->get('active') === 'true'){
              $paramActive = true;
            }  
            $queryBuilder->where('active', $paramActive );
          }//end request active


          if($request->get('free_sending') != null){
            $paramFree = false;
            if($request->get('free_sending') === 'true'){
              $paramFree = true;
            }  
            $queryBuilder->where('free_sending', $paramFree );
          }//end request active

          if($request->get('delay') != null){
            $queryBuilder->where('delay', $request->get('delay') );
          }//end request active

          
          if($request->get('text-search') !== null){
            $queryBuilder->where('name', 'like' , '%'. $request->get('text-search') . '%' );
          }

          if($request->get('page') || $request->get('page') == 0 ){
            $page = (int) $request->get('page');
            $queryBuilder->offset($page * 10 )->limit(10);
          }

          $transports = $queryBuilder->get();
          
          return response()->json(['success' => $transports],
            200);  

        }//end else

    }//end find all


    /**
     * buscar un trasporte por su id
    */
    public function findById(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }

        $id_param = $request->route('id');
        $trasport = Transport::find($id_param);

        if($trasport != null){
            return response()->json(['success' => $product],
                200);
        }else{
            return response()->json(['error' => "No se encuentra el trasporte buscado"],
                200);
        }
    
    }




    /**
     * encargado de actualizar el producdo en base de datos
    */
    public function update(Request $request){

        $validator = Validator::make($request->all(), [
          'name' => 'max:255|min:3',
          'delay' => 'integer',
          'free_sending' => 'in:false,true',
          'logo' => 'integer',
          'tracing' => 'max:255|min:3',
          'active' => 'in:false,true',
          'tax_id' => 'integer',
          'weight_tax_min' => 'numeric|max:10000000000000000',
          'weight_tax_max' => 'numeric|max:10000000000000000',
          'billing_type' => 'integer',
          'width_package' => 'numeric|max:10000000000000000',
          'high_package' => 'numeric|max:10000000000000000',
          'long_package' => 'numeric|max:10000000000000000',
          'weight_package' => 'numeric|max:10000000000000000'
        ]);

        $validatorQueryParam = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }else if($validatorQueryParam->fails() ){
            return response()->json(['error'=>$validatorQueryParam->errors()], 401);    
        }else{
            $transport = Transport::find($request->route('id'));

            if($transport != null){

             
              if($request->get('name')){
                $transport->name = $request->get('name');
              }


              if($request->get('delay')){
                $transport->delay = $request->get('delay');
              }

              if($request->get('free_sending')){
                $freeParam = ($request->get('free_sending') === 'true')? true: false;
                $transport->free_sending = $freeParam;
              }

              if($request->get('logo')){
                $transport->logo = $request->get('logo');
              }

              if($request->get('tracing')){
                $transport->tracing = $request->get('tracing');
              }

              if($request->get('active')){
                $activeParam = ($request->get('active') == 'true')? true: false;
                $transport->active = $activeParam;
              }

              if($request->get('tax_id')){
                $transport->tracing = $request->get('tax_id');
              }


              if($request->get('weight_tax_min')){
                $transport->tracing = $request->get('weight_tax_min');
              }



              if($request->get('weight_tax_max')){
                $transport->tracing = $request->get('weight_tax_max');
              }


               if($request->get('billing_type')){
                $transport->tracing = $request->get('billing_type');
              }

             
               if($request->get('width_package')){
                $transport->tracing = $request->get('width_package');
              }


              if($request->get('high_package')){
                $transport->tracing = $request->get('high_package');
              }


              if($request->get('long_package')){
                $transport->tracing = $request->get('long_package');
              }


              if($request->get('weight_package')){
                $transport->tracing = $request->get('weight_package');
              }


              $transport->save();

              return response()->json(['success' => "Trasporte actualizado de manera correcta" ],
                200);

            }else{

                return response()->json(['error' => "No se encuentra el transporte buscado"],
                    200);
            }
        
        }//end else

    }//end ipdate




    function create(Request $request){
        
        $validator = Validator::make($request->all(), [
          'name' => 'required|max:255|min:3',
          'delay' => 'integer',
          'free_sending' => 'required|in:false,true',
          'logo' => 'required|integer',
          'tracing' => 'max:255|min:3',
          'active' => 'in:false,true',
          'tax_id' => 'required|integer',
          'weight_tax_min' => 'numeric|max:10000000000000000',
          'weight_tax_max' => 'numeric|max:10000000000000000',
          'billing_type' => 'required|integer',
          'width_package' => 'numeric|max:10000000000000000',
          'high_package' => 'numeric|max:10000000000000000',
          'long_package' => 'numeric|max:10000000000000000',
          'weight_package' => 'numeric|max:10000000000000000'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }

        $input = $request->all();

        if($request->get('active') && ($request->get('active') == 'true'
                    || $request->get('active') == 'false') ){

            $activeParam = ($request->get('active')) === 'true'? true: false;
            $input["active"] = $activeParam;
        }

        if($request->get('free_sending') && ($request->get('free_sending') == 'true'
                    || $request->get('free_sending') == 'false') ){

            $freeParam = ($request->get('free_sending')) === 'true'? true: false;
            $input["free_sending"] = $freeParam;
        }

        $transport = Transport::create($input);
        
        if($transport !== null){
          return response()->json(['success'=>"Trasporte creado correctamente"], 200);

        }else{
          return response()->json(['error'=>"No se pudo crear el trasporte"], 200);
        }

    }//end create



}
