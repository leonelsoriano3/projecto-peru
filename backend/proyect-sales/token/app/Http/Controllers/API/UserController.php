<?php

namespace App\Http\Controllers\API;

use App\Classes\GenerateInitAcountToken;
use App\Classes\ImagenWorker;
use App\FakerModel\Roles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Mail;
use App\Province;

//$request->user() asi obtengo el usuario del token

class UserController extends Controller
{

    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
    

            $tokenUser =  $user->createToken('MyApp');

            $success = [];
            $success['token'] = $tokenUser->accessToken;
            $success['email'] = $user->email;
            $success['name'] = $user->name;
            $success['rol'] = $user->rol;
            $success['img'] = $user->img;

            if($user->active){
                return response()->json(['success' => $success], $this->successStatus);
            }else{
                return response()->json(['success' => "Usuario desactivado"], $this->successStatus);
            }

            
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 200);
        }
    }

    /**
     * este metodo permite agregar roles es solo para administradores
     */
    public function registerAdmin(Request $request){
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:3',
            'email' => 'required|email|max:255|min:3',
            'sur_name' => 'max:255|min:3',
            'password' => 'required|max:255|min:3',
            'c_password' => 'required|same:password',
            'rol' => 'integer',
            'img' => "max:10000000000",
            'type_img' => '',
            'favorite_payment' => 'integer',
            "distrito" => "min:3|max:255",
            "identification" => "required|max:16",
            "phone" => "max:255",
            "type_identification" => "required"
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }else  if($request->user()->rol != 1 && $request->user()->rol != 2){
            return response()->json(['error'=>"no tienes los permisos necesarios"], 200);
        }


        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        

        $userEmail = User::where('email', $input['email'])->first();

        if($userEmail != null){
          return response()->json(['error'=>"Ya existe un usuario registrado con este email"], 200);
        }

        $imagenWorker = new ImagenWorker();
        if($request->img && $request->type_img){
            $input['img'] = $imagenWorker->saveImg($input['img'] ,"user",$input['type_img']);
        }

        //token activador
        // $generatorToken = new GenerateInitAcountToken();
        //$input['token_activator'] =  $generatorToken->generateToken();
        //$input['active'] = 1;



        $user = User::create($input);
        
        $user->active = true;

        $user->save();

        //$success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;

        $userEmail = $input['email'];

        //TODO: falta arreglar esto de email
        Mail::raw('Este es tu token ' . $user->token_activator
            , function ($message)  use( &$userEmail) {
                $message->to($userEmail);
            //$message->to('leonelsoriano3@gmail.com');
        });

        return response()->json(['success'=>$success], $this->successStatus);
    }





    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|min:3',
            'email' => 'required|email|max:255|min:3',
            'sur_name' => 'max:255|min:4',
            'password' => 'required|max:255|min:3',
            'c_password' => 'required|same:password',
            'favorite_payment' => 'integer',
            "distrito" => "min:3|max:255",
            "identification" => "required|max:16",
            "img" => "max:10000000",
            'type_img' => '',
            "phone" => "max:255",
            "type_identification"=>'required|integer',         
  
        ]);




        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 200);            
        }

        
        $input = $request->all();

        $userEmail = User::where('email', $input['email'])->first();

        if($userEmail != null){
          return response()->json(['error'=>"Ya existe un usuario registrado con este email"], 200);
        }

        $input['password'] = bcrypt($input['password']);



        //token activador
        $generatorToken = new GenerateInitAcountToken();
        $imagenWorker = new ImagenWorker();

        $input['token_activator'] =  $generatorToken->generateToken();

        $input['rol'] =  4;



        if($request->img && $request->type_img){
            $input['img'] = $imagenWorker->saveImg($input['img'],"user", $request->type_img);
        }


        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;

        $userForEmail = $input['email'];

        //TODO: falta arreglar esto de email
        Mail::raw('Este es tu token ' . $user->token_activator
            , function ($message) use( &$userForEmail) {
            //$message->to('leonelsoriano3@gmail.com');
            $message->to($userForEmail);
        });

        return response()->json(['success'=> $user], 200);
    }

    /**
     * activador de las cuentas creadas
     *   se le debe enviar el token que llega por correo
     *   recive el token y el email
     */
    public function  activateAccount(Request $request){
        $token_param = $request->get('token');
        $email_param = $request->get('email');

        $user = User::where('token_activator',
            $token_param)->where('email', $email_param)->first();

        if($user != null){
            if($user->active == true){
                return response()->json(['success' => "usuario ya se encuentra activo"],
                    $this->successStatus);
            }else{
                $user->active = true;
                $user->token_activator = null;
                $user->save();
                return response()->json(['success' => "usuario verificado correctamente"],
                    $this->successStatus);
            }

        }else{
            return response()->json(['error' => "no se coincide con la data"],
                $this->successStatus);
        }

    }

    /**
     * envia un email con un token para luego verificarlo cuando olvida
     *  la contraseña
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function passWordForgotten(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                $this->successStatus);
        }

        $email_param = $request->get("email");


        $user = User::where('email', $email_param)->first();

        if($user != null){
            $tokenGenerator = new GenerateInitAcountToken();

            $tokenForgotten = $tokenGenerator->generateTokenForgotten();

            $user->token_forgotten = $tokenForgotten;

            $user->save();

            $userForEmail = $user->email;
            //TODO: falta arreglar esto de email
            Mail::raw('Este es tu token de recuperacion:  ' .
                $tokenForgotten
                , function ($message) use( &$userForEmail) {
                    //$message->to('leonelsoriano3@gmail.com');
                    $message->to($userForEmail);
                });

            return response()->json(['success' => "Token enviado"],
                $this->successStatus);

        }else{
            return response()->json(['error'=> "Data no encontrada"],
                $this->successStatus);

        }
    }

    /**
     * recuperacion de contraseña
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recuperate(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'token' => 'required|max:8',
            'password' => 'required|max:255|min:4',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                $this->successStatus);
        }

        $email_param = $request->get("email");
        $token_param = $request->get("token");
        $password_param = $request->get("password");

        $user = User::where('token_forgotten',
            $token_param)->where('email', $email_param)->first();

        if($user != null){

            $user->token_forgotten = null;
            $user->password = bcrypt($password_param);
            $user->save();
            return response()->json(['success' => "su contraseña se a actualizado"],
                $this->successStatus);
        }else{
            return response()->json(['error' => "no se coincide con la data"],
                $this->successStatus);
        }

    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        User::all();
        return response()->json(['success' => $user], $this->successStatus);
    }








    /* metodo para obtener todos los usuarios
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll(Request $request){

        $validator = Validator::make(  $request->query(), [
            'active' => 'integer',
            'page' => 'integer',
            'text-search' => 'max:255'
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                200);
        }else{

            $allUser = null;
            
            $queryBuilder = User::query();

      

            if($request->get('active') !== null){
                $queryBuilder = $queryBuilder->where
                    ('active', $request->get('active') );
            }

            if($request->get('page')){
                $page = (int) $request->get('page');
                $queryBuilder->offset($page * 10 )->limit(10);
            }

            if($request->get('text-search') !== null){
                $queryBuilder = $queryBuilder->orWhere('name', 'like' , '%'. 
                    $request->get('text-search') . '%' );
                $queryBuilder = $queryBuilder->orWhere('email', 'like' , '%'.
                    $request->get('text-search') . '%' );
                $queryBuilder = $queryBuilder->orWhere('sur_name', 'like' , '%'.
                    $request->get('text-search') . '%' );
            }

            $queryBuilder->orderBy('email', 'ASC');


            $allUser = $queryBuilder->get();


            for($i = 0; $i < count($allUser); $i++){
                if($allUser[$i]->provinces_id !== null){
                   $province = Province::find($allUser[$i]->provinces_id);
                   $allUser[$i]->province = $province;   
                }
                
            }


            return response()->json(['success' => $allUser],
                    200); 
        }

    }


    /**
     * consigue un usuario mediante su id en base de datos
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findById(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => ''
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                $this->successStatus);
        }

    
        $user = null;

        if($request->route('id')){

            $user = User::find($request->route('id'));
        }else{
            $userToken = $request->user();

            $user = User::find($userToken->id);
        }

        
        if($user != null){

            unset($user->token_activator);
            unset($user->token_forgotten);
            unset($user->updated_at);
            unset($user->created_at);

            $roles = new Roles(); 
            $tmpRol = $user->rol;
            unset($user->rol);

            $rol = $roles->findById($user->id);
            
            if($rol != null){
                $user->rol = (object) 
                    array('id' => $rol->getId(),
                        'name' => $rol->getName(),
                    'level' => $rol->getLevel());
            }

            if($user->provinces_id){
                $user->province = Province::find($user->provinces_id);
            }


            return response()->json(['success' => $user],
                $this->successStatus);
        }else{
            return response()->json(['error' => "No se encuentra el usuario buscado"],
                $this->successStatus);
        }

    }


    public function deactivate(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'required|integer',
            'active' => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                $this->successStatus);
        }


        $id_param = $request->route('id');

        $user = User::find($id_param);

        if($user != null){
            if($request->get('active') != null){

                if($request->get('active') == 1){
                    $user->active = true;
                }else{
                    $user->active = false;
                }
            }else{
                $user->active = false;

            }
            $user->save();
            return response()->json(['success' => "usuario desactivado de manera correcta"],
                $this->successStatus);
        }else{
            return response()->json(['error' => "No se encuentra el usuario buscado"],
                $getAllthis->successStatus);
        }

    }


    public function update(Request $request){

        $validator = Validator::make(  $request->route()->parameters(), [
            'id' => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                $this->successStatus);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'max:255|min:4',
            'favorite_payment'=> 'integer',
            'email' => 'email|max:255|min:3',
            'sur_name' => 'min:4',
            'password' => 'max:255|min:3',
            'c_password' => 'same:password',
            "distrito" => "min:3|max:255",
            "identification" => "max:16",
            "img" => "max:10000000",
            "type_img" => "",
            "phone" => "max:255",
            "type_identification"=>'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()],
                $this->successStatus);
        }

        
        $user = null;
        if($request->route('id')){
            $id_param = $request->route('id');
            $user = User::find($id_param);
        }else{
            $userToken = $request->user();
            $user = User::find($userToken->id);
        }

        $name_param = $request->get("name");

        if($user != null){

            if($name_param  !=  null ){
                $user->name = $name_param;
            }

            if($request->get("favorite_payment")){
                $user->favorite_payment = $request->get("favorite_payment");
            }


            if($request->get("password")){
                $user->password = bcrypt($request->get("password"));
            }


            if($request->get("sur_name")){
                $user->sur_name = $request->get("sur_name");
            }


            if($request->get("distrito")){
                $user->distrito = $request->get("distrito");
            }


            if($request->get("identification")){
                $user->identification = $request->get("identification");
            }

            if($request->get("email")){
                $user->email = $request->get("email");
            }

            if($request->get("provinces_id")){
                $user->provinces_id = $request->get("provinces_id");
            }

            if($request->get("img") && $request->get("type_img")){
                $imagenWorker = new ImagenWorker();
                $user->img = $imagenWorker->saveImg($input['img'] ,"user", $request->type_img);
            }



            if($request->get("phone")){
                $user->phone = $request->get("phone");
            }

            if($request->get("type_identification")){
                $user->type_identification = $request->get("type_identification");
            }


            $user->save();
            return response()->json(['success' => "usuario actualizado de manera correcta"],
                $this->successStatus);
        }else{
            return response()->json(['error' => "No se encuentra el usuario buscado"],
                $this->successStatus);
        }
    }



    /**
     * solo para efectos de prueba
     */
    public function prueba(){

        $generated = new Roles();

        return response()->json(['prueba' => $generated->findById(3)->getName()],
            $this->successStatus);
    }

}
