<?php

namespace App;

use illuminate\database\eloquent\model;
use illuminate\notifications\notifiable;


class Payment extends Model
{
    use  notifiable;

    /**
     * the attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * the attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
