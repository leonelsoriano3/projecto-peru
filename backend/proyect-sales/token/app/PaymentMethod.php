<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use illuminate\notifications\notifiable;

class PaymentMethod extends Model
{
    use  notifiable;

    /**
     * the attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'end_point', 'active'
    ];

    /**
     * the attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
