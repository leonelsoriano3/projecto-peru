<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use illuminate\notifications\notifiable;

class Price extends Model
{
    use  notifiable;

    /**
     * the attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'amount'
    ];

    /**
     * the attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
