<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Transport extends Model
{



    use  Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','delay', 'active', 'free_sending',
        'logo', 'tracing','weight_tax_max', 'tax_id',
        'weight_tax_min', 'billing_type', 'width_package',
       'high_package', 'long_package', 'weight_package' 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];



}
