<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarEstatusUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('active')->default(false)
                ->comment('Estatus del usuario para el sistema');

            $table->string('token_activator', '6')->nullable()->
                comment('Al crear el usuario crea esto esto es lo que lo activa');

            $table->string('token_forgotten', '8')->nullable()->
            comment('token para recuperacion de contraseñas perdidas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['active',"token_activator", "token_forgotten"]);


        });
    }
}
