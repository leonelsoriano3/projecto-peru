<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCaterorySubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_caterory_sub_categories', function (Blueprint $table) {
            
            $table->increments('id');

            $table->integer('product_id')->unsigned();

            $table->foreign('product_id')
                ->references('id')->on('products');

            $table->integer('category_id')->unsigned();

            $table->foreign('category_id')
                ->references('id')->on('categories');

            $table->integer('sub_category_id')->unsigned()->nullable();

            $table->foreign('sub_category_id')
                ->references('id')->on('sub_categories'); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_caterory_sub_categories');
    }
}
