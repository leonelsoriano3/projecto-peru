<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('description', 1024);
            $table->timestamps();
        });


        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->double('tip',15,8)->default(0);
            $table->float('discount')->default(0)->comment('este es el porcentaje');            
            
            $table->integer('seller_id')->unsigned();

            $table->foreign('seller_id')->references('id')->on('users');

            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('payment_id')->unsigned();

            $table->foreign('payment_id')->references('id')->on('users');

            $table->boolean('active')->default(true);

            $table->timestamps();
        });         


        Schema::table('users', function (Blueprint $table) {
            $table->integer('favorite_payment')->unsigned()->nullable()->comment('pago favorito');


            $table->foreign('favorite_payment')->references('id')->on('payments');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('favorite_payment');
            $table->dropColumn('favorite_payment');
        
        });
        Schema::dropIfExists('sales');
        Schema::dropIfExists('payment');

    }
}
