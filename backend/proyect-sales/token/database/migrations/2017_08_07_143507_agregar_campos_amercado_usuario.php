<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarCamposAmercadoUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {




        Schema::create('regions', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name', 255);

        });

        Schema::create('provinces', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name', 255);
            $table->string('regions_id');
            $table->foreign('regions_id')->references('id')->on('regions');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('sur_name', 255)->nulleable()->default('');
            $table->string('provinces_id')->nullable();
            $table->foreign('provinces_id')->references('id')->on('provinces');
            $table->string('phone', 16)->nullable();
            $table->string('auth-facebook',255)->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('region_id');
            $table->dropColumn('region_id');
            $table->dropColumn('phone');
            $table->dropColumn('auth-facebook');
            $table->dropColumn('sur_name');
        });
        Schema::dropIfExists('regions');
        Schema::dropIfExists('provinces');
      
    }
}
