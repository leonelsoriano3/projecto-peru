<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name','255');
            $table->integer('delay')->unsigned()
                ->comment('del 1 al 9 para ver un promedio de velocidad');
            $table->boolean('active')->default(true)
                ->comment('Estado del transporte');
            $table->boolean('free_sending')->default(true)
                ->comment('para saber si es gratiuito el envio o no');
            $table->integer('logo')->unsigned()
                ->comment('esto es para indicar un icono al backend');
            $table->string('tracing','255')->default('')
                ->comment('esto es para la traza de vehiculo del gps');

            
            $table->integer('tax_id')->unsigned()
                ->commet('este es el tipo de tax si es 
                0 el no tendra impuesto lo demas searan
                 agregados a una tabla luego');

            $table->double('weight_tax_min', 15, 8)
                ->comment('si esta en 0 no se toma en cuenta ');
            $table->double('weight_tax_max', 15, 8);
            
            $table->integer('billing_type')->unsigned()
                ->comment('tipo de faturacion
                1 para precio total 2 para peso ');
            //$table->double('tax', 15, 8);
            
            $table->double('width_package');

            $table->double('high_package');

            $table->double('long_package');

            $table->double('weight_package');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transports');
    }
}
