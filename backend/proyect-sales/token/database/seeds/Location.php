<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Location extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('provinces')->delete();
        DB::table('regions')->delete();
        

        DB::table('regions')->insert([
            'id' => '010000',
            'name' => 'Amazonas'
        ]);

        DB::table('provinces')->insert([
            'id' => '010100',
            'name' => 'Chachapoyas',
            'regions_id' => '010000'
        ]);

        DB::table('provinces')->insert([
            'id' => '010200',
            'name' => 'Bagua',
            'regions_id' => '010000'
        ]);




        DB::table('provinces')->insert([
            'id' => '010300',
            'name' => 'Bongará',
            'regions_id' => '010000'
        ]);

        DB::table('provinces')->insert([
            'id' => '010400',
            'name' => 'Condorcanqui',
            'regions_id' => '010000'
        ]);

        DB::table('provinces')->insert([
            'id' => '010500',
            'name' => 'Luya',
            'regions_id' => '010000'
        ]);

        DB::table('provinces')->insert([
            'id' => '010600',
            'name' => 'Rodríguez de Mendoza',
            'regions_id' => '010000'
        ]);

        DB::table('provinces')->insert([
            'id' => '010700',
            'name' => 'Utcubamba',
            'regions_id' => '010000'
        ]);


        DB::table('regions')->insert([
            'id' => '020000',
            'name' => 'Áncash'
        ]);

        DB::table('provinces')->insert([
            'id' => '020100',
            'name' => 'Huaraz',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '020200',
            'name' => 'Aija',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '020300',
            'name' => 'Antonio Raymondi',
            'regions_id' => '020000'
        ]);


        DB::table('provinces')->insert([
            'id' => '020400',
            'name' => 'Asunción',
            'regions_id' => '020000'
        ]);


        DB::table('provinces')->insert([
            'id' => '020500',
            'name' => 'Bolognesi',
            'regions_id' => '020000'
        ]);


        DB::table('provinces')->insert([
            'id' => '020600',
            'name' => 'Carhuaz',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '020700',
            'name' => 'Carlos Fermín Fitzcarrald',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '020800',
            'name' => 'Casma',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '020900',
            'name' => 'Corongo',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '021000',
            'name' => 'Huari',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '021100',
            'name' => 'Huarmey',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '021200',
            'name' => 'Huaylas',
            'regions_id' => '020000'
        ]);


        DB::table('provinces')->insert([
            'id' => '021300',
            'name' => 'Mariscal Luzuriaga',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '021400',
            'name' => 'Ocros',
            'regions_id' => '020000'
        ]);


        DB::table('provinces')->insert([
            'id' => '021500',
            'name' => 'Pallasca',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '021600',
            'name' => 'Pomabamba',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '021700',
            'name' => 'Recuay',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '021800',
            'name' => 'Santa',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '021900',
            'name' => 'Sihuas',
            'regions_id' => '020000'
        ]);

        DB::table('provinces')->insert([
            'id' => '022000',
            'name' => 'Yungay',
            'regions_id' => '020000'
        ]);

         DB::table('regions')->insert([
            'id' => '030000',
            'name' => 'Apurímac'
        ]);

         DB::table('provinces')->insert([
            'id' => '030100',
            'name' => 'Abancay',
            'regions_id' => '030000'
        ]);

      	DB::table('provinces')->insert([
            'id' => '030200',
            'name' => 'Andahuaylas',
            'regions_id' => '030000'
        ]);


      	DB::table('provinces')->insert([
            'id' => '030300',
            'name' => 'Antabamba',
            'regions_id' => '030000'
        ]);


      	DB::table('provinces')->insert([
            'id' => '030400',
            'name' => 'Aymaraes',
            'regions_id' => '030000'
        ]);

      	DB::table('provinces')->insert([
            'id' => '030500',
            'name' => 'Cotabambas',
            'regions_id' => '030000'
        ]);

      	DB::table('provinces')->insert([
            'id' => '030600',
            'name' => 'Chncheros',
            'regions_id' => '030000'
        ]);

      	DB::table('provinces')->insert([
            'id' => '030700',
            'name' => 'Grau',
            'regions_id' => '030000'
        ]);


        DB::table('regions')->insert([
            'id' => '040000',
            'name' => 'Arequipa'
        ]);

      	DB::table('provinces')->insert([
            'id' => '040100',
            'name' => 'Arequipa',
            'regions_id' => '040000'
        ]);

      	DB::table('provinces')->insert([
            'id' => '040200',
            'name' => 'Camaná',
            'regions_id' => '040000'
        ]);

      	DB::table('provinces')->insert([
            'id' => '040300',
            'name' => 'Caravelí',
            'regions_id' => '040000'
        ]);


      	DB::table('provinces')->insert([
            'id' => '040400',
            'name' => 'Castilla',
            'regions_id' => '040000'
        ]);


      	DB::table('provinces')->insert([
            'id' => '040500',
            'name' => 'Caylloma',
            'regions_id' => '040000'
        ]);


      	DB::table('provinces')->insert([
            'id' => '040600',
            'name' => 'Condesuyos',
            'regions_id' => '040000'
        ]);


      	DB::table('provinces')->insert([
            'id' => '040700',
            'name' => 'Islay',
            'regions_id' => '040000'
        ]);

      	DB::table('provinces')->insert([
            'id' => '040800',
            'name' => 'La Unión',
            'regions_id' => '040000'
        ]);


        DB::table('regions')->insert([
            'id' => '050000',
            'name' => 'Ayacucho'
        ]);


      	DB::table('provinces')->insert([
            'id' => '050100',
            'name' => 'Humanga',
            'regions_id' => '050000'
        ]);

      	DB::table('provinces')->insert([
            'id' => '0502100',
            'name' => 'Can gallo',
            'regions_id' => '050000'
        ]);


      	DB::table('provinces')->insert([
            'id' => '050300',
            'name' => 'Huanca Sancos',
            'regions_id' => '050000'
        ]);

        DB::table('provinces')->insert([
            'id' => '050400',
            'name' => 'Huanta',
            'regions_id' => '050000'
        ]);

        DB::table('provinces')->insert([
            'id' => '050500',
            'name' => 'La Mar',
            'regions_id' => '050000'
        ]);

        DB::table('provinces')->insert([
            'id' => '050600',
            'name' => 'Lucanas',
            'regions_id' => '050000'
        ]);

        DB::table('provinces')->insert([
            'id' => '050700',
            'name' => 'Parinacochas',
            'regions_id' => '050000'
        ]);

        DB::table('provinces')->insert([
            'id' => '050800',
            'name' => 'Páucar del Sara Sara',
            'regions_id' => '050000'
        ]);


        DB::table('provinces')->insert([
            'id' => '050900',
            'name' => 'Sucre',
            'regions_id' => '050000'
        ]);

        DB::table('provinces')->insert([
            'id' => '051000',
            'name' => 'Víctor Fajardo',
            'regions_id' => '050000'
        ]);


        DB::table('provinces')->insert([
            'id' => '051100',
            'name' => 'Vilcas Huamán',
            'regions_id' => '050000'
        ]);


        DB::table('regions')->insert([
            'id' => '060000',
            'name' => 'Cajamarca'
        ]);

        DB::table('provinces')->insert([
            'id' => '060100',
            'name' => 'Cajamarca',
            'regions_id' => '060000'
        ]);

        DB::table('provinces')->insert([
            'id' => '060200',
            'name' => 'Cajabamba',
            'regions_id' => '060000'
        ]);

        DB::table('provinces')->insert([
            'id' => '060300',
            'name' => 'Celedín',
            'regions_id' => '060000'
        ]);


        DB::table('provinces')->insert([
            'id' => '060400',
            'name' => 'Chota',
            'regions_id' => '060000'
        ]);

        DB::table('provinces')->insert([
            'id' => '060500',
            'name' => 'Contumaza',
            'regions_id' => '060000'
        ]);


        DB::table('provinces')->insert([
            'id' => '060600',
            'name' => 'Cutervo',
            'regions_id' => '060000'
        ]);


        DB::table('provinces')->insert([
            'id' => '060700',
            'name' => 'Hualgayoc',
            'regions_id' => '060000'
        ]);

        DB::table('provinces')->insert([
            'id' => '060800',
            'name' => 'Jaén',
            'regions_id' => '060000'
        ]);

        DB::table('provinces')->insert([
            'id' => '060900',
            'name' => 'San Ignacio',
            'regions_id' => '060000'
        ]);

        DB::table('provinces')->insert([
            'id' => '061000',
            'name' => 'San Marcos',
            'regions_id' => '060000'
        ]);

        DB::table('provinces')->insert([
            'id' => '061100',
            'name' => 'San Miguel',
            'regions_id' => '060000'
        ]);

        DB::table('provinces')->insert([
            'id' => '061200',
            'name' => 'San Pablo',
            'regions_id' => '060000'
        ]);


        DB::table('provinces')->insert([
            'id' => '061300',
            'name' => 'San Cruz',
            'regions_id' => '060000'
        ]);


        DB::table('regions')->insert([
            'id' => '070000',
            'name' => 'Callao'
        ]);

        DB::table('provinces')->insert([
            'id' => '070100',
            'name' => 'Prov Const del Callao',
            'regions_id' => '070000'
        ]);


        DB::table('regions')->insert([
            'id' => '080000',
            'name' => 'Cusco'
        ]);

        DB::table('provinces')->insert([
            'id' => '080100',
            'name' => 'Cusco',
            'regions_id' => '080000'
        ]);

        DB::table('provinces')->insert([
            'id' => '080200',
            'name' => 'Acomayo',
            'regions_id' => '080000'
        ]);

        DB::table('provinces')->insert([
            'id' => '080300',
            'name' => 'Anta',
            'regions_id' => '080000'
        ]);


        DB::table('provinces')->insert([
            'id' => '080400',
            'name' => 'Calca',
            'regions_id' => '080000'
        ]);


        DB::table('provinces')->insert([
            'id' => '080500',
            'name' => 'Canas',
            'regions_id' => '080000'
        ]);

        DB::table('provinces')->insert([
            'id' => '080600',
            'name' => 'Canchis',
            'regions_id' => '080000'
        ]);


        DB::table('provinces')->insert([
            'id' => '080700',
            'name' => 'Chumbivileas',
            'regions_id' => '080000'
        ]);

        DB::table('provinces')->insert([
            'id' => '080800',
            'name' => 'Espinar',
            'regions_id' => '080000'
        ]);

        DB::table('provinces')->insert([
            'id' => '080900',
            'name' => 'La Convención',
            'regions_id' => '080000'
        ]);


        DB::table('provinces')->insert([
            'id' => '081000',
            'name' => 'Paruro',
            'regions_id' => '080000'
        ]);

        DB::table('provinces')->insert([
            'id' => '081100',
            'name' => 'Paucartambo',
            'regions_id' => '080000'
        ]);


        DB::table('provinces')->insert([
            'id' => '081200',
            'name' => 'Quispicachi',
            'regions_id' => '080000'
        ]);

        DB::table('provinces')->insert([
            'id' => '081300',
            'name' => 'Hurubamba',
            'regions_id' => '080000'
        ]);

        DB::table('regions')->insert([
            'id' => '090000',
            'name' => 'Huancavelica'
        ]);

        DB::table('provinces')->insert([
            'id' => '090100',
            'name' => 'Huancavelica',
            'regions_id' => '090000'
        ]);

        DB::table('provinces')->insert([
            'id' => '090200',
            'name' => 'Acobamba',
            'regions_id' => '090000'
        ]);

        DB::table('provinces')->insert([
            'id' => '090300',
            'name' => 'Angaraes',
            'regions_id' => '090000'
        ]);

        DB::table('provinces')->insert([
            'id' => '090400',
            'name' => 'Castrovirreyna',
            'regions_id' => '090000'
        ]);


        DB::table('provinces')->insert([
            'id' => '090500',
            'name' => 'Churcampa',
            'regions_id' => '090000'
        ]);


        DB::table('provinces')->insert([
            'id' => '090600',
            'name' => 'Huaytará',
            'regions_id' => '090000'
        ]);


        DB::table('provinces')->insert([
            'id' => '090700',
            'name' => 'Tayacaja',
            'regions_id' => '090000'
        ]);


        DB::table('regions')->insert([
            'id' => '100000',
            'name' => 'Huánuco'
        ]);

        DB::table('provinces')->insert([
            'id' => '100100',
            'name' => 'Huánuco',
            'regions_id' => '100000'
        ]);

        DB::table('provinces')->insert([
            'id' => '100200',
            'name' => 'Ambo',
            'regions_id' => '100000'
        ]);	

        DB::table('provinces')->insert([
            'id' => '100300',
            'name' => 'Dos de Mayo',
            'regions_id' => '100000'
        ]);	

        DB::table('provinces')->insert([
            'id' => '100400',
            'name' => 'Huacaybamba',
            'regions_id' => '100000'
        ]);	

        DB::table('provinces')->insert([
            'id' => '100500',
            'name' => 'Huamalíes',
            'regions_id' => '100000'
        ]);	

        DB::table('provinces')->insert([
            'id' => '100600',
            'name' => 'Leoncio Prado',
            'regions_id' => '100000'
        ]);

        DB::table('provinces')->insert([
            'id' => '100700',
            'name' => 'Marañon',
            'regions_id' => '100000'
        ]);	

        DB::table('provinces')->insert([
            'id' => '100800',
            'name' => 'Pachitea',
            'regions_id' => '100000'
        ]);

        DB::table('provinces')->insert([
            'id' => '100900',
            'name' => 'Puerto Inca',
            'regions_id' => '100000'
        ]);


        DB::table('provinces')->insert([
            'id' => '101000',
            'name' => 'Lauricocha',
            'regions_id' => '100000'
        ]);

        DB::table('provinces')->insert([
            'id' => '101100',
            'name' => 'Yarowilea',
            'regions_id' => '100000'
        ]);

        DB::table('regions')->insert([
            'id' => '110000',
            'name' => 'Ica'
        ]);

        DB::table('provinces')->insert([
            'id' => '110100',
            'name' => 'Ica',
            'regions_id' => '110000'
        ]);

        DB::table('provinces')->insert([
            'id' => '110200',
            'name' => 'Chincha',
            'regions_id' => '110000'
        ]);

        DB::table('provinces')->insert([
            'id' => '110300',
            'name' => 'Nazca',
            'regions_id' => '110000'
        ]);

        DB::table('provinces')->insert([
            'id' => '110400',
            'name' => 'Palpa',
            'regions_id' => '110000'
        ]);

        DB::table('provinces')->insert([
            'id' => '110500',
            'name' => 'Pisco',
            'regions_id' => '110000'
        ]);


        DB::table('regions')->insert([
            'id' => '120000',
            'name' => 'Junín'
        ]);

        DB::table('provinces')->insert([
            'id' => '120100',
            'name' => 'Huancayo',
            'regions_id' => '120000'
        ]);

        DB::table('provinces')->insert([
            'id' => '120200',
            'name' => 'Concepción',
            'regions_id' => '120000'
        ]);

        DB::table('provinces')->insert([
            'id' => '120300',
            'name' => 'Chanchamayo',
            'regions_id' => '120000'
        ]);

        DB::table('provinces')->insert([
            'id' => '120400',
            'name' => 'Jauja',
            'regions_id' => '120000'
        ]);

        DB::table('provinces')->insert([
            'id' => '120500',
            'name' => 'Junín',
            'regions_id' => '120000'
        ]);

        DB::table('provinces')->insert([
            'id' => '120600',
            'name' => 'Satipo',
            'regions_id' => '120000'
        ]);

        DB::table('provinces')->insert([
            'id' => '120700',
            'name' => 'Tama',
            'regions_id' => '120000'
        ]);

        DB::table('provinces')->insert([
            'id' => '120800',
            'name' => 'Yauli',
            'regions_id' => '120000'
        ]);

        DB::table('provinces')->insert([
            'id' => '120900',
            'name' => 'Chupaca',
            'regions_id' => '120000'
        ]);

        DB::table('regions')->insert([
            'id' => '130000',
            'name' => 'La libertad'
        ]);

        DB::table('provinces')->insert([
            'id' => '130100',
            'name' => 'Trujillo',
            'regions_id' => '130000'
        ]);

        DB::table('provinces')->insert([
            'id' => '130200',
            'name' => 'Ascope',
            'regions_id' => '130000'
        ]);

        DB::table('provinces')->insert([
            'id' => '130300',
            'name' => 'Bolivar',
            'regions_id' => '130000'
        ]);

        DB::table('provinces')->insert([
            'id' => '130400',
            'name' => 'Chepen',
            'regions_id' => '130000'
        ]);

        DB::table('provinces')->insert([
            'id' => '130500',
            'name' => 'Julcán',
            'regions_id' => '130000'
        ]);

        DB::table('provinces')->insert([
            'id' => '130600',
            'name' => 'Otuzco',
            'regions_id' => '130000'
        ]);

        DB::table('provinces')->insert([
            'id' => '130700',
            'name' => 'Pacasmayo',
            'regions_id' => '130000'
        ]);


        DB::table('provinces')->insert([
            'id' => '130800',
            'name' => 'Pataz',
            'regions_id' => '130000'
        ]);

        DB::table('provinces')->insert([
            'id' => '130900',
            'name' => 'Sánchez carrión',
            'regions_id' => '130000'
        ]);

        DB::table('provinces')->insert([
            'id' => '131000',
            'name' => 'Santiago de Chuco',
            'regions_id' => '130000'
        ]);


        DB::table('provinces')->insert([
            'id' => '131100',
            'name' => 'Gran Chimú',
            'regions_id' => '130000'
        ]);

        DB::table('provinces')->insert([
            'id' => '131200',
            'name' => 'Virú',
            'regions_id' => '130000'
        ]);


        DB::table('regions')->insert([
            'id' => '140000',
            'name' => 'Lambayeque'
        ]);

		DB::table('provinces')->insert([
            'id' => '140100',
            'name' => 'Chiclayo',
            'regions_id' => '140000'
        ]);


		DB::table('provinces')->insert([
            'id' => '140200',
            'name' => 'Ferrenafe',
            'regions_id' => '140000'
        ]);

        DB::table('provinces')->insert([
            'id' => '140300',
            'name' => 'Lambayeque',
            'regions_id' => '140000'
        ]);

        DB::table('regions')->insert([
            'id' => '150000',
            'name' => 'Lima'
        ]);

		DB::table('provinces')->insert([
            'id' => '150100',
            'name' => 'Lima',
            'regions_id' => '150000'
        ]);

        DB::table('provinces')->insert([
            'id' => '150200',
            'name' => 'Barranca',
            'regions_id' => '150000'
        ]);


 		DB::table('provinces')->insert([
            'id' => '150300',
            'name' => 'Cajatambo',
            'regions_id' => '150000'
        ]);

        DB::table('provinces')->insert([
            'id' => '150400',
            'name' => 'Canta',
            'regions_id' => '150000'
        ]);

		DB::table('provinces')->insert([
            'id' => '150500',
            'name' => 'Cañete',
            'regions_id' => '150000'
        ]);

        DB::table('provinces')->insert([
            'id' => '150600',
            'name' => 'Huaral',
            'regions_id' => '150000'
        ]);

        DB::table('provinces')->insert([
            'id' => '150700',
            'name' => 'Huarochirí',
            'regions_id' => '150000'
        ]);


        DB::table('provinces')->insert([
            'id' => '150800',
            'name' => 'Huaura',
            'regions_id' => '150000'
        ]);

        DB::table('provinces')->insert([
            'id' => '150900',
            'name' => 'Oyón',
            'regions_id' => '150000'
        ]);


        DB::table('provinces')->insert([
            'id' => '151000',
            'name' => 'Yauyos',
            'regions_id' => '150000'
        ]);


        DB::table('regions')->insert([
            'id' => '160000',
            'name' => 'Loreto'
        ]);
 
         DB::table('provinces')->insert([
            'id' => '160100',
            'name' => 'Maynas',
            'regions_id' => '160000'
        ]);


        DB::table('provinces')->insert([
            'id' => '160200',
            'name' => 'Alto Amazonas',
            'regions_id' => '160000'
        ]);


        DB::table('provinces')->insert([
            'id' => '160300',
            'name' => 'Loreto',
            'regions_id' => '160000'
        ]);


        DB::table('provinces')->insert([
            'id' => '160400',
            'name' => 'Mariscal Ramón',
            'regions_id' => '160000'
        ]);



        DB::table('provinces')->insert([
            'id' => '160500',
            'name' => 'Requena',
            'regions_id' => '160000'
        ]);

        DB::table('provinces')->insert([
            'id' => '160600',
            'name' => 'Ucayali',
            'regions_id' => '160000'
        ]);

       DB::table('provinces')->insert([
            'id' => '160700',
            'name' => 'Datem del Marañon',
            'regions_id' => '160000'
        ]);


        DB::table('provinces')->insert([
            'id' => '160800',
            'name' => 'Putumayo',
            'regions_id' => '160000'
        ]);

        DB::table('regions')->insert([
            'id' => '170000',
            'name' => 'Madre de Dios'
        ]);

        DB::table('provinces')->insert([
            'id' => '170100',
            'name' => 'Tambopata',
            'regions_id' => '170000'
        ]);

        DB::table('provinces')->insert([
            'id' => '170200',
            'name' => 'Manu',
            'regions_id' => '170000'
        ]);

        DB::table('provinces')->insert([
            'id' => '170300',
            'name' => 'Tahuamanu',
            'regions_id' => '170000'
        ]);

       DB::table('regions')->insert([
            'id' => '180000',
            'name' => 'Moquegua'
        ]);

        DB::table('provinces')->insert([
            'id' => '180100',
            'name' => 'Mariscal Nieto',
            'regions_id' => '180000'
        ]);

        DB::table('provinces')->insert([
            'id' => '180200',
            'name' => 'General Sánchez',
            'regions_id' => '180000'
        ]);


        DB::table('provinces')->insert([
            'id' => '180300',
            'name' => 'Llo',
            'regions_id' => '180000'
        ]);

        DB::table('regions')->insert([
            'id' => '190000',
            'name' => 'Pasco'
        ]);

        DB::table('provinces')->insert([
            'id' => '190100',
            'name' => 'Pasco',
            'regions_id' => '190000'
        ]);


        DB::table('provinces')->insert([
            'id' => '190200',
            'name' => 'Daniel Alcides',
            'regions_id' => '190000'
        ]);

        DB::table('provinces')->insert([
            'id' => '190300',
            'name' => 'Oxapampa',
            'regions_id' => '190000'
        ]);

        DB::table('provinces')->insert([
            'id' => '190400',
            'name' => 'Pasco',
            'regions_id' => '190000'
        ]);

        DB::table('regions')->insert([
            'id' => '200000',
            'name' => 'Piura'
        ]);

        DB::table('provinces')->insert([
            'id' => '200100',
            'name' => 'Piura',
            'regions_id' => '200000'
        ]);


        DB::table('provinces')->insert([
            'id' => '200200',
            'name' => 'Ayabaca',
            'regions_id' => '200000'
        ]);


        DB::table('provinces')->insert([
            'id' => '200300',
            'name' => 'Huancabamba',
            'regions_id' => '200000'
        ]);


        DB::table('provinces')->insert([
            'id' => '200400',
            'name' => 'Morropón',
            'regions_id' => '200000'
        ]);

        DB::table('provinces')->insert([
            'id' => '200500',
            'name' => 'Paita',
            'regions_id' => '200000'
        ]);


      DB::table('provinces')->insert([
            'id' => '200600',
            'name' => 'Sullana',
            'regions_id' => '200000'
        ]);


      DB::table('provinces')->insert([
            'id' => '200700',
            'name' => 'Talara',
            'regions_id' => '200000'
        ]);

      DB::table('provinces')->insert([
            'id' => '200800',
            'name' => 'Sechura',
            'regions_id' => '200000'
        ]);

        DB::table('regions')->insert([
            'id' => '210000',
            'name' => 'Puno'
        ]);

        DB::table('provinces')->insert([
            'id' => '210100',
            'name' => 'Puno',
            'regions_id' => '210000'
        ]);


        DB::table('provinces')->insert([
            'id' => '210200',
            'name' => 'Azángaro',
            'regions_id' => '210000'
        ]);

        DB::table('provinces')->insert([
            'id' => '210300',
            'name' => 'Carabaya',
            'regions_id' => '210000'
        ]);

        DB::table('provinces')->insert([
            'id' => '210400',
            'name' => 'Chucuito',
            'regions_id' => '210000'
        ]);

        DB::table('provinces')->insert([
            'id' => '210500',
            'name' => 'El Collao',
            'regions_id' => '210000'
        ]);

        DB::table('provinces')->insert([
            'id' => '210600',
            'name' => 'Huancané',
            'regions_id' => '210000'
        ]);

        DB::table('provinces')->insert([
            'id' => '210700',
            'name' => 'Lampa',
            'regions_id' => '210000'
        ]);

        DB::table('provinces')->insert([
            'id' => '210800',
            'name' => 'Melgar',
            'regions_id' => '210000'
        ]);


        DB::table('provinces')->insert([
            'id' => '210900',
            'name' => 'Moho',
            'regions_id' => '210000'
        ]);

        DB::table('provinces')->insert([
            'id' => '211000',
            'name' => 'San Antonio de Putina',
            'regions_id' => '210000'
        ]);

        DB::table('provinces')->insert([
            'id' => '211100',
            'name' => 'San Román',
            'regions_id' => '210000'
        ]);

        DB::table('provinces')->insert([
            'id' => '211200',
            'name' => 'Sandia',
            'regions_id' => '210000'
        ]);

        DB::table('provinces')->insert([
            'id' => '211300',
            'name' => 'Yunguyo',
            'regions_id' => '210000'
        ]);


        DB::table('regions')->insert([
            'id' => '220000',
            'name' => 'San Martín'
        ]);


        DB::table('provinces')->insert([
            'id' => '220100',
            'name' => 'Moyobamba',
            'regions_id' => '220000'
        ]);

        DB::table('provinces')->insert([
            'id' => '220200',
            'name' => 'Bellavista',
            'regions_id' => '220000'
        ]);

        DB::table('provinces')->insert([
            'id' => '220300',
            'name' => 'El Dorado',
            'regions_id' => '220000'
        ]);

        DB::table('provinces')->insert([
            'id' => '220400',
            'name' => 'Huallaga',
            'regions_id' => '220000'
        ]);


        DB::table('provinces')->insert([
            'id' => '220500',
            'name' => 'Lamas',
            'regions_id' => '220000'
        ]);

        DB::table('provinces')->insert([
            'id' => '220600',
            'name' => 'Mariscal Cáceres',
            'regions_id' => '220000'
        ]);

        DB::table('provinces')->insert([
            'id' => '220700',
            'name' => 'Picota',
            'regions_id' => '220000'
        ]);

        DB::table('provinces')->insert([
            'id' => '220800',
            'name' => 'Rioja',
            'regions_id' => '220000'
        ]);

        DB::table('provinces')->insert([
            'id' => '220900',
            'name' => 'San Martín',
            'regions_id' => '220000'
        ]);

        DB::table('provinces')->insert([
            'id' => '221000',
            'name' => 'Tocache',
            'regions_id' => '220000'
        ]);

        DB::table('regions')->insert([
            'id' => '230000',
            'name' => 'Tacna'
        ]);

        DB::table('provinces')->insert([
            'id' => '230100',
            'name' => 'Tacna',
            'regions_id' => '230000'
        ]);

        DB::table('provinces')->insert([
            'id' => '230200',
            'name' => 'Candarave',
            'regions_id' => '230000'
        ]);


        DB::table('provinces')->insert([
            'id' => '230300',
            'name' => 'Jorge Basadre',
            'regions_id' => '230000'
        ]);

        DB::table('provinces')->insert([
            'id' => '230400',
            'name' => 'Tarata',
            'regions_id' => '230000'
        ]);


        DB::table('regions')->insert([
            'id' => '240000',
            'name' => 'Tumbes'
        ]);


        DB::table('provinces')->insert([
            'id' => '240100',
            'name' => 'Tumbes',
            'regions_id' => '240000'
        ]);

        DB::table('provinces')->insert([
            'id' => '240200',
            'name' => 'Contralmirante Villar',
            'regions_id' => '240000'
        ]);

        DB::table('provinces')->insert([
            'id' => '240300',
            'name' => 'Zarumilla',
            'regions_id' => '240000'
        ]);

        DB::table('regions')->insert([
            'id' => '250000',
            'name' => 'Ucayali'
        ]);

        DB::table('provinces')->insert([
            'id' => '250100',
            'name' => 'Coronel Portillo',
            'regions_id' => '250000'
        ]);

        DB::table('provinces')->insert([
            'id' => '250200',
            'name' => 'Atalaya',
            'regions_id' => '250000'
        ]);


        DB::table('provinces')->insert([
            'id' => '250300',
            'name' => 'Padre Abad',
            'regions_id' => '250000'
        ]);

        DB::table('provinces')->insert([
            'id' => '250400',
            'name' => 'Purús',
            'regions_id' => '250000'
        ]);
    }
}
