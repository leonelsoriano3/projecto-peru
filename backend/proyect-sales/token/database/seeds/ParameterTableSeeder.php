<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ParameterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
      DB::table('parameters')->delete(); 

      DB::table('parameters')->insert([
        'id' => 1,
        'name' => 'Cobro de envio',
        'level' => 1,
        'description' => 'Se cobrara por envio del producto',
        'active' => true
      ]);
    }
}
