<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PaymentMethodSeeder extends Seeder
{     

//  php artisan make:seeder UsersTableSeeder
//    php artisan db:seed --class=UsersTableSeeder
//php artisan migrate:refresh --seed


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('payment_methods')->delete(); 

      DB::table('payment_methods')->insert([
        'id' => 1,
        'name' => 'Pago directo',
        'description' => 'Pago de ejemplo',
        'end_point' => 'x',
        'active' => true
      ]);

    }
}
