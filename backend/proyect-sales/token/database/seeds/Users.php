<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Admin',
            'email' => 'ejemplo@ejemplo.com',
            'password' => '$2y$10$wAzKlcitj5U5DXofr8AsPen9DPvLlUEgulbLDEWLL2ozdv.ro08pe',
            'rol' => 1,
            'active' => 1,
            'type_identification' => 1,
            'identification' => '123456',
            'sur_name' => 'Soriano',
            'created_at' => '2017-08-08 23:49:59',
            'updated_at' => '2017-08-08 23:49:59'
        ]);



    }
}
