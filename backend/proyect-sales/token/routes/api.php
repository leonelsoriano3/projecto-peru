<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// CORS
//
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');


header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );


Route::post('login', 'API\UserController@login');

/**
 * registra un usuario para el sistema este envia un token al email que hay q verificar
 *  param
 *      name
 *      email
 *      password
 *      c_password verificacion que este sea igual al otro password
 */
Route::post('register', 'API\UserController@register');





/**
 * activa la cuenta recien creada
 *     param
 *          -> email
 *          -> token luego de crearla
 */
Route::post('user/activateAccount', 'API\UserController@activateAccount');

/**
 * manda un token para verificar la clave del usuario enviada
 *  param
 *      email
 */
Route::post('user/passWordForgotten', 'API\UserController@passWordForgotten');

/**
 * recupera la contraseña del usuario
 *      param
 *          email
 *          token
 *          password
 *          c_password
 */
Route::post('user/recuperate', 'API\UserController@recuperate');



//location
Route::get('location/province/{id}',
    "API\LocationController@provinceFindByid");

Route::get('location/region/',
    "API\LocationController@regionFindAll");

Route::get('location/region/{id}',
    "API\LocationController@regionFindByid");

Route::get('location/province-by-region/{id}',
    "API\LocationController@provinceByRegion");





Route::get('prueba', 'API\UserController@prueba');


Route::group(['middleware' => 'auth:api'], function(){

    /*
     * ruta de usuario
     * 
     */


    Route::post('user/register-admin', 'API\UserController@registerAdmin');



    /**
     * conseguir todos los usuarios
     *    param
     *       active -> false obtiene los no activos, true los activos si no se
     *                  pasa obtiene los dos
     */
    Route::get('user', 'API\UserController@getAll');

    /**
     * conseguir  un usuario mediante su ide que se envia por param de url
     *    param (url)
     *       id -> id de usuario en base de datos
     */
    Route::get('user/{id}', 'API\UserController@findById');

    /**
    *  igual que el user/{id} pero es por el usuario logeado
    */
    Route::get('user-me/', 'API\UserController@findById');


    /**
     * desactiva a un usuario pasado pro url
     *    param (url)
     *       id* -> id de usuario en base de datos
     */
    Route::post('user/desactivate/{id}', 'API\UserController@deactivate');

    /**
     *  modifica el usuario
     *    param (url)
     *       id* -> id de usuario en base de datos
     *    param (body)
     *          email -> email del usuario
     *          password -> contraseña
     *          c_password -> repeticion de la contraseña
     *          name -> nombre de usuario
     */
    Route::put('user/{id}', 'API\UserController@update');
    Route::put('user', 'API\UserController@update');
    
    /**
     *  crea un producto
     *      param (body)
     *          name* -> nombre del producto
     *          description* -> descripcion del producto
     */
    Route::post('product/', 'API\ProductController@create');


    /**
     *  consigue un producto dado su id
     *      para (url)
     *          id*
     */
    Route::get('product/{id}', "API\ProductController@findById");


    /**
     * consigue todos los productos
     *     param (url)
     *         name -> filtra por nombre
     *         active -> filtra por estado del producto valores validos true|false
     *         paga -> consigue una pagina de diez cada una si no se le pasa
     *             consigue todos los productos
     *          
     */
    Route::get('product/', "API\ProductController@findAll");

    /**
     * actualiza los productos
     *     param(url)
     *         id* -> identificaro del producto que se actualizara
     *     para(body)
     *         name nobre -> del producto
     *         description -> descripcion del producto
     *         active -> estado del producto, values permitidos  true|false
     */
    Route::put('product/{id}', "API\ProductController@update");

    /**
     * crea un precio
     *     param(body)
     *         product_id* id del producto que se le asociara el precio 
     *             debe de existir en base de datos
     *         price* precio del producto
     */
    Route::post('price/', "API\PriceController@create");

    /**
     * consigue un precio pro su ide
     *     param(url)
     *         id* identificar del precio
     */
    Route::get('price/{id}', "API\PriceController@findById");

    /**
     * consigue el precio del producto
     *     param(url)
     *         id* identificador del producto
     */
    Route::get('price/product/{id}', "API\PriceController@getPriceProduc");

    /**
    * consigue todos los precios
    *     param(url)
    *         product_id id delproducto
    *         amount_less filtro para no devolver por debajo de este monto
    *         amount_more no puede pasar el precio este valor
    *         page paginado 
    */
    Route::get('price/', "API\PriceController@findAll");

    //Associate

    /**
    * genera un tipo de asociacion
    *     param(body)
    *         name* nombre de la asociacion
    *         description* descripcion de la asociacion          
    */ 
    Route::post('associate/', "API\AssociateController@create");


    /**
     * consigue un asociador por id
     *     param(url)
     *         id* id a buscar en la base de datos del asociador
     */
    Route::get('associate/{id}', "API\AssociateController@findById");

    /**
     * consigue todos los asociadores en base de dato
     *     param(url request)
     *         name nombre por el cual se buscara  
     *         description descripcion por el cual se buscara
     *         page paginado de la peticion
     */
    Route::get('associate/', "API\AssociateController@findAll");

    /**
     * actualiza un asociador dado su id
     *     param(url)
     *         id* en base de datos del asociador a actualizar
     *     param(body)
     *         name nombre del asociador
     *         description descrion del asociador
    */
    Route::put('associate/{id}', "API\AssociateController@update");

    /*
     * elimina de base de datos un asociador
     *     param(url)
     *         id* a eliminar en la base de datos
    */
    Route::delete('associate/{id}', "API\AssociateController@deleteById");


    //productsAsociate

    Route::post('productsAsociate/', "API\ProductsAsociateController@create");
    
    Route::delete('productsAsociate/fromId/{id}', "API\ProductsAsociateController@removeFromId");

    Route::delete('productsAsociate/removeFromAsociate/{id}',
        "API\ProductsAsociateController@removeFromAsociate");

    Route::get('productsAsociate/{id}', "API\ProductsAsociateController@getByIdAsociate");


    //category 
    Route::post('categories/', "API\CategoriesController@create");

    Route::get('categories/{id}', "API\CategoriesController@findById");

    Route::get('categories/', "API\CategoriesController@findAll");

    Route::put('categories/{id}', "API\CategoriesController@update");

    Route::delete('categories/{id}', "API\CategoriesController@deleteById");

    //subCategory
    Route::post('sub-categories/', "API\SubCategoriesController@create");

    Route::get('sub-categories/{id}', "API\SubCategoriesController@findById");

    Route::get('sub-categories/', "API\SubCategoriesController@findAll");

    Route::put('sub-categories/{id}', "API\SubCategoriesController@update");

    Route::delete('sub-categories/{id}', "API\SubCategoriesController@deleteById");


    /**
     * asocia categoria y subcategoria a un producto
     *  param body
     *      product_id* id del producto
     *      category_id* id de la categoria
     *      sub_category_id id de la sub categoria
    */    
    Route::post('product-category-subcategory/',
        "API\ProductCategorySubCategoryController@create");


    /**
     * consigue una catgoria segun el id de la asociacion
     *     param(url)
     *         id* id a buscar en la base de datos
     */
    Route::get('product-category-subcategory/{id}',
        "API\ProductCategorySubCategoryController@findById");

    /**
     * consigue una asociacion segun el ide de su categoria
     *     param(url)
     *     id*"id de la categori que depende la busquda
     */
    Route::get('product-category-subcategory/findByCategory/{id}',
        "API\ProductCategorySubCategoryController@findByCategory");

    /**
    * consigue una asociacion segun el id de su producto
    *     param(url)
    *     id*"id de la categori que depende la busquda
    */
    Route::get('product-category-subcategory/findByProduct/{id}',
        "API\ProductCategorySubCategoryController@findByProduct");


    /**
     * actualiza un producto categoria subcaregoria
     *     param(url)
     *         id* id a buscar en base de datos
     *     param(body)
     *         product_id id del producto a actualizar
     *         category_id id de la categoria a actualizar
     *         sub_category_id id de la subcategoria a actualizar
     */
    Route::put('product-category-subcategory/{id}',
        "API\ProductCategorySubCategoryController@update");

    /**
     * elimina un product-category-subcategory dela base de datos
     *    param(url)
     *        id* id a eliminar en la base de datos
    */
    Route::delete('product-category-subcategory/{id}',
        "API\ProductCategorySubCategoryController@remove");


    //payment

    Route::post('payment/',
        "API\PaymentController@create");

    Route::get('payment/',
        "API\PaymentController@findAll");

    Route::get('payment/{id}',
        "API\PaymentController@findOne");

    Route::put('payment/{id}',
        "API\PaymentController@update");


    //sale
    
    Route::post('sale/',
        "API\SaleController@create");

    Route::put('sale/{id}',
        "API\SaleController@update");

    Route::delete('sale/',
        "API\SaleController@desActivate");

    Route::get('sale/',
        "API\SaleController@findAll");

    Route::get('sale/{id}',
        "API\SaleController@findById");

    //sales price 
    Route::post('sales-price/',
        "API\SalesPriceController@create");

    Route::get('sales-price/{id}',
        "API\SalesPriceController@findById");

    Route::get('sales-price/',
        "API\SalesPriceController@findAll");

    Route::get('sales-price/',
        "API\SalesPriceController@findAll");

    Route::delete('sales-price/{id}',
        "API\SalesPriceController@desactivate");




    Route::post('transport/', 'API\TransportController@create');

    Route::get('transport/{id}', "API\TransportController@findById");


    Route::get('transport/', "API\TransportController@findAll");

    Route::put('transport/{id}', "API\TransportController@update");


    Route::get('payment-method/', "API\PaymentMethodController@findAll");
    Route::put('payment-method/{id}', "API\PaymentMethodController@update");


    Route::get('parameters/', "API\ParameterController@findAll");
    Route::put('parameters/{id}', "API\ParameterController@update");


});
//final token


//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return 'API\UserController@getAll';
//});

