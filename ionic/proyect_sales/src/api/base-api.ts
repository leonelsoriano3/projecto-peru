import { Http, RequestOptions, Headers } from '@angular/http';
import {EndPointConfig, } from '../config/endpoint-config';
import {GlobalConfig} from '../config/global-config';
import {  LoadingController, ToastController, NavController} from 'ionic-angular';
import {AppModule} from '../app/app.module';
import {Observable} from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

/*export abstract class LoginDao <T> {
  
  private _header: Headers;
  private _loaderUnauthorized : any;

  constructor(){

      this._header = new Headers();
      this._header.append('Content-Type', 'application/json');
      this._header.append('Accept', 'application/json');

  }



  findOne: (jsonResponse : any) => T;

  findAll: (filter: Array<string>) => T;

  remove: ( id : number ) => void; 
  
  save: (dto : T ) => void;

  update: (id : number, dto : T) => void;

}*/

export class  BaseApi{

  protected _header: Headers;
  protected _loader : any;
  protected _loadingCtrl: LoadingController;
  protected _http: Http;
  protected _toastCtrl : ToastController;
  protected _navCtrl : NavController;

  private _noLoader : boolean;

  constructor(){
      this._noLoader = false;
      this._header = new Headers();
      this._header.append('Content-Type', 'application/json');
      this._header.append('Accept', 'application/json');
     // this._header.append('Access-Control-Allow-Origin','*');

      this._loadingCtrl = AppModule.injector.get(LoadingController);
      this._http = AppModule.injector.get(Http);
      this._toastCtrl = AppModule.injector.get(ToastController);
      //this._navCtrl =  AppModule.injector.get(NavController);     
  }



  protected makeRequestDelete(endpoint : string, auth : boolean): Observable<any>{
  
    let observable: Observable<any>;
    let localObserver: Observer<any>;

    observable = new Observable((observer: Observer<any>) => {
      localObserver = observer;
    });

    if(!this._noLoader){
       this._loader = this._loadingCtrl.create({
        content: "Cargando"
       });

        this._loader.present();
    }
    if(true){
      console.info(endpoint);
    }

    if(auth){
        if(!this._header.get("Authorization")){
            this._header.append("Authorization" , 'Bearer ' + localStorage.getItem('user.token'));
        }
    }

    let options = new RequestOptions({headers : this._header });
    this._http.delete(endpoint  ,options)
        .subscribe(
        data => {
                console.log(data);
                let response = data.json();
                if(!this._noLoader){
                    this._loader.dismiss();
                }

        
                if(response.success !== undefined){
                  localObserver.next(response.success);
                  localObserver.complete();
                }else{
                  localObserver.error(response.error);
                }

               },

        err => {
                console.log(err);


                if(!this._noLoader){
                    this._loader.dismiss();
                }

                localObserver.error(err);
              }                                                 
        );
        return observable;
  }



  //protected makeRequestPost<t>(endpoint : string, jsonData : any ): Observable<t>{

  protected makeRequestPost(endpoint : string, jsonData : any, auth : boolean): Observable<any>{
  
    let observable: Observable<any>;
    let localObserver: Observer<any>;

    observable = new Observable((observer: Observer<any>) => {
      localObserver = observer;
    });

    if(!this._noLoader){
       this._loader = this._loadingCtrl.create({
        content: "Cargando"
       });

        this._loader.present();
    }
    if(true){
      console.info(endpoint);
    }

    if(auth){
        if(!this._header.get("Authorization")){
            this._header.append("Authorization" , 'Bearer ' + localStorage.getItem('user.token'));
        }
    }

    let options = new RequestOptions({headers : this._header });
    this._http.post(endpoint ,jsonData ,options)
        .subscribe(
        data => {
                console.log(data);
                let response = data.json();
                if(!this._noLoader){
                    this._loader.dismiss();
                }

        
                if(response.success !== undefined){
                  localObserver.next(response.success);
                  localObserver.complete();
                }else{
                  localObserver.error(response.error);
                }

               },

        err => {
                console.log(err);


                if(!this._noLoader){
                    this._loader.dismiss();
                }

                localObserver.error(err);
              }                                                 
        );
        return observable;
  }



  protected makeRequestPut(endpoint : string, jsonData : any, auth : boolean): Observable<any>{
  
    let observable: Observable<any>;
    let localObserver: Observer<any>;

    observable = new Observable((observer: Observer<any>) => {
      localObserver = observer;
    });

    if(!this._noLoader){
       this._loader = this._loadingCtrl.create({
        content: "Cargando"
       });

        this._loader.present();
    }
    if(true){
      console.info(endpoint);
    }

    if(auth){
        if(!this._header.get("Authorization")){
            this._header.append("Authorization" , 'Bearer ' + localStorage.getItem('user.token'));
        }
    }

    let options = new RequestOptions({headers : this._header });
    this._http.put(endpoint ,jsonData ,options)
        .subscribe(
        data => {
                console.log(data);
                let response = data.json();
                if(!this._noLoader){
                    this._loader.dismiss();
                }

        
                if(response.success !== undefined){
                  localObserver.next(response.success);
                  localObserver.complete();
                }else{
                  localObserver.error(response.error);
                }

               },

        err => {
                console.log(err);

                if(!this._noLoader){
                    this._loader.dismiss();
                }

                localObserver.error(err);
              }                                                 
        );
        return observable;
  }







  protected makeRequestGet(endpoint : string, auth : boolean ): Observable<any>{
  
    let observable: Observable<any>;
    let localObserver: Observer<any>;

    observable = new Observable((observer: Observer<any>) => {
      localObserver = observer;
    });


    if(!this._noLoader){
       this._loader = this._loadingCtrl.create({
        content: "Cargando"
       });

        this._loader.present();
    }

    if(auth){
        if(!this._header.get("Authorization")){
            this._header.append("Authorization" , 'Bearer ' + localStorage.getItem('user.token'));
        }
    }
    
    let  options : RequestOptions = new RequestOptions({headers : this._header});
    
    if(true){
      console.info(endpoint);
    }
    this._http.get(endpoint, options)
        .subscribe(
        data => {
                let response = data.json();

                if(!this._noLoader){
                    this._loader.dismiss();
                }

                if(response.success !== undefined){
                  localObserver.next(response.success);
                  localObserver.complete();
                }else{
                  localObserver.error(response.error);
                }

               },

        err => {
                console.log(err);
                if(!this._noLoader){
                    this._loader.dismiss();
                }
                localObserver.error(err);
              }                                                 
        );

        return observable;
  }


  protected _defaultErrorOutput(err : string): void{
  
    if(err === 'Unauthorised'){

      let toast = this._toastCtrl.create({
        message: 'Credenciales equivocadas',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }else{
            
      let toast = this._toastCtrl.create({
        message: err,
        duration: 3000,
        position: 'bottom'
        });
        toast.present();

    }
  }//end defaultErrorOutput


  protected set noLoader(option: boolean){
    this._noLoader = option;
  }


    


} 



/*protected _http: Http, private loadingCtrl:LoadingController,
    protected toastCtrl: ToastController, protected navCtrl: NavController*/
