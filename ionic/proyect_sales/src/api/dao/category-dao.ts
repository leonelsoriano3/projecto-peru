import {EndPointConfig } from '../../config/endpoint-config';
import { BaseApi } from '../base-api';


export  class CategoryDao extends BaseApi {


  public update(id: number, jsonData : any, callback: (any) => void ) : void{

    this.makeRequestPut(EndPointConfig.CATEGORY + "/" + id
      , jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Asociacion actualizado correctamente',
                      duration: 1500,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                    if(callback !== null){
                      callback(data);
                    }                      
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create


  public getAll(loader : boolean,page : number,jsonData : any,
    callback: (any) => void ): void{
    
    if(loader){
      this.noLoader = false;
    }else{
      this.noLoader = true;
    }

    let textSearch : string = "";

    if(jsonData !== null){
      if(jsonData.textSearch !== undefined){
        jsonData.textSearch = jsonData.textSearch.replace(" ", "%20");
        textSearch = "&text-search=" + jsonData.textSearch;
      }
    }


    let pageQuery : string = '';
    if(page !== null){
      pageQuery = "?page=" + page ;
    }

    this.makeRequestGet(EndPointConfig.CATEGORY + pageQuery + textSearch , true).subscribe(
      data => { 
              callback(data);                         
            },
      err => {
              this._defaultErrorOutput(err);
              callback(null);
            }
        );//end subscribe
  
  
  }//end get all
  
  public create(jsonData : any, callback: (any) => void ) : void{

    this.makeRequestPost(EndPointConfig.CATEGORY, jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Asociacion creado correctamente',
                      duration: 1200,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                      callback(data);
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create



  public deleteOne(id : number, callback: (any) => void ) : void{

    this.makeRequestDelete(EndPointConfig.CATEGORY + "/" + id, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Asociacion eliminada correctamente',
                      duration: 1200,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                      callback(data);
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create




}
