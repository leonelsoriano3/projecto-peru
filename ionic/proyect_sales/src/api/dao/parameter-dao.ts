import {EndPointConfig } from '../../config/endpoint-config';
import { BaseApi } from '../base-api';
import { Session } from '../../injecter/session'
import { NavController } from 'ionic-angular';


export  class ParameterDao extends BaseApi {
  

  public update(id: number, jsonData : any, callback: (any) => void ) : void{

    this.makeRequestPut(EndPointConfig.PARAMETERS + "/" + id
      , jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Parametro actualizado correctamente',
                      duration: 1500,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                    if(callback !== null){
                      callback(data);
                    }                      
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create

 


  public getAll(loader : boolean, 
    callback: (any) => void ): void{
    
    this.makeRequestGet(EndPointConfig.PARAMETERS , true).subscribe(
      data => {                
              callback(data);                         
            },
      err => {
              this._defaultErrorOutput(err);
              callback(null);
            }
        );//end subscribe
  
  
  }//end getall



}//end class
