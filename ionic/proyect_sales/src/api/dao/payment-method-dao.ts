import {EndPointConfig } from '../../config/endpoint-config';
import { BaseApi } from '../base-api';
import { Session } from '../../injecter/session'
import { NavController } from 'ionic-angular';


export  class PaymentMethodDao extends BaseApi {
  

  public update(id: number, jsonData : any, callback: (any) => void ) : void{

    this.makeRequestPut(EndPointConfig.PAYMENT_METHOD + "/" + id
      , jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Metodo de pago actualizado correctamente',
                      duration: 1500,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                    if(callback !== null){
                      callback(data);
                    }                      
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create

 




  public getAll(loader : boolean, page : number , 
    jsonData : any,
    callback: (any) => void ): void{
    
    let textSearchBackend = '';

    //busqueda por patron en el backend
    if(jsonData.textSearch !== null && jsonData.textSearch !== undefined &&
      jsonData.textSearch.length > 0){
      textSearchBackend = '&text-search='+ jsonData.textSearch;
    }  

    if(loader){
      this.noLoader = false;
    }else{
      this.noLoader = true;
    }

    let state = "";

    if(jsonData.stateEntity === '1' || jsonData.stateEntity === '0'){
      let stateEntityStr = (jsonData.stateEntity === '1')? 'true': 'false';
      state = "&active="+ stateEntityStr;
    }

    let pageParam = '';

    if(page !== null){
      pageParam = "?page=" + page;
    }


    this.makeRequestGet(EndPointConfig.PAYMENT_METHOD + pageParam +  state + 
      textSearchBackend, true).subscribe(
      data => {                
              callback(data);                         
            },
      err => {
              this._defaultErrorOutput(err);
              callback(null);
            }
        );//end subscribe
  
  
  }//end getalluser






}//end class
