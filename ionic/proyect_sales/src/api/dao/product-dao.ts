import {EndPointConfig } from '../../config/endpoint-config';
import { BaseApi } from '../base-api';
import { Session } from '../../injecter/session'
import { NavController } from 'ionic-angular';


export  class ProductsDao extends BaseApi {
  
  public update(id: number, jsonData : any, callback: (any) => void ) : void{

    this.makeRequestPut(EndPointConfig.PRODUCTS + "/" + id
      , jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Producto actualizado correctamente',
                      duration: 1500,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                    if(callback !== null){
                      callback(data);
                    }                      
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create

  


  public getAll(loader : boolean,page : number,jsonData : any,
    callback: (any) => void ): void{
    
    if(loader){
      this.noLoader = false;
    }else{
      this.noLoader = true;
    }

    let textSearch : string = "";

    if(jsonData.textSearch !== undefined){
      jsonData.textSearch = jsonData.textSearch.replace(" ", "%20");
      textSearch = "&text-search=" + jsonData.textSearch;
    }


    let activeParam : string = "";
    if(jsonData.active !== undefined){
      let activeStr = "" 
      if(jsonData.active === 1 || jsonData.active === true){
         activeStr = 'true';;
      }else if(jsonData.active === 0 || jsonData.active === false){
         activeStr = 'false';
      }

      activeParam = "&active=" + activeStr;

    }

     let categoryParam = '';
     if(jsonData.category !== undefined){
       categoryParam = "&category=" + jsonData.category;
     }


    this.makeRequestGet(EndPointConfig.PRODUCTS+ "?page=" + page + textSearch + 
      activeParam + categoryParam , true).subscribe(
      data => { 
              callback(data);                         
            },
      err => {
              this._defaultErrorOutput(err);
              callback(null);
            }
        );//end subscribe
  
  
  }//end recuperateAccount


  public create(product : any, callback: (any) => void ) : void{

    this.makeRequestPost(EndPointConfig.PRODUCTS, product, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Producto creado correctamente',
                      duration: 3000,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                      callback(data);
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create



}//end class
