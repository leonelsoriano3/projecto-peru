import {EndPointConfig, } from '../../config/endpoint-config';
import { BaseApi } from '../base-api';
import { Session } from '../../injecter/session';
import { NavController } from 'ionic-angular';


export  class RegionDao extends BaseApi {

  public getAllRegion( callBack: (any) => void ) : void{
        this.noLoader = false;
    
        this.makeRequestGet(EndPointConfig.LOCATION_REGION, false).subscribe(
        data => {
                 /** esto es por un error del backend que debe arregalr se que por ser 
                  o interpretar el id como unmerico elimina ceros a la derecha**/
                if(data && data.length > 0){
                   for(let i = 0; i < data.length; i++){

                    if(data[i].id.toString().length < 6){
                    
       
                      for(let j = 0; j < 6 - data[i].id.toString().length ; j++){
                        data[i].id = "0" + data[i].id;
                      }
                      
                    }
                   }
                }//end data && data.len > 0
               
                callBack(data);
               },

        err => {

              if(err === 'Unauthorised'){

                let toast = this._toastCtrl.create({
                    message: 'Credenciales equivocadas',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
              }else{
                    console.log(err);
                 let toast = this._toastCtrl.create({
                    message: 'Error en el servidor',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();

              }
            }
        );
  }//end getAllProvinces


  public getRegionByProvinceId(id : string, callBack: (any) => void): void{
    


    this.noLoader = false;
    this.makeRequestGet(EndPointConfig.LOCATION_PROVINCE + id, false).subscribe(
        data => {

                 /** esto es por un error del backend que debe arregalr se que por ser 
                  o interpretar el id como unmerico elimina ceros a la derecha**/
                if(data && data.length > 0){
                   for(let i = 0; i < data.length; i++){

                    if(data[i].id.toString().length < 6){
                    
       
                      for(let j = 0; j < 6 - data[i].id.toString().length ; j++){
                        data[i].id = "0" + data[i].id;
                      }
                      
                    }
                   }
                }//end data && data.len > 0
          
                callBack(data);
               },

        err => {
                console.log(err);
              if(err === 'Unauthorised'){

                let toast = this._toastCtrl.create({
                    message: 'Credenciales equivocadas',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
              }else{

                 let toast = this._toastCtrl.create({
                    message: 'Error en el servidor',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();

              }
            }
        );
  
  }//end getRegionByProvinceId

  



    

}
