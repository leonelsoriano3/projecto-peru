import {EndPointConfig } from '../../config/endpoint-config';
import { BaseApi } from '../base-api';


export  class SubCategoryDao extends BaseApi {


  public update(id: number, jsonData : any, callback: (any) => void ) : void{

    this.makeRequestPut(EndPointConfig.SUB_CATEGORY + "/" + id
      , jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Sub categoria actualizada correctamente',
                      duration: 1500,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                    if(callback !== null){
                      callback(data);
                    }                      
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create


  public getAll(loader : boolean,page : number,jsonData : any,
    callback: (any) => void ): void{
    
    if(loader){
      this.noLoader = false;
    }else{
      this.noLoader = true;
    }

    let textSearch : string = "";

    if(jsonData !== null){
      if(jsonData.textSearch !== undefined){
        jsonData.textSearch = jsonData.textSearch.replace(" ", "%20");
        textSearch = "&text-search=" + jsonData.textSearch;
      }
    }


    let pageQuery : string = '';
    if(page !== null){
      pageQuery = "?page=" + page ; 
    }


    this.makeRequestGet(EndPointConfig.SUB_CATEGORY + pageQuery + textSearch , true).subscribe(
      data => { 
              callback(data);                         
            },
      err => {
              this._defaultErrorOutput(err);
              callback(null);
            }
        );//end subscribe
  
  
  }//end get all
  
  public create(jsonData : any, callback: (any) => void ) : void{

    this.makeRequestPost(EndPointConfig.SUB_CATEGORY, jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Sub categoria creada correctamente',
                      duration: 1200,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                      callback(data);
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create



  public deleteOne(id : number, callback: (any) => void ) : void{

    this.makeRequestDelete(EndPointConfig.SUB_CATEGORY + "/" + id, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Sub categoria eliminada correctamente',
                      duration: 1200,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                      callback(data);
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create




}
