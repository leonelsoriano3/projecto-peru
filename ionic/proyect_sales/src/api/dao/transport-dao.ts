import {EndPointConfig } from '../../config/endpoint-config';
import { BaseApi } from '../base-api';
import { Session } from '../../injecter/session'
import { NavController } from 'ionic-angular';


export  class TransportDao extends BaseApi {
  

  public update(id: number, jsonData : any, callback: (any) => void ) : void{

    this.makeRequestPut(EndPointConfig.TRANSPORT + "/" + id
      , jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Transporte actualizado correctamente',
                      duration: 1500,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                    if(callback !== null){
                      callback(data);
                    }                      
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create

 





  public getAll(loader : boolean, page : number , 
    jsonData : any,
    callback: (any) => void ): void{
    
    let textSearchBackend = '';

    //busqueda por patron en el backend
    if(jsonData.textSearch !== null && jsonData.textSearch !== undefined &&
      jsonData.textSearch.length > 0){
      textSearchBackend = '&text-search='+ jsonData.textSearch;
    }  

    if(loader){
      this.noLoader = false;
    }else{
      this.noLoader = true;
    }

    let state = "";

    if(jsonData.stateEntity === '1' || jsonData.stateEntity === '0'){
      let stateEntityStr = (jsonData.stateEntity === '1')? 'true': 'false';
      state = "&active="+ stateEntityStr;
    }

    let isFree = '';

    if(jsonData.isFreeEntity === '1' || jsonData.isFreeEntity === '0'){
      let isFreeEntityStr = (jsonData.isFreeEntity === '1')?  'true' : 'false'; 
      isFree = "&free_sending="+ isFreeEntityStr;
    }


    this.makeRequestGet(EndPointConfig.TRANSPORT + "?page=" + page +  isFree +  state + 
      textSearchBackend, true).subscribe(
      data => {                
              callback(data);                         
            },
      err => {
              this._defaultErrorOutput(err);
              callback(null);
            }
        );//end subscribe
  
  
  }//end getalluser



  public create(jsonData : any, callback: (any) => void ) : void{

    this.makeRequestPost(EndPointConfig.TRANSPORT, jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Transporte creado correctamente',
                      duration: 1600,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                      callback(data);
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create




}//end class
