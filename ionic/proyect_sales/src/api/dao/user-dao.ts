import {EndPointConfig, } from '../../config/endpoint-config';
import { BaseApi } from '../base-api';
import { Session } from '../../injecter/session'
import { NavController } from 'ionic-angular';
import {UserRegisterDto} from '../dto/user-register';
import {ActivateAcountInDto} from '../dto/activate-acount-in-dto'; 
import {RecuperateAccountDtoIn} from '../dto/recuperate-account-dto-in';

export  class UserDao extends BaseApi {


  public update(id: number, jsonData : any, callback: (any) => void ) : void{

    let idUrl = (id === null) ?  "" :  "/" + id;

    this.makeRequestPut(EndPointConfig.USER +  idUrl
      , jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Usuario actualizado correctamente',
                      duration: 1500,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                    if(callback !== null){
                      callback(data);
                    }                      
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create



  public getAllUser(loader : boolean,page : number , 
    userState: string, textSearch : string,
    callback: (any) => void ): void{
    
    let textSearchBackend = '';

    //busqueda por patron en el backend
    if(textSearch !== null && textSearch !== undefined &&
      textSearch.length > 0){
      textSearchBackend = '&text-search='+ textSearch;
    }  

    if(loader){
      this.noLoader = false;
    }else{
      this.noLoader = true;
    }

    let state = "";

    if(userState === '1' || userState === '0'){
      state = "&active="+ userState;
    }

    this.makeRequestGet(EndPointConfig.USER + "?page=" + page + state + 
      textSearchBackend, true).subscribe(
      data => {                
              callback(data);                         
            },
      err => {
              this._defaultErrorOutput(err);
              callback(null);
            }
        );//end subscribe
  
  
  }//end getalluser


  public getUser(id : number, loader : boolean,
    callback: (any) => void ): void{


    if(loader){
      this.noLoader = false;
    }else{
      this.noLoader = true;
    }

    let idParam = '';

    if(id !== null){
      idParam = "/" + id;
    }else{
      idParam = "-me"
    }


    this.makeRequestGet(EndPointConfig.USER + idParam , true).subscribe(
      data => {  
              callback(data);                         
            },
      err => {
              this._defaultErrorOutput(err);
              callback(null);
            }
        );//end subscribe
  
  
  }//end recuperateAccount


  /**
   * recuperacion de la cuenta de usuario luego de enviar el token
   */
  public recuperateAccount(recuperateDto : RecuperateAccountDtoIn,
    callback: () => void ): void{
    
    this.makeRequestPost(EndPointConfig.USER_RECUPERATE_ACCOUNT, 
      recuperateDto.toJson() ,false).subscribe(
      data => {
                let toast = this._toastCtrl.create({
                    message: 'Se ha recuperado tu cuenta correctamente',
                    duration: 1500,
                    position: 'bottom'
                });

                toast.onDidDismiss(() => {
                    callback();
                });
                toast.present();
            },

      err => {
              this._defaultErrorOutput(err);           
            }
        );//end subscribe
  
  
  }//end recuperateAccount

  
  
  public forgotterPassword(emailIn: string, callback: () =>void):void{
    let jsonObject = {
      email: emailIn
    };
  
    this.makeRequestPost(EndPointConfig.USER_FORGOTTEN_PASSWORD, jsonObject,false).subscribe(
        data => {
                    let toast = this._toastCtrl.create({
                        message: 'Se ha enviado un token a su correo de verificación',
                        duration: 1500,
                        position: 'bottom'
                    });

                    toast.onDidDismiss(() => {
                        callback();
                    });
                    toast.present();
               },

        err => {
                this._defaultErrorOutput(err);           
              }
        );

  }//end forgotterPassword


  public validateUser(dto : ActivateAcountInDto, callback: () =>void ): void{
        
      this.makeRequestPost(EndPointConfig.USER_VALIDATE, dto.toJson(),false).subscribe(
        data => {
                    let toast = this._toastCtrl.create({
                        message: 'Usuario validado correctamente',
                        duration: 1500,
                        position: 'bottom'
                    });

                    toast.onDidDismiss(() => {
                        callback();
                    });
                    toast.present();
               },

        err => {

              this._defaultErrorOutput(err);

            }
        );
  }//end validate user

  public registerUser(userRegister : UserRegisterDto, callBack: () => void): void{

    let jsoObject : any = userRegister.toJsonObject();
   
    if(jsoObject.provinces_id !== null && 
        jsoObject.provinces_id.toString().length == 5){
        jsoObject.provinces_id = "0" + jsoObject.provinces_id;
    }


    this.makeRequestPost(EndPointConfig.USER_REST, jsoObject,false).subscribe(
        data => {
                    callBack();
                    let toast = this._toastCtrl.create({
                        message: 'Se ha enviado un token a su correo de activación de cuenta',
                        duration: 1500,
                        position: 'bottom'
                    });
                    toast.present();

               },

        err => {

                this._defaultErrorOutput(err);
          
              }
        );//end subscribe

  
  }//end register user



  public doLogig(email: string, password: string, nav : NavController) : void{

       let jsonData = {
            email : email,
            password : password
        };


        this.makeRequestPost(EndPointConfig.USER_LOGIN_API,
            jsonData, false).subscribe(
        data => {
                //console.log(data);
                localStorage.setItem('user.name',data.name);
                localStorage.setItem('user.token',data.token);
                localStorage.setItem('user.email', data.email);
                localStorage.setItem('user.rol', data.rol);
                localStorage.setItem('user.img', data.img);
                let session : Session = new Session(nav);
                session.internalControl();
               },

        err => {
                this._defaultErrorOutput(err);            
              }
        );


    //console.log(this._http);
    /*let options = new RequestOptions({headers : this._header });




    this._http.put(EndPointConfig.USER_LOGIN_API , jsonData,  options)
        .subscribe(
        data =>console.log(data.json()),
        err => console.log("error")
        );*/

  }



  public deleteOne(id : number, jsonData : any,  callback: (any) => void ) : void{

      this.makeRequestPost(EndPointConfig.USER_DESACTIVATE + "/" + id, jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Usuario modificado correctamente',
                      duration: 1200,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                      callback(data);
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  
  }//end desactivate




  public createbyAdmin(jsonData : any, callback: (any) => void ) : void{


    this.makeRequestPost(EndPointConfig.CREATE_USER_BY_ADMIN, jsonData, true).subscribe(
        data => {
                  let toast = this._toastCtrl.create({
                      message: 'Usuario creado correctamente',
                      duration: 1200,
                      position: 'bottom'
                  });
                  toast.onDidDismiss(() => {
                      callback(data);
                  });
                  toast.present();
               },

        err => {
                this._defaultErrorOutput(err);
              }
        );//end subscribe
  }//end create


}
