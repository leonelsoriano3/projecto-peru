
/**
 * modelo para usuario 
 */
export  class ActivateAcountInDto {
  
  private _email : string;
  private _token : string;  

  public toJson():any{
    let jsonObject = {
        email : this._email,
        token : this._token
    }
    return jsonObject;
  }

  //getter setter

  public get email() : string {
      return this._email;
  }
  
  public set email(email : string) {
      this._email = email;
  }

  public get token() : string {
      return this._token;
  }
  
  public set token(token : string) {
      this._token = token;
  }

}
