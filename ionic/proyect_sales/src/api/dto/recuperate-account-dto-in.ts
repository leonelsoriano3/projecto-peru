

/**
 * modelo para la recuperacion de usuario
 */
export  class RecuperateAccountDtoIn {
  
  private _email : string;
  private _token : string;
  private _password: string;
  private _cPassword: string;

  public toJson():any{
    let jsonObject = {
        email : this._email,
        token : this._token,
        password: this._password,
        c_password: this._cPassword
    }
    return jsonObject;
  }

  //getter setter

  public get email() : string {
      return this._email;
  }
  
  public set email(email : string) {
      this._email = email;
  }

  public get token() : string {
      return this._token;
  }
  
  public set token(token : string) {
      this._token = token;
  }

  public get password() : string {
      return this._password;
  }
  
  public set password(password : string) {
      this._password = password;
  }

  public get cPassword() : string {
      return this._cPassword;
  }
  
  public set cPassword(cPassword : string) {
      this._cPassword = cPassword;
  }


}






