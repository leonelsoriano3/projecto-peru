
/**
 * modelo para usuario 
 */
export  class UserDao {
  
  private _id : string;
  private _name: string;
  private _email: string;
  private _active: boolean;
  private _favoritePayment: number;
  private _rol : number;
    
  //getter setter

  public get id() : string {
        return this._id;
  }  

  public set id(id : string) {
      this._id = id;
  }
  
  
  public get name() : string {
      return this._name;
  }
  
  public set name(name : string) {
      this._name = name;
  }

  public get email() : string {
      return this._email;
  }
  
  public set email(email : string) {
      this._email = email;
  }

  public get active() : boolean {
      return this._active;
  }
  
  public set active(active : boolean) {
      this._active = active;
  }

  public get favoritePayment() : number {
      return this._favoritePayment;
  }
  
  public set favoritePayment(favoritePayment : number) {
      this._favoritePayment = favoritePayment;
  }

  public get rol() : number {
      return this._rol;
  }
  
  public set rol(rol : number) {
      this._rol = rol;
  }



}