
/**
 * usuario que mapea al usuario 
 */
export  class UserLoginDto {

  private _name: string;
  private _token: string;
  private _email: string;
  private _rol: number;

  constructor(){
        this._name = localStorage.getItem('user.name');
        this._token = localStorage.getItem('user.token');
        this._email = localStorage.getItem('user.email');
        this._rol =  parseInt(localStorage.getItem('user.rol'));
  }


  public get name() : string {
        return this._name;
  }  

  public set name(name : string) {
      this._name = name;
  }
  
  public get token() : string {
        return this._token;
  }  

  public set token(token : string) {
      this._token = token;
  }

  public get email() : string {
        return this._email;
  }  

  public set email(email : string) {
      this._email = email;
  }

  public get rol() : number {
        return this._rol;
  }  

  public set rol(rol : number) {
      this._rol = rol;
  }


}