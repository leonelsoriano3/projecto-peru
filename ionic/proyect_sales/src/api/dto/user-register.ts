/**
 * modelo registro de usuario 
 */
export  class UserRegisterDto {
  
  private _name : string;
  private _email : string;
  private _surName : string;
  private _password : string;
  private _cPassword : string;
  private _distrito: string;
  private _identification : string;
  private _img : string;
  private _typeImg: string;
  private _phone: string;
  private _typeIdentification: number;
  private _provincesId : number;
  //getter setter

  public toJsonObject(): any{
    let json = {
        name : this._name,
        email: this._email,
        sur_name: this._surName,
        password: this._password,
        c_password: this._cPassword,
        distrito: this._distrito,
        identification: this._identification,
        img : this._img,
        phone : this._phone,
        type_identification : this._typeIdentification,
        provinces_id : this._provincesId,
        type_img : this._typeImg
    };

    return json;
  }


  public get name() : string {
        return this._name;
  }  

  public set name(name : string) {
      this._name = name;
  }
  
  public get email() : string {
        return this._email;
  }  

  public set email(email : string) {
      this._email = email;
  }


  public get surName() : string {
        return this._surName;
  }  

  public set surName(surName : string) {
      this._surName = surName;
  }

  public get password() : string {
        return this._password;
  }  

  public set password(password: string) {
      this._password = password;
  }


  public get cPassword() : string {
        return this._cPassword;
  }  

  public set cPassword(cPassword : string) {
      this._cPassword = cPassword;
  }

  public get distrito() : string {
        return this._distrito;
  }  

  public set distrito(distrito : string) {
      this._distrito = distrito;
  }

  public get identification() : string {
        return this._identification;
  }  

  public set identification(identification : string) {
      this._identification = identification;
  }


  public get img() : string {
        return this._img;
  }  

  public set img(img : string) {
      this._img = img;
  }

  public get typeImg(){
        return this._typeImg;
  }  

  public set typeImg(typeImg : string) {
      this._typeImg = typeImg;
  }

  public get phone() : string {
        return this._phone;
  }  

  public set phone(phone : string) {
      this._phone = phone;
  }

  public get typeIdentification() : number {
        return this._typeIdentification;
  }  

  public set typeIdentification(typeIdentification : number) {
      this._typeIdentification = typeIdentification;
  }


  public get provincesId() : number {
        return this._provincesId;
  }  

  public set provincesId(provincesId : number) {
      this._provincesId = provincesId;
  }

}
