import {GlobalConfig} from '../config/global-config';
/**
* maneja algunas peticiones  son cacheo de getall basicamente es decir
* cacheos sencillos en la app, en cache localstorage
* estructura de la cache
* {
*   time : 12221212112,
*   data: {}
*  }
*/
export class  ManagerCache{

  constructor(){}

  resolveCache(key: string): any{
    //MAX_TIME_CACHE
    let currentDate = new Date().getTime;

    if(localStorage.getItem(key) === undefined ||
     localStorage.getItem(key) === null){
      return null;
    }

    let cacheJson = JSON.parse( localStorage.getItem(key));
    if(cacheJson.time < (+ new Date().getTime()) ){
      return null;
    }else{
      return cacheJson.data;
    }
  }


  addCache(key: string, data: any): void{
    let currentDate = new Date().getTime();

    let dataToStorage = {
        time: (+ currentDate + GlobalConfig.MAX_TIME_CACHE),
        data : data
    }
    localStorage.setItem(key, JSON.stringify(dataToStorage));
  }

}