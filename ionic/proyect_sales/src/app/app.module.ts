import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, Injector, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule, Http} from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { PrivacyPolicyPage } from 
    '../pages/privacy-policy/privacy-policy';
import { ValidateAccountPage } from '../pages/validate-account/validate-account';
import {ForgotterPasswordPage} from '../pages/forgotter-password/forgotter-password';
import { CreateUserPage } from '../pages/create-user/create-user';
import {AdminCreateUserPage} from '../pages/admin-create-user/admin-create-user';
import {ListUserPage} from '../pages/list-user/list-user';
import { ModalAssociatePage } from '../pages/modal-associate/modal-associate';
import {CreateProductPage} from '../pages/create-product/create-product';
import {ListAssociatePage} from '../pages/list-associate/list-associate';
import {CreateAssociatePage} from '../pages/create-associate/create-associate';

import {ListCategoryPage} from '../pages/list-category/list-category';
import {CreateCategoryPage} from '../pages/create-category/create-category';
import {ListSubCategoryPage} from '../pages/list-sub-category/list-sub-category';
import {CreateSubCategoryPage} from 
  '../pages/create-sub-category/create-sub-category';
import {UpdateUserPage} from '../pages/update-user/update-user';
import {ParameterPage} from '../pages/parameter/parameter';
import { UserMapPage} from '../pages/user-map/user-map';


import {RecuperateAccountPage} from '../pages/recuperate-account/recuperate-account';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FileChooser } from '@ionic-native/file-chooser';
import { FileChooserComponent } from '../components/file-chooser/file-chooser';
import {AdminHomePage} from '../pages/admin-home/admin-home';
import {CreateTransportPage} from '../pages/create-transport/create-transport';
import {ListTransportPage} from '../pages/list-transport/list-transport';
import {ProductListPage} from '../pages/product-list/product-list';
import { PriceValidatorServiceProvider } from '../providers/price-validator-service/price-validator-service';
import { ListMethodPaymentPage} from '../pages/list-method-payment/list-method-payment';
import { UserPurchaseModalItemCartPage } from '../pages/user-purchase-modal-item-cart/user-purchase-modal-item-cart';
import { UserPurchaseModalModalCartPage } from '../pages/user-purchase-modal-modal-cart/user-purchase-modal-modal-cart';

import {UserPurchasePaymentPage} from '../pages/user-purchase-payment/user-purchase-payment';

import {PromoModalPage} from '../pages/promo-modal/promo-modal';


import { UserPurchasePage} from '../pages/user-purchase/user-purchase';
import { UserPurchasePophoverPage  } from "../pages/user-purchase-pophover/user-purchase-pophover";

import { ImgCacheModule } from 'ng-imgcache';
import { Base64 } from '@ionic-native/base64';

import { Ionic2RatingModule } from 'ionic2-rating';

import { Geolocation } from '@ionic-native/geolocation';

import { IonicImageViewerModule } from 'ionic-img-viewer';


import { Ng2DragDropModule } from 'ng2-drag-drop';





export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    PrivacyPolicyPage,
    CreateUserPage,
    FileChooserComponent,
    ValidateAccountPage,
    ForgotterPasswordPage,
    RecuperateAccountPage,
    AdminCreateUserPage,
    AdminHomePage,
    ListUserPage,
    ProductListPage,
    CreateProductPage,
    ListAssociatePage,
    CreateAssociatePage,
    ModalAssociatePage,
    ListCategoryPage,
    CreateCategoryPage,
    ListSubCategoryPage,
    CreateSubCategoryPage,
    UpdateUserPage,
    CreateTransportPage,
    ListTransportPage,
    ListMethodPaymentPage,
    ParameterPage,
    UserMapPage,
    UserPurchasePage,
    UserPurchasePophoverPage,
    UserPurchaseModalItemCartPage,
    UserPurchaseModalModalCartPage,
    UserPurchasePaymentPage,
    PromoModalPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ImgCacheModule,
    Ionic2RatingModule,
    IonicImageViewerModule,
    Ng2DragDropModule.forRoot(),
    TranslateModule.forRoot({
    loader: {
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    }
  }),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    PrivacyPolicyPage,
    CreateUserPage,
    ValidateAccountPage,
    ForgotterPasswordPage,
    RecuperateAccountPage,
    AdminCreateUserPage,
    AdminHomePage,
    ListUserPage,
    ProductListPage,
    CreateProductPage,
    ListAssociatePage,
    CreateAssociatePage,
    ModalAssociatePage,
    ListCategoryPage,
    CreateCategoryPage,
    ListCategoryPage,
    CreateSubCategoryPage,
    ListSubCategoryPage,
    UpdateUserPage,
    CreateTransportPage,
    ListTransportPage,
    ListMethodPaymentPage,
    ParameterPage,
    UserMapPage,
    UserPurchasePage,
    UserPurchasePophoverPage,
    UserPurchaseModalItemCartPage,
    UserPurchaseModalModalCartPage,
    UserPurchasePaymentPage,
    PromoModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FileChooser,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PriceValidatorServiceProvider,
    Base64
  ]
})
export class AppModule {
  static injector: Injector;

  constructor(private injector: Injector) {
    AppModule.injector = this.injector;
  }

}
