/**
 * clase que se encarga de crear itenes en el carrito de compra
 */
export  class EntityPoket {
  
  private _item: any;

  private _amount : number;

  constructor(item: any,amount? : number){
  	this._item = item;
  	this._amount = amount || 0;
  }


  public get item(): any{
  	return this._item

  }

  public set item(item : any){
  	this._item = item;
  }


  public get amount(): number{
  	return this._amount;
  }


  public set amount(amount: number){
  	this._amount = amount;
  }

  public get total(){
    return this._amount * this._item.price.amount;
  }


}