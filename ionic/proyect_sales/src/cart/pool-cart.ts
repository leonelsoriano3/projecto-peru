
import {EntityPoket} from './entity-poket'

/**
 * class para simular una db de cart
 */
export  class PoolCart {
  
  private _data : Array<EntityPoket>; 

  public constructor(){
    this._data = new Array();
  }

  /**
  * agrega o actualiza en el store
  */
  public add( itemData : EntityPoket){
    
    let existInStore : number = null ;

    for(let i = 0 ; i < this._data.length; i++){
      if(this._data[i].item.id === itemData.item.id){
        existInStore = i;
      }
    }

    if(existInStore === null){
     
      this._data.push(itemData);
    }else{

      if(itemData.amount === 0){
        itemData.amount = this._data[existInStore].amount;
      }
      this._data[existInStore] = itemData;
    }
    
  }



  public getByIndex(index  : number) : EntityPoket{
    if(index >= this._data.length ){
      return null;
    }else{
       return this._data[index];
    }
  }

  public getAll() :  Array<EntityPoket>{
    return this._data;
  }

  public getById(id : string){
    for(let i = 0 ; i < this._data.length; i++){
      if(this._data[i].item.id === id){
         return this._data[i];
      }
    }
    return null;
  }

  /**
  * remueve del store a bvase del id del json
  */
  public removeById(id : string): void{
    for(let i = 0 ; i < this._data.length; i++){
      if(this._data[i].item.id === id){
         this._data.slice(i, 1);
      }
    }
  }


  public getByIdAmount(id : string) : number{

    for(let i = 0 ; i < this._data.length; i++){
      console.log(this._data[i].item.id  + "  = "+ id );
      if(this._data[i].item.id === id){
         return this._data[i].amount;
      }
    }
    return 0;
  }


  /**
  * obtiene el totem de monto para este item si amount es null
  * no lo actualiza de lo contrario actualiza el amount dle item
  * extre tenemos un bolleano que no ayuda en los modales de sacar la cuenta pero no 
  * actualizar este estado
  */
  public getTotalItem(  id : string , amount? : number, updateAmount : boolean = true): number{

    for(let i = 0 ; i < this._data.length; i++){

      if(this._data[i].item.id === id){

          if(amount !== null && updateAmount ){
            this._data[i].amount = amount;
          }
          

         return  Math.round((this._data[i].item.price.amount * amount)* 100)/100;
      }
    }

    return 0;

  }



  /**
  * total a pagar en el carrito
  */
  public getTotal( ): number{

   
    let totalToPay = 0;

    for (var i = 0; i < this.data.length; ++i) {
      totalToPay += this.data[i].amount * this.data[i].item.price.amount;
    }

    return  Math.round( totalToPay * 100)/100;
  }






  /**
  * consigue todos los itenes seleccionados
  */
  public getTotalSelected(): number{
    let total : number = 0;

    for(let i = 0 ; i < this._data.length; i++){
     total = (total * 1) +  (this._data[i].amount * 1);
    }

    return total * 1;

  }


  public get data(){
    return this._data;
  }

  public set data(newData :  Array<EntityPoket>){
    this._data = newData;
  }

 
}