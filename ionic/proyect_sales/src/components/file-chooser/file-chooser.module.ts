import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FileChooserComponent } from './file-chooser';

@NgModule({
  declarations: [
    FileChooserComponent,
  ],
  imports: [
    IonicPageModule.forChild(FileChooserComponent),
  ],
  exports: [
    FileChooserComponent
  ]
})
export class FileChooserComponentModule {}
