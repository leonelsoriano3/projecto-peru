import { Component, Output, Input,  EventEmitter,  } from '@angular/core';
import {  LoadingController} from 'ionic-angular';
import { FileChooser } from '@ionic-native/file-chooser';
//import { Base64 } from '@ionic-native/base64';
declare var window;
import {AppModule} from '../../app/app.module';

@Component({
  selector: 'file-chooser',
  templateUrl: 'file-chooser.html'
})
export class FileChooserComponent {
  
  private _defaultMsg : string;

  @Input()  msg : string = null;

  private _err : string;

  private _input : any;

  private _loader : any;

  protected _loadingCtrl: LoadingController;



  @Output() states = new EventEmitter();


  constructor(private _fileChooser: FileChooser) {
    this._defaultMsg = "Selecione una imagen*";
    this._loadingCtrl = AppModule.injector.get(LoadingController);
           // this.eventEmitter = new EventEmitter();
  }

  ngOnInit() {
   
    if(this.msg !== null && 
      this.msg.length !== 0){
      //console.log("");
      this.msg = "Archivo seleccionado  " + this.msg ;
    }else{
      //console.log("not null");
      this.msg = this._defaultMsg;
    }

  }



  public onSelectFile(event): void{
    console.log("onSelectFile");
    let self : FileChooserComponent = this;
    this._fileChooser.open()
      .then( (uri) =>  {
            console.log("selected");
            console.log(uri);
            window.resolveLocalFileSystemURL(uri, (fileEntry) =>{
 //console.log("resolveLocalFileSystemURL");
              fileEntry.file(function (file){
              
                   self._loader = self._loadingCtrl.create({
                        content: "Verificando imagen"
                   });

                   self._loader.present();

                    console.log(file);
                    console.log("fileentry");
                  if(file.type !== "image/jpeg" && file.type !== 'image/png'){
                    console.log(file.type);
                    self._err = "Tipo de archivo no soportado";
                    self.msg = self._defaultMsg;
                    self.states.emit({error : "tipo de dato no soportado"});
                    self._loader.dismiss();

                  }else if(file.size > 10000000){
                    self._err = "Tamaño exede el maximo";
                    console.log(file.size );
                    self.msg = self._defaultMsg;
                    self.states.emit({error : "Tamaño maximo"});
                    self._loader.dismiss();
                  }else{
                    console.log("else");
                    
                    let reader = new FileReader();
                    

                   

                      reader.readAsDataURL(file);
                        console.log("after readAsDataURL");
                      reader.onloadend = function () {
                      console.log("on load");
                      self._err = "";
                      console.log("lef errro");

                      self.msg = "Archivo seleccionado " + file.name;
                        console.log("self.msg");
                      self.states.emit({base64 : this.result, typeImagen : file.type });
                        console.log("states.emit");
                        self._loader.dismiss();
                        console.log("_loader.dismiss");
                                
                      };
                    reader.onerror = function (error) {
                       console.log("error");
                      self._err = "Error en cargar el archivo";
                      self.msg = self._defaultMsg;
                      self.states.emit({error : error});
                      self._loader.dismiss();

                    };
                  
                  }



               });
                            
            },
            ()=>{
                self.msg = self._defaultMsg;
                self.states.emit({error : "Problemas al leer el archivo"});
            } );
            
        })
      .catch(e => {
            
            self._input = document.createElement('input');
            self._input.type = 'file';
            
            self._input.onchange = function(e) { 
          //  console.log(this.files[0]);
              let file : any = this.files[0];
              
              console.log(file);

              if(file.type !== "image/jpeg" && file.type !== 'image/png'){

                self._err = "Tipo de archivo no soportado";
                self.msg = self._defaultMsg;
                self.states.emit({error : "tipo de dato no soportado"});

              }else if(file.size > 100000000){
                self._err = "Tamaño exede el maximo";
                self.msg = self._defaultMsg;
                self.states.emit({error : "Tamaño maximo acalsando"});
              }else{
                let reader = new FileReader();
                
                 self._loader = self._loadingCtrl.create({
                      content: "Decodificando imagen"
                 });

                  self._loader.present();

                reader.readAsDataURL(file);
                reader.onload = function () {
                  self._err = "";
                  self.msg = "Archivo seleccionado " + file.name;
                   // console.log(self.eventEmitter);
                  self.states.emit({base64 : reader.result, typeImagen : file.type });
                  self._loader.dismiss();
                
                };
                reader.onerror = function (error) {
                  self._err = "Error en cargar el archivo";
                  self.msg = self._defaultMsg;
                  self._loader.dismiss();
                  self.states.emit({error : error});
                };
              
              }
            };
            self._input.click();
            
      });








      
  }


  //getter setter  
  public get err(): string{
    return this._err;
  }



}
