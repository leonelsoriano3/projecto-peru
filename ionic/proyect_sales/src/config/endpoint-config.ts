
export class EndPointConfig {

  // C:/Bitnami/wampstack-7.0.21-0/php/php ./artisan serve --host=192.168.1.163

  //private static _endPoint : string = "http://10.0.2.2:8000";

  //private static _endPoint : string = "http://localhost:8000";

 private static _endPoint : string =  "http://5.189.148.84:8000";
  
  //private static _endPoint : string =  "http://192.168.1.163:8000";
private static _postUrl : string = "/api/";

  public static get USER_LOGIN_API():string {
    return EndPointConfig._endPoint + EndPointConfig._postUrl  + "login";}

  public static LOCATION_REGION : string = 
        EndPointConfig._endPoint + EndPointConfig._postUrl + "location/region";

  public static LOCATION_PROVINCE : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "location/province-by-region/";

  public static USER_REST : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "register";

  public static USER_DESACTIVATE : string = 
    EndPointConfig._endPoint + EndPointConfig._postUrl + "user/desactivate";

 
  public static USER_VALIDATE : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "user/activateAccount";

  public static USER_FORGOTTEN_PASSWORD : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "user/passWordForgotten";

  public static USER_RECUPERATE_ACCOUNT : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "user/recuperate";

  public static USER : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "user";

  public static PRODUCTS : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "product";

  public static ASSOCIATE : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "associate";

  public static CATEGORY : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "categories";

  public static TRANSPORT : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "transport";

  public static SUB_CATEGORY : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "sub-categories";

  public static CREATE_USER_BY_ADMIN : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "user/register-admin";


  public static PAYMENT_METHOD : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "payment-method";

  public static PARAMETERS : string =
        EndPointConfig._endPoint + EndPointConfig._postUrl + "parameters";


  public static DEFAULT_IMG : string = EndPointConfig._endPoint + "/img/default-img.png";

  public static BASE_IMG : string = EndPointConfig._endPoint + "/img/";
}


