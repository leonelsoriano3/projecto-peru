
export class GlobalConfig {
  private static _endPoint : string = "http://127.0.0.1:8000";

  private static _postUrl : string = "/api/";

  public static get GET():string {
    return "GET"
  }

  public static get POST(): string{
      return "POST";
  }

  public static get DELETE(): string{
      return "DELETE";
  }

  public static get PUT(): string{
      return "PUT";
  }


  /**
  * minutos de cache
  */
  public static get MAX_TIME_CACHE(): number{
    return 60 * 60 * 60 * 60;
  }

  /**
  *  tag de category para el local storage de cache
  */
  public static get CATEGORY_TAG(): string{
    return "CATEGORY_TAG";
  }

  public static get PARAMTER_TAG(): string{
    return "PARAMETER_TAG";
  }

  public static get PAYMENT_METOD_TAG(): string{
    return "PAYMENT_METOD";
  }


}
