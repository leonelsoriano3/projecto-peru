import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
/**
 * clase que se sera injectada y 
 * vigilara el estado de la trduccion
 */
@Injectable()
export class  LangCheker{

    constructor(private translate: TranslateService){
        if(localStorage.getItem('user.lang') === null ||
            localStorage.getItem('user.lang') === undefined){
            localStorage.setItem('user.lang','es');

        }else if(localStorage.getItem('user.lang') !== 'en'){
            localStorage.setItem('user.lang', 'es');
        }
        this.translate.setDefaultLang(localStorage.getItem('user.lang'));
    }


    /**
     *  consigue el idioma 
     */
    public getLang() : string {
        return localStorage.getItem('user.lang');
    }

    public setLang(lang : string ) : void{
        if(lang == 'es' || lang == 'en'){
            localStorage.setItem('user.lang',lang);
        }else{
            localStorage.setItem('user.lang', 'es');
        }
        this.translate.setDefaultLang(localStorage.getItem('user.lang'));
    }

} 


