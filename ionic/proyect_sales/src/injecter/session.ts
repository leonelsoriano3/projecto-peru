import { Injectable } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../pages/home/home';
import {PrivacyPolicyPage} from '../pages/privacy-policy/privacy-policy';
import { LoginPage } from '../pages/login/login';
import {AppModule} from '../app/app.module';
/**
 * clase que maneja las sesiones
 *
 */
@Injectable()
export class  Session{

    private _navCtrl : NavController;

    constructor(private navCtrl : NavController){
         this._navCtrl = navCtrl;
    }

    public internalControl() : void{
        if(localStorage.getItem('user.token') === null ||
          localStorage.getItem('user.token') === undefined ){
            this._navCtrl.setRoot(LoginPage);
        }else if(
            localStorage.getItem('user.privacy-policy') === undefined ||
            localStorage.getItem('user.privacy-policy') === null ||
            localStorage.getItem('user.privacy-policy') === 'false'){

            this._navCtrl.setRoot(PrivacyPolicyPage);

        }else{

            this._navCtrl.setRoot(HomePage);
        }
    }

    /**
     * si tengo token yu he asceptado me voy a home
     */
    public internalControlForLogin() : void{
        if(localStorage.getItem('user.token') !== null &&
          localStorage.getItem('user.token') !== undefined){

            if(localStorage.getItem('user.privacy-policy') === 'true'){

                this._navCtrl.setRoot(HomePage);

            }
        }

    }//end internalControlForLogin

    /**
     * cierra sesion
     */
    public closeSession() :void{
        localStorage.removeItem('user.name');
        localStorage.removeItem('user.token');
        localStorage.removeItem('user.email');
        localStorage.removeItem('user.rol');
        this._navCtrl.setRoot(LoginPage);
    }

    /**
     * evento al rechazar terminos
     */
    public rejectPolicy(): void{
        localStorage.removeItem('user.name');
        localStorage.removeItem('user.token');
        localStorage.removeItem('user.email');
        localStorage.removeItem('user.rol');
        localStorage.removeItem('user.privacy-policy');
        this._navCtrl.setRoot(LoginPage);
    }

    public acceptPolicy():void{
        localStorage.setItem('user.privacy-policy', 'true');
        this.internalControl();
    }

}
