import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminCreateUserPage } from './admin-create-user';

@NgModule({
  declarations: [
    AdminCreateUserPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminCreateUserPage),
  ],
  exports: [
    AdminCreateUserPage
  ]
})
export class AdminCreateUserPageModule {}
