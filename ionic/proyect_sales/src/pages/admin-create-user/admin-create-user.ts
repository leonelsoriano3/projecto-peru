import { Component,ViewChild  } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar } from 'ionic-angular';
import { LangCheker } from '../../injecter/lang-cheker';
import { FileChooser } from '@ionic-native/file-chooser';
import {UserDao} from "../../api/dao/user-dao";
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RegionDao} from '../../api/dao/region-dao';



@IonicPage()
@Component({
  selector: 'page-admin-create-user',
  templateUrl: 'admin-create-user.html',
  providers: [LangCheker]
})
export class AdminCreateUserPage {

  @ViewChild(Navbar) navBar: Navbar;

  private _formGroup : FormGroup;

  private _documentType: string;

  private _provinces : any[];
  
  private _regions : any[];

  private _region : string;

  private _resquestRegion : RegionDao;

  private _base64 : string;

  private _validImagen : boolean;

    //este es para cambiarle el nombre del seleccioanda al componete :D
  private _nameFileUpdate : string = null;

  /**
   * si este valor se cambia el formulario sabe que sera una actualizacion
   */
  private _id : number = null;

  private _typeImg : string;


  constructor(private _langCheker : LangCheker, private _navCtrl: NavController, 
  	private _navParams: NavParams,   private _fb: FormBuilder, 
    private _fileChooser: FileChooser) {

    console.log(this._navParams.get("user"));

    let self : AdminCreateUserPage = this;

    //verdadero porque la imagen no es required  
    this._validImagen = true;


    let regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    let regexPhone = "^\\+[1-9]{1}[0-9]{3,14}$";
    let regexDni = "^\\d{8}(\\-)\\d{1}$";
  
    this._formGroup = this._fb.group({
      email: ['', [Validators.required, Validators.pattern(regexEmail)]],
      name : ['',[Validators.required,Validators.minLength(3),
              Validators.maxLength(254)]],
      surname: ['',[Validators.required,Validators.minLength(4),
                Validators.maxLength(254)]],
      password: ['', [ Validators.required,Validators.minLength(4)]],
      c_password: ['', [Validators.required, Validators.minLength(4)]],
      documentType: ['',Validators.required],
      numberIdentification: ['',[Validators.required, Validators.pattern(regexDni)]],
      phone: ['',[Validators.required, Validators.pattern(regexPhone)] ],
      region : [{disabled: true}, [Validators.required]],
      province: [{disabled: true},[Validators.required]],
      distrite: ['',[Validators.maxLength(254), Validators.minLength(3)]],
      rol: ['', Validators.required]
    });


    this._formGroup.get("province").disable();
    this._resquestRegion = new RegionDao();


    this._resquestRegion.getAllRegion(function(data){//busco todas las regiones
      
        self._regions = data;
        self._formGroup.get("region").enable();

       //si mando como parametro el usuario
        let userSelected : any = self._navParams.get("data");

        if(userSelected !== undefined){
          if(userSelected.province !== undefined &&
            userSelected.province !== null){
            
            self._formGroup.get('region').setValue(userSelected.province.regions_id);
          
                self._formGroup.get("province").disable(); 

                self._resquestRegion.getRegionByProvinceId(
                      self._formGroup.get("region").value, function(data){    
                      self._provinces = data;
                      self._formGroup.get("province").enable();  
                      self._formGroup.get("province").setValue(userSelected.province.id);  
                  });
                    
          }
                    
        }

        
    });


     let userSelected : any = this._navParams.get("data");
     

     if(userSelected !== undefined){

        this._id = userSelected.id;

        this._formGroup.get('email').setValue(userSelected.email);
        this._formGroup.get('name').setValue(userSelected.name);
        this._formGroup.get('surname').setValue(userSelected.sur_name);

       
       this._formGroup.get('documentType').setValue(userSelected.type_identification);

        this._formGroup.get('numberIdentification').setValue(userSelected.identification);
        this._formGroup.get('phone').setValue(userSelected.phone);
        this._formGroup.get('rol').setValue(userSelected.rol);
        this._formGroup.get('distrite').setValue(userSelected.distrito);
        this._nameFileUpdate  = userSelected.img;
        if(this._nameFileUpdate !== undefined && this._nameFileUpdate !== null &&
         this._nameFileUpdate !== ''){
          this._validImagen = true;
        }


        this._formGroup.get('password').setValidators([ Validators.minLength(4)]);
        this._formGroup.get('c_password').setValidators([  Validators.minLength(4)]);
     }//userSelected !== undefined



  }//constructor





  ionViewDidLoad() {

  }

  
  
  onProvinceSelect($event):void{
    let self : AdminCreateUserPage = this;
    let userSelected : any = this._navParams.get("data");

    if(this._formGroup.get("region").value !== null){

        self._formGroup.get("province").disable(); 

        this._resquestRegion.getRegionByProvinceId(
            this._formGroup.get("region").value, function(data){    
            self._provinces = data;

            self._formGroup.get("province").enable();  

        });
    }

  }


  /**
   * evento de seleccion de imagen
  */
  public valuesFileChooser(event) : void{
    if(event.base64){
      this._base64 = event.base64;
      this._typeImg = event.typeImagen;
      this._validImagen = true;
    }else if(event.error){
       this._validImagen = false; 
    }
  }


  /**
   * accion al hacer submit en en formulario
  */
  public onFormAction(): void{
         let self : AdminCreateUserPage = this;

    let jsonObject : any = {};

    let dao = new UserDao();

    jsonObject.name = this._formGroup.get('name').value;
    jsonObject.email = this._formGroup.get('email').value;
    jsonObject.sur_name = this._formGroup.get('surname').value;

    jsonObject.rol = this._formGroup.get('rol').value;
    jsonObject.provinces_id = String(this._formGroup.get('province').value);

    if(this._base64 !== null){
      jsonObject.img = this._base64;
    }

    if(this._typeImg !== null){
      jsonObject.typeImg = this._typeImg;
    }

    if(this._typeImg !== null){
      jsonObject.typeImg = this._typeImg;
    }

    jsonObject.rol = this._formGroup.get('rol').value;

    if(this._formGroup.get('distrite').value !== ''){
      jsonObject.distrito = this._formGroup.get('distrite').value;
    }



    
    jsonObject.identification = this._formGroup.get('numberIdentification').value;
    jsonObject.phone = this._formGroup.get('phone').value;
    jsonObject.type_identification = this._formGroup.get('documentType').value;

    jsonObject.password = this._formGroup.get('password').value;
    jsonObject.c_password = this._formGroup.get('c_password').value;


    if(this._id === null){


      dao.createbyAdmin(jsonObject, function(){
        let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });

      });

    }else{

      let userSelected : any = this._navParams.get("data");

      if(userSelected.name === jsonObject.name){
        delete jsonObject.name;
      }

      if(userSelected.email === jsonObject.email){
        delete jsonObject.email;
      }

      if(userSelected.img === jsonObject.img){
        delete jsonObject.img;
      }

      if(jsonObject.sur_name === userSelected.sur_name){
        delete jsonObject.sur_name; 
      }

      if(jsonObject.rol === userSelected.rol){
        delete jsonObject.rol;
      }

      if(jsonObject.password === ''){
        delete jsonObject.password;
      }

      if(jsonObject.c_password === ''){
        delete jsonObject.c_password;
      }

      if(jsonObject.identification === userSelected.identification){
        delete jsonObject.identification;
      }

      if(jsonObject.phone === userSelected.phone){
        delete jsonObject.phone;
      }

      if(jsonObject.type_identification === userSelected.type_identification){
        delete jsonObject.type_identification;
      }

      if(jsonObject.distrito === userSelected.distrito){
        delete jsonObject.distrito;
      }

      if(jsonObject.provinces_id === userSelected.provinces_id){
        delete jsonObject.provinces_id;
      }


      dao.update(this._id, jsonObject, function(){
        let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });

      });




    }//else 

  }//end formaction


  public get formGroup(){
    return this._formGroup;
  }

  

  public get documentType() : string{
      return this._documentType;
  }


  public get provinces(){
    return this._provinces;
  }

  public get regions(){
    return this._regions;
  }

  public get region(){
    return this._region;
  }


  public get validImagen(){
    return this._validImagen;
  }

  public get nameFileUpdate(){
    return this._nameFileUpdate;
  }

  public get id(){
    return this._id;
  }

}


