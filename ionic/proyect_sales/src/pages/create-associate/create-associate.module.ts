import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateAssociatePage } from './create-associate';

@NgModule({
  declarations: [
    CreateAssociatePage,
  ],
  imports: [
    IonicPageModule.forChild(CreateAssociatePage),
  ],
  exports: [
    CreateAssociatePage
  ]
})
export class CreateAssociatePageModule {}
