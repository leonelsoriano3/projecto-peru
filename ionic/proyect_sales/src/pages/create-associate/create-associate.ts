import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { LangCheker } from '../../injecter/lang-cheker';
import { ModalAssociatePage } from '../modal-associate/modal-associate';
import {AssociateDao} from '../../api/dao/asocciate-dao';


@IonicPage()
@Component({
  selector: 'page-create-associate',
  templateUrl: 'create-associate.html',
})
export class CreateAssociatePage {

	private _formGroup : FormGroup;
  
	private _listData = new Array<any>();

  private _id : number = null;

  constructor(private _langCheker : LangCheker, private _navCtrl: NavController, 
  	private _navParams: NavParams,   private _fb: FormBuilder, 
  	private _modalCtrl: ModalController  ) {

     this._formGroup = this._fb.group({
 				name : ['',[Validators.required,Validators.minLength(3),
          Validators.maxLength(254)]],
        description : ['',[Validators.required,Validators.minLength(6),
          Validators.maxLength(1024)]],
     });

    let dataPush : any = this._navParams.get('data');
  
    if( dataPush !== undefined){
      this._id = dataPush.id;
      this._formGroup.get('name').setValue(dataPush.name);
      this._formGroup.get('description').setValue(dataPush.description);
      this._listData = dataPush.products;


    }


    
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad CreateAssociatePage');
  }

    /**
   * accion al hacer submit en en formulario
  */
  public onFormAction(): void{
    
    let self : CreateAssociatePage = this;
    
    let associateDao = new AssociateDao();

    let jsonObject = {};

    jsonObject["name"] = this._formGroup.get('name').value;
    jsonObject['description'] = this._formGroup.get('description').value;

    jsonObject['products'] = this._listData;

    if(this._id === null){
      associateDao.create(jsonObject, function(){
          let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });
      });
    }else{
    
      associateDao.update(this._id, jsonObject, function(){
        let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });

      });


    }


  }

  public openModalItems():void{

    let modal = this._modalCtrl.create(ModalAssociatePage);
    
	  modal.onDidDismiss(data => {
	    this._listData = data
	  });

    modal.present();
  }

  public itemPressed(index: number): void{
  
  	 this._listData.splice(index, 1);

  }

  //getter public set 
  public get formGroup() {
  	return this._formGroup;
  }

  public get listData(){
  	return this._listData;
  }



}
