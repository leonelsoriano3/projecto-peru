import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { LangCheker } from '../../injecter/lang-cheker';
import {CategoryDao} from '../../api/dao/category-dao';


@IonicPage()
@Component({
  selector: 'page-create-category',
  templateUrl: 'create-category.html',
})
export class CreateCategoryPage {

  	private _formGroup : FormGroup;
  

    private _id : number = null;

  constructor(private _langCheker : LangCheker, private _navCtrl: NavController, 
  	private _navParams: NavParams,   private _fb: FormBuilder, 
  	private _modalCtrl: ModalController  ) {

     this._formGroup = this._fb.group({
 				name : ['',[Validators.required,Validators.minLength(3),
          Validators.maxLength(254)]],
        description : ['',[Validators.required,Validators.minLength(6),
          Validators.maxLength(1024)]],
     });

    let dataPush : any = this._navParams.get('data');
  
    if( dataPush !== undefined){
      this._id = dataPush.id;
      this._formGroup.get('name').setValue(dataPush.name);
      this._formGroup.get('description').setValue(dataPush.description);
    }

    
  }


  ionViewDidLoad() {
    //console.log('ionViewDidLoad CreateAssociatePage');
  }

  /**
   * accion al hacer submit en en formulario
  */
  public onFormAction(): void{
    
    let self : CreateCategoryPage = this;
    
    let categoryDao = new CategoryDao();

    let jsonObject = {};

    jsonObject["name"] = this._formGroup.get('name').value;
    jsonObject['description'] = this._formGroup.get('description').value;


    if(this._id === null){
      categoryDao.create(jsonObject, function(){
        let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });

      });
    }else{
    
      categoryDao.update(this._id, jsonObject, function(){
        let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });

      });


    }

  }//end onFormAction











  //getter public set 
  public get formGroup() {
  	return this._formGroup;
  }



}
