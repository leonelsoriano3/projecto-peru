import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { LangCheker } from '../../injecter/lang-cheker';
import { FileChooser } from '@ionic-native/file-chooser';
import { CurrencyPipe } from '@angular/common';
import {ProductsDao} from "../../api/dao/product-dao";
import { PriceValidatorServiceProvider } from 
  '../../providers/price-validator-service/price-validator-service';
import {CategoryDao} from '../../api/dao/category-dao';
import {SubCategoryDao} from '../../api/dao/sub-category-dao';


@IonicPage()
@Component({
  selector: 'page-create-product',
  templateUrl: 'create-product.html',
  providers: [CurrencyPipe, LangCheker]
})
export class CreateProductPage {

  private _formGroup : FormGroup;

  private _base64 : string;

  private _validImagen : boolean;

  private _id : number = null;

  //este es para cambiarle el nombre del seleccioanda al componete :D
  private _nameFileUpdate : string = null;

  
  private _listCategory = new Array<any>();

  private _listSubCategory = new Array<any>();

  private _typeImg : string;


  constructor(private _langCheker : LangCheker, private _navCtrl: NavController, 
  	private _navParams: NavParams,   private _fb: FormBuilder, 
    private _fileChooser: FileChooser,private _currencyPipe: CurrencyPipe ) {
  
    let self : CreateProductPage = this;
   
    this._validImagen = false; 

    this._base64 = null;

     this._formGroup = this._fb.group({
        name : ['',[Validators.required,Validators.minLength(3),
          Validators.maxLength(254)]],
        description : ['',[Validators.required,Validators.minLength(6),
          Validators.maxLength(1024)]],
        price : ['',[PriceValidatorServiceProvider.len(15)]],
        category : ['',[Validators.required]],
        subCategory : ['',[Validators.required]],
        active: ['true']

     });

    let dataPush : any = this._navParams.get('data');

    if( dataPush !== undefined){
      this._id = dataPush.id;
      this._formGroup.get('name').setValue(dataPush.name);
      this._formGroup.get('description').setValue(dataPush.description);
      
      if(dataPush.price !== undefined){
        this._formGroup.get('price').setValue(dataPush.price.amount);
      }

      if(dataPush.active === 1){
        this._formGroup.get('active').setValue('true');
      }else{
        this._formGroup.get('active').setValue('false');
      }

      this._nameFileUpdate  = dataPush.img;
      this._validImagen = true;

      if(dataPush.category !== undefined){
        this._formGroup.get('category').setValue(dataPush.category.category_id);
        this._formGroup.get('subCategory').setValue(dataPush.category.sub_category_id);
      }

    }

    

    
    let categoryDao = new CategoryDao();
    categoryDao.getAll(true,null ,null ,function(data){
      
      if(data !== null){
        for(let i = 0; i < data.length; i++){
          self._listCategory.push(data[i]); 
        }
 
      }

    });//end getAll


    let subCategoryDao = new SubCategoryDao();

    subCategoryDao.getAll(true, null ,null ,function(data){
      if(data !== null){
        for(let i = 0; i < data.length; i++){
          self._listSubCategory.push(data[i]); 
        }
         
      }

    });



  
  }//end construct

  ionViewDidLoad() {

  }



  
  /**
   * accion al hacer submit en en formulario
  */
  public onFormAction(): void{


    let self : CreateProductPage = this;

    
    let jsonObject : any = {};
    
    let productsDao = new ProductsDao();
    
    if(this._base64 !== null){
      jsonObject.img = this._base64;
    }

    if(this._typeImg !== null){
      jsonObject.type_img = this._typeImg;
    }

    if(this._formGroup.get('name').dirty){
        jsonObject.name = this._formGroup.get('name').value;
    }

    if(this._formGroup.get('description').dirty){
      jsonObject.description = this._formGroup.get('description').value;
    }

    jsonObject.category_id = this._formGroup.get('category').value;

    jsonObject.sub_category_id = this._formGroup.get('subCategory').value;
    
    jsonObject.active = this._formGroup.get('active').value;

    if(this._formGroup.get('price').value !== null && 
      this._formGroup.get('price').value.length != 0  &&
      this._formGroup.get('price').dirty){
      jsonObject.price = this._formGroup.get('price').value;
    }
    
    if(this._id === null){
      productsDao.create(jsonObject, function(){
        let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });

      });

  
    }else{
      productsDao.update(this._id, jsonObject, function(){
        let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });

      });
    
    
    }

  }//end onsubmit



  /**
   * evento de seleccion de imagen
  */
  public valuesFileChooser(event) : void{
    
    if(event.base64){
      this._base64 = event.base64;
      this._typeImg = event.typeImagen;
      this._validImagen = true;
       
    }else if(event.error){
      this._base64 = null;
      this._validImagen = false; 
    }
  }



  public onChangePrice(evt) {
    
    if(evt != ''){
      //console.log(this._currencyPipe.transform(evt, 'EUR', true, '1.2-2'));
    }
  }


  public onPriceUp(evt){
    
    if(evt != ''){
      //console.log(this._currencyPipe.transform(evt, 'EUR', true, '1.2-2'));
    }
  }


  public get formGroup(){
    return this._formGroup;
  }


  public get validImagen(){
    return this._validImagen;
  }

  public get nameFileUpdate(){
    return this._nameFileUpdate;
  }

  public get listCategory(){

     return this._listCategory;
  }

  public get listSubCategory(){

     return this._listSubCategory;
  }

}
