import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateSubCategoryPage } from './create-sub-category';

@NgModule({
  declarations: [
    CreateSubCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateSubCategoryPage),
  ],
  exports: [
    CreateSubCategoryPage
  ]
})
export class CreateSubCategoryPageModule {}
