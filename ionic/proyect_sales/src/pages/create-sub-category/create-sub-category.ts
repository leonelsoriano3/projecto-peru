import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { LangCheker } from '../../injecter/lang-cheker';
import {SubCategoryDao} from '../../api/dao/sub-category-dao';

/**
 * Generated class for the CreateSubCategoryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-create-sub-category',
  templateUrl: 'create-sub-category.html',
})
export class CreateSubCategoryPage {

  private _formGroup : FormGroup;
  
  private _id : number = null;

  constructor(private _langCheker : LangCheker, private _navCtrl: NavController, 
  	private _navParams: NavParams,   private _fb: FormBuilder, 
  	private _modalCtrl: ModalController  ) {

     this._formGroup = this._fb.group({
 				name : ['',[Validators.required,Validators.minLength(3),
          Validators.maxLength(254)]],
        description : ['',[Validators.required,Validators.minLength(6),
          Validators.maxLength(1024)]],
     });

    let dataPush : any = this._navParams.get('data');
  
    if( dataPush !== undefined){
      this._id = dataPush.id;
      this._formGroup.get('name').setValue(dataPush.name);
      this._formGroup.get('description').setValue(dataPush.description);
    }

  }


  ionViewDidLoad() {
    //console.log('ionViewDidLoad CreateAssociatePage');
  }

  /**
   * accion al hacer submit en en formulario
  */
  public onFormAction(): void{
    
    let self : CreateSubCategoryPage = this;
    
    let subCategoryDao = new SubCategoryDao();

    let jsonObject = {};

    jsonObject["name"] = this._formGroup.get('name').value;
    jsonObject['description'] = this._formGroup.get('description').value;


    if(this._id === null){
      subCategoryDao.create(jsonObject, function(){
        let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });

      });
    }else{
    
      subCategoryDao.update(this._id, jsonObject, function(){
        let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });

      });

    }

  }//end onFormAction




  //getter public set 
  public get formGroup() {
  	return this._formGroup;
  }



}
