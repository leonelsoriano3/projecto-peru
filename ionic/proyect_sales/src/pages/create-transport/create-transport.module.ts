import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateTransportPage } from './create-transport';

@NgModule({
  declarations: [
    CreateTransportPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateTransportPage),
  ],
  exports: [
    CreateTransportPage
  ]
})
export class CreateTransportPageModule {}
