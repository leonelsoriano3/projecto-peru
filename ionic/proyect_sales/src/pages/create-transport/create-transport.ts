import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { LangCheker } from '../../injecter/lang-cheker';
import { PriceValidatorServiceProvider } from 
  '../../providers/price-validator-service/price-validator-service';
import {TransportDao} from '../../api/dao/transport-dao';

@IonicPage()
@Component({
  selector: 'page-create-transport',
  templateUrl: 'create-transport.html',
  providers: [LangCheker]
})
export class CreateTransportPage {

  private _formGroup : FormGroup;
  private _listData = new Array<any>();
  private _id : number = null;

  constructor(private _langCheker : LangCheker, private _navCtrl: NavController, 
  	private _navParams: NavParams,   private _fb: FormBuilder) {

    this._formGroup = this._fb.group({
		name : ['',[Validators.required,Validators.minLength(3),
          Validators.maxLength(254)]],
        delay : ['',[PriceValidatorServiceProvider.len(15)]],
        active: ['true'],
        free_sending: ['true'],
        logo: ['1'],
        tracing: ['',[Validators.minLength(3),Validators.maxLength(254)]],
        tax: ['0'],
        weight_tax_max: ['', [PriceValidatorServiceProvider.len(15)]],
        weight_tax_min: ['', [PriceValidatorServiceProvider.len(15)]],
        billing_type: ['1'],
        width_package: ['',[PriceValidatorServiceProvider.len(15)]],
        high_package : ['',[PriceValidatorServiceProvider.len(15)]],
        long_package : ['',[PriceValidatorServiceProvider.len(15)]],
        weight_package : ['',[PriceValidatorServiceProvider.len(15)]], 

    });

    let dataPush : any = this._navParams.get('data');

    if( dataPush !== undefined){          
      this._id = dataPush.id;
      this._formGroup.get('name').setValue(dataPush.name);
      if(dataPush.delay !== undefined){
        this._formGroup.get('delay').setValue(dataPush.delay);
      }

      if(dataPush.active !== undefined){
        this._formGroup.get('active').setValue(dataPush.active);
      }


      if(dataPush.free_sending !== undefined){
        this._formGroup.get('free_sending').setValue(dataPush.free_sending);
      }

      if(dataPush.logo !== undefined){
        this._formGroup.get('logo').setValue(dataPush.logo);
      }

      if(dataPush.tracing !== undefined){
        this._formGroup.get('tracing').setValue(dataPush.tracing);
      }

      if(dataPush.tax_id !== undefined){
        this._formGroup.get('tax').setValue(dataPush.tax_id);
      }

      if(dataPush.weight_tax_max !== undefined){
        this._formGroup.get('weight_tax_max').setValue(dataPush.weight_tax_max);        
      }

      if(dataPush.weight_tax_min !== undefined){
        this._formGroup.get('weight_tax_min').setValue(dataPush.weight_tax_min);        
      }


      if(dataPush.weight_tax_min !== undefined){
        this._formGroup.get('weight_tax_min').setValue(dataPush.weight_tax_min);        
      }

      if(dataPush.billing_type !== undefined){
        this._formGroup.get('billing_type').setValue(dataPush.billing_type);        
      }

      if(dataPush.width_package !== undefined){
        this._formGroup.get('width_package').setValue(dataPush.width_package);        
      }

      if(dataPush.high_package !== undefined){
        this._formGroup.get('high_package').setValue(dataPush.high_package);        
      }

      if(dataPush.long_package !== undefined){
        this._formGroup.get('long_package').setValue(dataPush.long_package);        
      }

      if(dataPush.weight_package !== undefined){
        this._formGroup.get('weight_package').setValue(dataPush.weight_package);        
      }

      
    }//end data push


  }//end construc



  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateTransportPage');
  }



  public onFormAction():void{

    let self : CreateTransportPage = this;

    
    let jsonObject : any = {};
    
    let transportDao = new TransportDao();

    if(this._formGroup.get('name').dirty){
      jsonObject.name = this._formGroup.get('name').value;
    }
   
    if(this._formGroup.get('tracing').dirty){
      jsonObject.tracing = this._formGroup.get('tracing').value;
    }

    if(this._formGroup.get('delay').dirty){
      jsonObject.delay = this._formGroup.get('delay').value;
    }

    if(this._formGroup.get('weight_tax_max').dirty){
      let itemValue = (this._formGroup.get('weight_tax_max').value === '')? '0' :
        this._formGroup.get('weight_tax_max').value; 
      jsonObject.weight_tax_max = itemValue;
    }


    if(this._formGroup.get('weight_tax_min').dirty){
      let itemValue = (this._formGroup.get('weight_tax_min').value === '')? '0' :
        this._formGroup.get('weight_tax_min').value; 
      jsonObject.weight_tax_min = itemValue;
    }

    if(this._formGroup.get('weight_tax_min').dirty){
      let itemValue = (this._formGroup.get('weight_tax_min').value === '')? '0' :
        this._formGroup.get('width_package').value; 
      jsonObject.weight_tax_min = itemValue;
    }


    if(this._formGroup.get('width_package').dirty){
      let itemValue = (this._formGroup.get('width_package').value === '')? '0' : 
        this._formGroup.get('width_package').value; 
      jsonObject.width_package = itemValue;
    }

    if(this._formGroup.get('high_package').dirty){
      let itemValue = (this._formGroup.get('high_package').value === '')? '0' : 
        this._formGroup.get('high_package').value; 
      jsonObject.high_package = itemValue;
    }


    if(this._formGroup.get('long_package').dirty){
      let itemValue = (this._formGroup.get('long_package').value === '')? '0' : 
        this._formGroup.get('long_package').value; 
      jsonObject.long_package = itemValue;
    }

    if(this._formGroup.get('weight_package').dirty){
      let itemValue = (this._formGroup.get('weight_package').value === '')? '0' : 
        this._formGroup.get('weight_package').value; 
      jsonObject.weight_package = itemValue;
    }

    if(this._formGroup.get('high_package').dirty){
      let itemValue = (this._formGroup.get('free_sending').value === '')? '0' : 
        this._formGroup.get('free_sending').value; 
      jsonObject.high_package = itemValue;
    }
  
    jsonObject.logo = this._formGroup.get('logo').value;
    jsonObject.tax_id = this._formGroup.get('tax').value;
    jsonObject.billing_type = this._formGroup.get('billing_type').value;

    if(this._id === null){
      transportDao.create(jsonObject, function(){
        let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });

      });

  
    }else{

      transportDao.update(this._id,jsonObject, function(){
        let option : object = {reload : true};
         let callback = self._navParams.get("callback");
          callback(true).then(()=>{
            self._navCtrl.pop();
          });

      });

    }


  }//end onFormAction




   //getter public set 
  public get formGroup() {
  	return this._formGroup;
  }

  public get listData(){
  	return this._listData;
  }


}
