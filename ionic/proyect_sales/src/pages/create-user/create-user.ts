import { Component } from '@angular/core';
import { LangCheker } from '../../injecter/lang-cheker';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RegionDao} from '../../api/dao/region-dao';
import { FileChooser } from '@ionic-native/file-chooser';
import {UserRegisterDto} from '../../api/dto/user-register';
import {UserDao} from "../../api/dao/user-dao";
import {LoginPage} from '../login/login';


@IonicPage()
@Component({
  selector: 'page-create-user',
  templateUrl: 'create-user.html',
  providers: [LangCheker],
})
export class CreateUserPage{

  private _loginForm : FormGroup;

  private _documentType: string;

  private _provinces : any[];
  
  private _regions : any[];

  private _region : string;

  private _resquestRegion : RegionDao;

  private _base64 : string;

  private _validImagen : boolean;

  private _typeImg : string;


  constructor(private _langCheker : LangCheker, private _navCtrl: NavController, 
  		private _navParams: NavParams,   private _fb: FormBuilder, private _fileChooser: FileChooser) {

    //verdadero porque la imagen no es required  
    this._validImagen = true;

    let self : CreateUserPage = this;

    let regexDni = "^\\d{8}(\\-)\\d{1}$";
    let regexPhone = "^\\+[1-9]{1}[0-9]{3,14}$";
    let regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    this._loginForm = this._fb.group({
            email: ['', [Validators.required, Validators.pattern(regexEmail)]],
            name : ['',[Validators.required,Validators.minLength(3),
              Validators.maxLength(254)]],
            surname: ['',[Validators.required,Validators.minLength(4),
                Validators.maxLength(254)]],
            documentType: ['',Validators.required],
            numberIdentification: ['',[Validators.required, Validators.pattern(regexDni)]],
            phone: ['',[Validators.required, Validators.pattern(regexPhone)] ],
            province: [{disabled: true},[Validators.required]],
            region : [{disabled: true}, [Validators.required]],
            distrite: ['',[Validators.required ,Validators.maxLength(254), Validators.minLength(3)]],
            password: ['', [Validators.required, Validators.minLength(4)]],
            c_password: ['', [Validators.required, Validators.minLength(4)]]
    });

    this._loginForm.get("province").disable();
    this._resquestRegion = new RegionDao();
    this._resquestRegion.getAllRegion(function(data){
        self._regions = data;
        self._loginForm.get("region").enable();        
        
    });


  }//end construct

  onProvinceSelect($event):void{
    let self : CreateUserPage = this;

    if(this._loginForm.get("region").value !== null){

        self._loginForm.get("province").disable(); 

        this._resquestRegion.getRegionByProvinceId(
            this._loginForm.get("region").value, function(data){    
            self._provinces = data;
            self._loginForm.get("province").enable();    
        });
    }

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad CreateUserPage');
    
  }

  /**
   * evento de seleccion de imagen
   */
  public valuesFileChooser(event) : void{
    if(event.base64){
      this._base64 = event.base64;
      this._validImagen = true;
      this._typeImg = event.typeImagen;
    }else if(event.error){
       this._validImagen = false; 
    }
  }

  public onCreatUser() : void{

    let self : CreateUserPage = this;
      
    let userRegisterDto : UserRegisterDto = new UserRegisterDto();

    userRegisterDto.name = this._loginForm.get("name").value; 
    userRegisterDto.email = this._loginForm.get("email").value;
    userRegisterDto.surName = this._loginForm.get("surname").value; 
    userRegisterDto.password = this._loginForm.get("password").value; 
    userRegisterDto.cPassword = this._loginForm.get("c_password").value;   
    userRegisterDto.distrito = this._loginForm.get("distrite").value;
    userRegisterDto.identification = this._loginForm.get("numberIdentification").value; 
    userRegisterDto.img = this._base64;

    userRegisterDto.typeImg = this._typeImg;
    userRegisterDto.phone = this._loginForm.get("phone").value; 
    userRegisterDto.typeIdentification = this._loginForm.get("documentType").value; 
    userRegisterDto.provincesId = this._loginForm.get("province").value; 
    console.log( userRegisterDto.toJsonObject);
    let userDao : UserDao = new UserDao();

    userDao.registerUser(userRegisterDto, function(){
        self._loginForm.reset();
        self._loginForm.get("province").disable();
        self._provinces = [];
    });
        
  }//end on create user

  public goLogin(): void{
    this._navCtrl.setRoot(LoginPage);
  }


  public get documentType() : string{
      return this._documentType;
  }

  public get loginForm(): FormGroup{
        return this._loginForm;
  }

  public get provinces(){
    return this._provinces;
  }

  public get regions(){
    return this._regions;
  }

  public get region(){
    return this._region;
  }


  public get validImagen(){
    return this._validImagen;
  }



}
