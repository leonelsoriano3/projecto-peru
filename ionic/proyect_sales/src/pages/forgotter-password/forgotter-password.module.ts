import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForgotterPasswordPage } from './forgotter-password';

@NgModule({
  declarations: [
    ForgotterPasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(ForgotterPasswordPage),
  ],
  exports: [
    ForgotterPasswordPage
  ]
})
export class ForgotterPasswordPageModule {}
