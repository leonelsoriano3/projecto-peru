import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LangCheker } from '../../injecter/lang-cheker';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserDao} from "../../api/dao/user-dao";
import {LoginPage} from '../login/login';

@IonicPage()
@Component({
  selector: 'page-forgotter-password',
  templateUrl: 'forgotter-password.html',
  providers: [LangCheker]
})
export class ForgotterPasswordPage {

  private _formGroup : FormGroup;

  constructor(private _langCheker : LangCheker,
      public navCtrl: NavController, public navParams: NavParams, 
      private _fb: FormBuilder) {
        
        let regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        this._formGroup = this._fb.group({
            email: ['', [Validators.required, Validators.pattern(regexEmail)]]
        });
  }

  public onSerndEmailForggoten(){
     let self : ForgotterPasswordPage = this;
    
      let userDao : UserDao = new UserDao();
      
      userDao.forgotterPassword(
          this._formGroup.get('email').value, function(){
        self.goLogin();
      });

  } 


  ionViewDidLoad() {
    //console.log('ionViewDidLoad FoogotterPasswordPage');
  }

  public goLogin(): void{
    this.navCtrl.setRoot(LoginPage);
  }

  public get formGroup(){
    return this._formGroup;
  }

}
