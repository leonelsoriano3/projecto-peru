import { Component, ViewChild } from '@angular/core';
import { NavController, Nav, AlertController } from 'ionic-angular';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';


import {AdminCreateUserPage} from '../admin-create-user/admin-create-user';
import {AdminHomePage} from '../admin-home/admin-home';
import {ListUserPage} from '../list-user/list-user';
import {ListAssociatePage} from '../list-associate/list-associate';
import {ListCategoryPage} from '../list-category/list-category';
import {ListSubCategoryPage} from '../list-sub-category/list-sub-category';
import {UpdateUserPage} from '../update-user/update-user';
import {ListTransportPage} from '../list-transport/list-transport';
import { ListMethodPaymentPage} from '../list-method-payment/list-method-payment';
import { ParameterPage} from '../parameter/parameter';
import { UserMapPage} from '../user-map/user-map';

import { UserPurchasePage } from '../user-purchase/user-purchase';

import {ProductListPage} from '../product-list/product-list';
import { LangCheker } from '../../injecter/lang-cheker'
import {Session} from '../../injecter/session';


import {EndPointConfig} from "../../config/endpoint-config";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [LangCheker]
})
export class HomePage {

@ViewChild(Nav) nav: Nav;

  rootPage: any = AdminHomePage;

  pages: Array<{title: string, component: any}>;

    
  constructor(public _nav: NavController,
    private _langCheker : LangCheker, 
    private _alertCtrl: AlertController) {
     
    //verifico la session
   // new Session(this._nav).internalControlForLogin();

    //conseguir tipo de usuario
    //var userRol = parseInt(localStorage.getItem('user.rol'));

    //this.pages : []= null;
    //if(isNaN(userRol)){
     // this.closeSession();
    //}
    //

    if(localStorage.getItem('user.rol') === '1' ||
      localStorage.getItem('user.rol') === '2'
    ){
    
      this.pages = [
        { title: 'HOME', component : AdminHomePage},
        { title: 'USERS', component: ListUserPage},
        { title: 'PRODUCTS', component: ProductListPage},
        {title: 'ASSOCIATE', component : ListAssociatePage},
        {title: 'CATEGORY', component: ListCategoryPage},
        {title: 'SUB-CATEGORY', component: ListSubCategoryPage},
        {title: 'TRANSPORT', component: ListTransportPage},
        {title: 'METHOD_PAYMENT', component: ListMethodPaymentPage},
        {title: 'PARAMETERS', component: ParameterPage}
      ];


    }else if(localStorage.getItem('user.rol') === '3'){
    
    }else if(localStorage.getItem('user.rol') === '4'){
    
      this.pages = [
        { title: 'UPDATE_USER', component : UpdateUserPage},
        {title: 'MAP_USER', component : UserMapPage},
        {title: 'USER_PURCHASE', component: UserPurchasePage}
      ];
    
    }else{
      this.pages = [
      ];

    
    }
  
  
  }//end construct

  
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.title === 'UPDATE_USER'){
      this._nav.push(page.component);
    }else{
      this.nav.setRoot(page.component);
    }
    
  }

  /**
  * cierra la session de la aplicacion
  */
  public closeSession(){

        let alert = this._alertCtrl.create({
      title: 'Esta seguro de cerrar la sessión',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          
          }
        },
        {
          text: 'Confimar',
          handler: () => {
           
            new Session(this._nav).closeSession();

          }
        }
      ]
    });
    alert.present();




  }


  public getDataImagen() : any{

    let rol : string;

    if(localStorage.getItem('user.rol') === '1'){
      rol = 'ROOT'
    }if(localStorage.getItem('user.rol') === '2'){
      rol = 'ADMIN';
    }else if(localStorage.getItem('user.rol') === '3'){
      rol = 'SELLER';
    }else if(localStorage.getItem('user.rol') === '4'){
      rol = 'USER';
    }

    return {
      img : (localStorage.getItem('user.img') === undefined || 
            localStorage.getItem('user.img') === null ||
             localStorage.getItem('user.img') === 'null' ||
            localStorage.getItem('user.img') === 'undefined' 
            )? EndPointConfig.DEFAULT_IMG : EndPointConfig.BASE_IMG + localStorage.getItem('user.img'),
      name : localStorage.getItem('user.name'),
      rol :  rol,
      email: localStorage.getItem('user.email')
    };
  }

}
