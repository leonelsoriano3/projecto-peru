import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListAssociatePage } from './list-associate';

@NgModule({
  declarations: [
    ListAssociatePage,
  ],
  imports: [
    IonicPageModule.forChild(ListAssociatePage),
  ],
  exports: [
    ListAssociatePage
  ]
})
export class ListAssociatePageModule {}
