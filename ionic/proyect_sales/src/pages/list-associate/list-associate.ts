import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, 
  AlertController,ToastController } from 'ionic-angular';
import { CreateAssociatePage } from "../create-associate/create-associate";
import {AssociateDao} from '../../api/dao/asocciate-dao';

@IonicPage()
@Component({
  selector: 'page-list-associate',
  templateUrl: 'list-associate.html',
})
export class ListAssociatePage {


  private _searchQuery: string;

  private _pullRequest : number;
  //private _productDao : ProductsDao;
  private _listData = new Array<any>();

  constructor(private _nav: NavController, private _navParams: NavParams,
    private _alertCtrl: AlertController, private _toastCtrl: ToastController) {
    
    //let self : ProductListPage = this;
    //this._productDao = new ProductsDao();
    this._pullRequest = 0;
    this._makeRequestList(true,null);
  }



  ionViewDidLoad(){
    //console.log('ionViewDidLoad ListAssociatePage');  
  }


  /**
  *  de la busqueda por texto
  * @param evento de js
  */
  public valuechangeSearch(e):void{
    //console.log(e);
    if(e.keyCode === 13){
      this._pullRequest = 0;
      this._listData = new Array<any>()
      this._makeRequestList(true,null);
    }
  }


  /**
  * metodo del refres de ionic
  * @param refresher es el refresher inyectado de ionic 
  */
  public doRefresh(refresher){
    this._pullRequest++;
    this._makeRequestList(false,refresher);      
  };


  public addResource():void{

    let self : ListAssociatePage = this;


    let paramPopCallback = function(_params) {
       return new Promise((resolve, reject) => {
              if(_params){
                  self._listData = new Array<any>();
                  self._pullRequest = 0;
                  self._makeRequestList(true,null);
              } 
              resolve();
           });
   }

    this._nav.push(CreateAssociatePage,{
      callback : paramPopCallback
    }); 
  
  }



  /**
   * llamo a el recuest globalizado
   * @param loader: boolean si se quiere hacer la peticion con el loader o no
   * @param refresher : any si se quiere llemar la petion con el refresher de la vista
  */
  private _makeRequestList(loader : boolean, refresher : any){

    let self : ListAssociatePage = this;

    let associateDao : AssociateDao = new AssociateDao();

    let jsonData : any = {};

    if(this._searchQuery !== undefined &&
      this._searchQuery !== null && 
      this._searchQuery.length > 0){
      jsonData["textSearch"] = this._searchQuery;
    }
  
    associateDao.getAll(loader,this._pullRequest ,jsonData ,function(data){
      if(data !== null){
        for(let i = 0; i < data.length; i++){
          self._listData.unshift(data[i]); 
        }
         
      }
      if(refresher !== null){
        refresher.complete();
      }
    });
  
  }//makeRequestList

  
  /**
   * click en la lista
   */ 
  itemPressed(itemData: any):void{
    let self : ListAssociatePage = this;

    let alert = this._alertCtrl.create({
      title: 'Decea eliminar esta associación',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          
          }
        },
        {
          text: 'Confimar',
          handler: () => {
            let associateDao : AssociateDao = new AssociateDao();

            associateDao.deleteOne(itemData.id, data =>{ 
              self._listData = new Array<any>();
              self._pullRequest = 0;
              self._makeRequestList(true,null);

            });

          }
        }
      ]
    });
    alert.present();



  }

  itemTapped(itemData):void{
    let self : ListAssociatePage = this;

    let paramPopCallback = function(_params) {
       return new Promise((resolve, reject) => {
              if(_params){
                  self._listData = new Array<any>();
                  self._pullRequest = 0;
                  self._makeRequestList(true,null);
              } 
              resolve();
           });
    }


    this._nav.push(CreateAssociatePage, {
      data : itemData,
      callback : paramPopCallback
    });

  }


  //getter setter


  public get searchQuery(){
    return this._searchQuery;
  }

  public set searchQuery(searchQueryParam : string){
    this._searchQuery = searchQueryParam;
  }

  public get listData(){
    return this._listData;
  }

}
