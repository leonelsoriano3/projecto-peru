import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, 
  AlertController,ToastController } from 'ionic-angular';
import {CreateCategoryPage} from "../create-category/create-category";
import {CategoryDao} from '../../api/dao/category-dao';


@IonicPage()
@Component({
  selector: 'page-list-category',
  templateUrl: 'list-category.html',
})
export class ListCategoryPage {

  private _searchQuery: string;

  private _pullRequest : number;
  
  private _listData = new Array<any>();

  constructor(private _nav: NavController, private _navParams: NavParams,
    private _alertCtrl: AlertController, private _toastCtrl: ToastController) {
    

    this._pullRequest = 0;
    this._makeRequestList(true,null);
  }



  ionViewDidLoad(){
    //console.log('ionViewDidLoad ListAssociatePage');  
  }


  /**
  *  de la busqueda por texto
  * @param evento de js
  */
  public valuechangeSearch(e):void{
    //console.log(e);
    if(e.keyCode === 13){
      this._pullRequest = 0;
      this._listData = new Array<any>()
      this._makeRequestList(true,null);
    }
  }


  /**
  * metodo del refres de ionic
  * @param refresher es el refresher inyectado de ionic 
  */
  public doRefresh(refresher){
    this._pullRequest++;
    this._makeRequestList(false,refresher);      
  }


  public addResource():void{

    let self : ListCategoryPage = this;


    let paramPopCallback = function(_params) {
       return new Promise((resolve, reject) => {
              if(_params){
                  self._listData = new Array<any>();
                  self._pullRequest = 0;
                  self._makeRequestList(true,null);
              } 
              resolve();
           });
   }

    this._nav.push(CreateCategoryPage,{
      callback : paramPopCallback
    }); 
  
  }



  /**
   * llamo a el request globalizado
   * @param loader: boolean si se quiere hacer la peticion con el loader o no
   * @param refresher : any si se quiere llemar la petion con el refresher de la vista
  */
  private _makeRequestList(loader : boolean, refresher : any){

    let self : ListCategoryPage = this;

    let categoryDao : CategoryDao = new CategoryDao();

    let jsonData : any = {};

    if(this._searchQuery !== undefined &&
      this._searchQuery !== null && 
      this._searchQuery.length > 0){
      jsonData["textSearch"] = this._searchQuery;
    }
  
    categoryDao.getAll(loader,this._pullRequest ,jsonData ,function(data){
      if(data !== null){
        for(let i = 0; i < data.length; i++){
          self._listData.unshift(data[i]); 
        }
         
      }
      if(refresher !== null){
        refresher.complete();
      }
    });
  
  }//makeRequestList

  
  /**
   * click en la lista
   */ 
  itemPressed(itemData: any):void{
    let self : ListCategoryPage = this;

    let alert = this._alertCtrl.create({
      title: 'Desea eliminar esta associación',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          
          }
        },
        {
          text: 'Confimar',
          handler: () => {
            let categoryDao : CategoryDao = new CategoryDao();

            categoryDao.deleteOne(itemData.id, data =>{ 
              self._listData = new Array<any>();
              self._pullRequest = 0;
              self._makeRequestList(true,null);

            });

          }
        }
      ]
    });
    alert.present();

  }//itempressed


  itemTapped(itemData):void{
    let self : ListCategoryPage = this;

    let paramPopCallback = function(_params) {
       return new Promise((resolve, reject) => {
              if(_params){
                  self._listData = new Array<any>();
                  self._pullRequest = 0;
                  self._makeRequestList(true,null);
              } 
              resolve();
           });
    }


    this._nav.push(CreateCategoryPage, {
      data : itemData,
      callback : paramPopCallback
    });

  }


  //getter setter


  public get searchQuery(){
    return this._searchQuery;
  }

  public set searchQuery(searchQueryParam : string){
    this._searchQuery = searchQueryParam;
  }

  public get listData(){
    return this._listData;
  }
}
