import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListMethodPaymentPage } from './list-method-payment';

@NgModule({
  declarations: [
    ListMethodPaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(ListMethodPaymentPage),
  ],
  exports: [
    ListMethodPaymentPage
  ]
})
export class ListMethodPaymentPageModule {}
