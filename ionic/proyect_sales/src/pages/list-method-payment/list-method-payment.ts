import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, 
  AlertController,ToastController } from 'ionic-angular';
import { PaymentMethodDao } from '../../api/dao/payment-method-dao';




@IonicPage()
@Component({
  selector: 'page-list-method-payment',
  templateUrl: 'list-method-payment.html',
})
export class ListMethodPaymentPage {


  private _searchQuery: string;
  private _pullRequest : number;
  private _listData = new Array<any>();
  private _entityState : string;



  constructor(private _nav: NavController, private _navParams: NavParams,
    private _alertCtrl: AlertController, private _toastCtrl: ToastController) {

    this._pullRequest = 0;
    this._makeRequestList(true,null);
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ListMethodPaymentPage');
  }


  /**
  * metodo del refres de ionic
  * @param refresher es el refresher inyectado de ionic 
  */
  public doRefresh(refresher){
    this._pullRequest++;
    this._makeRequestList(false,refresher);      
  }



  /**
   * llamo a el request globalizado
   * @param loader: boolean si se quiere hacer la peticion con el loader o no
   * @param refresher : any si se quiere llemar la petion con el refresher de la vista
  */
  private _makeRequestList(loader : boolean, refresher : any){

    let self : ListMethodPaymentPage = this;

    let  paymentMethodDao = new PaymentMethodDao();

    let jsonData : any = {};



    if(this._searchQuery !== undefined &&
      this._searchQuery !== null && 
      this._searchQuery.length > 0){
      jsonData.textSearch = this._searchQuery;
    }

    jsonData.state = this._entityState;

  
    paymentMethodDao.getAll(loader,this._pullRequest ,jsonData ,function(data){
      if(data !== null){
        for(let i = 0; i < data.length; i++){
          self._listData.unshift(data[i]); 
        }
         
      }
      if(refresher !== null){
        refresher.complete();
      }
    });
  
  }//makeRequestList



  /**
   * click en la lista
   */ 
  itemPressed(itemData: any):void{

  
    let self : ListMethodPaymentPage = this;

    let alert = this._alertCtrl.create();
    alert.setTitle('Activa/Desactiva usuario');
    alert.addInput({type: 'radio', label: 'Activar', value: '1', checked: true});
    alert.addInput({type: 'radio', label: 'Desactivar', value: '0'});
    alert.addButton('Cancelar');
    alert.addButton({
      text: 'OK',
      handler: data => {
        alert.dismiss();
        
        if(data === itemData.active.toString()){
        
          let msg : string;


          if(itemData.active.toString() === '1' ){
            msg = "El metodo de pago ya esta activo";
          }else{
            msg = "El metodo de pago ya esta desactivo";
          }
          
          let toas = self._toastCtrl.create({
            message:  msg,
            duration: 2000          
          });

          toas.present();

        }else{
        
          let paymentMethodDao = new PaymentMethodDao();
          
          let jsonData = {};

          if(data === '1'){
            jsonData['active'] = 'true';
          }else if(data === '0'){
            jsonData['active'] = 'false';
          }

          paymentMethodDao.update(itemData.id, jsonData , data =>{ 
              self._listData = new Array<any>();
              self._pullRequest = 0;
              self._makeRequestList(true,null);
          });


        }

        return false;
      }
    });
    alert.present();
  
  
  }//itempressed







  //getter setter
  public get searchQuery(){
    return this._searchQuery;
  }

  public set searchQuery(searchQueryParam : string){
    this._searchQuery = searchQueryParam;
  }

  public get listData(){
    return this._listData;
  }

  public get entityState(){
    return this._entityState
  }

  public set entityState( entityState : string){
    this._entityState = entityState;
  }




}
