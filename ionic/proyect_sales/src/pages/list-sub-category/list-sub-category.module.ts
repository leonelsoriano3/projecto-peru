import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListSubCategoryPage } from './list-sub-category';

@NgModule({
  declarations: [
    ListSubCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(ListSubCategoryPage),
  ],
  exports: [
    ListSubCategoryPage
  ]
})
export class ListSubCategoryPageModule {}
