import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListTransportPage } from './list-transport';

@NgModule({
  declarations: [
    ListTransportPage,
  ],
  imports: [
    IonicPageModule.forChild(ListTransportPage),
  ],
  exports: [
    ListTransportPage
  ]
})
export class ListTransportPageModule {}
