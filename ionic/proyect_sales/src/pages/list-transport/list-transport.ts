import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, 
  AlertController,ToastController } from 'ionic-angular';
import {CreateTransportPage} from '../create-transport/create-transport';
import {TransportDao} from '../../api/dao/transport-dao';


@IonicPage()
@Component({
  selector: 'page-list-transport',
  templateUrl: 'list-transport.html',
})
export class ListTransportPage {


  private _searchQuery: string;

  private _pullRequest : number;
  private _listData = new Array<any>();
  private _entityState : string;
  private _freeSending : string;



  constructor(private _nav: NavController, private _navParams: NavParams,
    private _alertCtrl: AlertController, private _toastCtrl: ToastController) {

    this._pullRequest = 0;
    this._makeRequestList(true,null);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListTransportPage');
  }


  /**
   *  de la busqueda por texto
   * @param evento de js
   */
  public valuechangeSearch(e):void{
    if(e.keyCode === 13){
      this._listData = new Array<any>();
      this._makeRequestList(true, null);
    }
    //console.log(this._searchQuery);
  }



  /**
   * llamo a el request globalizado
   * @param loader: boolean si se quiere hacer la peticion con el loader o no
   * @param refresher : any si se quiere llemar la petion con el refresher de la vista
  */
  private _makeRequestList(loader : boolean, refresher : any){

    let self : ListTransportPage = this;

    let transportDao : TransportDao = new TransportDao();

    let jsonData : any = {};



    if(this._searchQuery !== undefined &&
      this._searchQuery !== null && 
      this._searchQuery.length > 0){
      jsonData.textSearch = this._searchQuery;
    }

    jsonData.stateEntity = this._entityState;

    jsonData.isFreeEntity = this._freeSending;


  
    transportDao.getAll(loader,this._pullRequest ,jsonData ,function(data){
      if(data !== null){
        for(let i = 0; i < data.length; i++){
          self._listData.unshift(data[i]); 
        }
         
      }
      if(refresher !== null){
        refresher.complete();
      }
    });
  
  }//makeRequestList


  public onEntityStateSelect(event): void{
  
    this._pullRequest = 0;
    this._listData = new Array<any>();
    this._makeRequestList(true, null);

  }//end onEntityStateselect

  /**
  * evento de seleccion si es gratis o no el envio
  */
  public onEntityFreeSendingSeletect(event): void{
     this._pullRequest = 0;
    this._listData = new Array<any>();
    this._makeRequestList(true, null);    
  }


  public addResource(): void{


    let self : ListTransportPage = this;


    let paramPopCallback = function(_params) {
       return new Promise((resolve, reject) => {
              if(_params){
                  self._listData = new Array<any>();
                  self._pullRequest = 0;
                  self._makeRequestList(true,null);
              } 
              resolve();
           });
   }

    this._nav.push(CreateTransportPage,{
      callback : paramPopCallback
    }); 


  }

  /**
  * metodo del refres de ionic
  * @param refresher es el refresher inyectado de ionic 
  */
  public doRefresh(refresher){
    this._pullRequest++;
    this._makeRequestList(false,refresher);      
  }


  /**
   * click en la lista
   */ 
  itemPressed(itemData: any):void{

  
    let self : ListTransportPage = this;

    let alert = this._alertCtrl.create();
    alert.setTitle('Activa/Desactiva usuario');
    alert.addInput({type: 'radio', label: 'Activar', value: '1', checked: true});
    alert.addInput({type: 'radio', label: 'Desactivar', value: '0'});
    alert.addButton('Cancelar');
    alert.addButton({
      text: 'OK',
      handler: data => {
        alert.dismiss();
        
        if(data === itemData.active.toString()){
        
          let msg : string;


          if(itemData.active.toString() === '1' ){
            msg = "El transporte ya esta activo";
          }else{
            msg = "El trasporte ya esta desactivo";
          }
          
          let toas = self._toastCtrl.create({
            message:  msg,
            duration: 2000          
          });

          toas.present();

        }else{
        
          let transportDao = new TransportDao();
          
          let jsonData = {};

          if(data === '1'){
            jsonData['active'] = 'true';
          }else if(data === '0'){
            jsonData['active'] = 'false';
          }

          transportDao.update(itemData.id, jsonData , data =>{ 
              self._listData = new Array<any>();
              self._pullRequest = 0;
              self._makeRequestList(true,null);
          });


        }

        return false;
      }
    });
    alert.present();
  
  
  }//itempressed



  itemTapped(itemData):void{
    let self : ListTransportPage = this;

    let paramPopCallback = function(_params) {
       return new Promise((resolve, reject) => {
              if(_params){
                  self._listData = new Array<any>();
                  self._pullRequest = 0;
                  self._makeRequestList(true,null);
              } 
              resolve();
           });
    }

    this._nav.push(CreateTransportPage, {
      data : itemData,
      callback : paramPopCallback
    });

  }


    //getter setter


  public get searchQuery(){
    return this._searchQuery;
  }

  public set searchQuery(searchQueryParam : string){
    this._searchQuery = searchQueryParam;
  }

  public get listData(){
    return this._listData;
  }

  public get entityState(){
    return this._entityState
  }

  public set entityState( entityState : string){
    this._entityState = entityState;
  }


  public get freeSending(){
    return this._freeSending;
  }

  public set freeSending(freeSending : string){
    this._freeSending = freeSending;
  }

}
