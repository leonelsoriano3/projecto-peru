import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, 
  AlertController,ToastController } from 'ionic-angular';
import {UserDao} from "../../api/dao/user-dao";
import { AdminCreateUserPage } from "../admin-create-user/admin-create-user";


@IonicPage()
@Component({
  selector: 'page-list-user',
  templateUrl: 'list-user.html',
})
export class ListUserPage {
  
  private _pullRequest : number;
  private _userDao : UserDao;
  private _listData = new Array<any>();
  private _userState : string;
  private _searchQuery: string;


  constructor(private _nav: NavController, public navParams: NavParams,
    private _alertCtrl: AlertController, private _toastCtrl: ToastController) {
    let self : ListUserPage = this;

    this._pullRequest = 0;
    this._userDao = new UserDao();

    this._makeRequestList(true, null);

  }

  ionViewDidLoad() {
    
  }

  /**
   * llamo a el recuest globalizado
   * @param loader: boolean si se quiere hacer la peticion con el loader o no
   * @param refresher : any si se quiere llemar la petion con el refresher de la vista
  */
  private _makeRequestList(loader : boolean, refresher : any){
    let self : ListUserPage = this;
    this._userDao.getAllUser(loader,this._pullRequest, this._userState,
      this._searchQuery , function(data){
      if(data !== null){
        for(let i = 0; i < data.length; i++){
          
          self._listData.unshift(data[i]); 
        }
         
      }
      if(refresher !== null){
        refresher.complete();
      }
    });

  
  }//makeRequestList

  /**
   * metodo del refres de ionic
   * @param refresher es el refresher inyectado de ionic 
   */
  public doRefresh(refresher){
    this._pullRequest++;
    this._makeRequestList(false,refresher);      
  };

  public itemTapped(user: any):void{
   
    let self : ListUserPage = this;


    let paramPopCallback = function(_params) {
       return new Promise((resolve, reject) => {
              if(_params){
                  self._listData = new Array<any>();
                  self._pullRequest = 0;
                  self._makeRequestList(true,null);
              } 
              resolve();
           });
    }



    this._nav.push(AdminCreateUserPage, {
      data: user,
      callback : paramPopCallback
    });
  }

  public itemPressed(user: any):void{
     let self : ListUserPage = this;

    let alert = this._alertCtrl.create();
    alert.setTitle('Activa/Desactiva usuario');
    alert.addInput({type: 'radio', label: 'Activar', value: '1', checked: true});
    alert.addInput({type: 'radio', label: 'Desactivar', value: '0'});
    alert.addButton('Cancelar');
    alert.addButton({
      text: 'OK',
      handler: data => {
        alert.dismiss();
        
        console.log(user.active);
        console.log(data);
        if( parseInt(user.active) === parseInt(data)){
          let msg : string;

          if(parseInt(user.active) === 1 ){
            msg = "El usuario ya esta activo";
          }else{
            msg = "El usuario ya esta desactivo";
          }
          
          let toas = self._toastCtrl.create({
            message:  msg,
            duration: 2000          
          });

          toas.present();
        }else{
          //hacer peticion de desactivate
          
            let userDao : UserDao = new UserDao();
            let jsonData = {};
            jsonData['active'] = parseInt(data);

            userDao.deleteOne(user.id, jsonData , data =>{ 
              self._listData = new Array<any>();
              self._pullRequest = 0;
              self._makeRequestList(true,null);
            });
                   

        }

        return false;
      }
    });
    alert.present();
  }

  /**
   *  de la busqueda por texto
   * @param evento de js
   */
  public valuechangeSearch(e):void{
   
    if(e.keyCode === 13){
      this._listData = new Array<any>();
      this._makeRequestList(true, null);
    }
    //console.log(this._searchQuery);
  }

  /**
   * evento de agregar recursos
   */
  public addResource(): void{
    let self : ListUserPage = this;

    let paramPopCallback = function(_params) {
       return new Promise((resolve, reject) => {
              if(_params){
                  self._listData = new Array<any>();
                  self._pullRequest = 0;
                  self._makeRequestList(true,null);
              } 
              resolve();
           });
   }

    this._nav.push(AdminCreateUserPage,{
      callback : paramPopCallback
    }); 
  

  }

  public onUserStateSelect(event): void{
   
    this._pullRequest = 0;
    this._listData = new Array<any>();
    this._makeRequestList(true, null);

  }//end onUserStateSelect

  public get listData(){
    return this._listData;
  }

  public get userState(): string{
    return this._userState;
  }

  public set userState(userState : string){
    this._userState = userState;
  }

  public get searchQuery(){
    return this._searchQuery;
  }

  public set searchQuery(searchQueryParam : string){
    this._searchQuery = searchQueryParam;
  }

}
