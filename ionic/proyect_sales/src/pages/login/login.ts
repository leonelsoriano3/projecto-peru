import { Component } from '@angular/core';
import { LangCheker } from '../../injecter/lang-cheker'
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserDao} from "../../api/dao/user-dao";
import { NavController } from 'ionic-angular';
import {Session} from '../../injecter/session';
import {CreateUserPage} from '../create-user/create-user';
import {ValidateAccountPage} from '../validate-account/validate-account';
import {ForgotterPasswordPage} from '../forgotter-password/forgotter-password';
import {RecuperateAccountPage} from '../recuperate-account/recuperate-account';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [LangCheker]
})
export class LoginPage {
    
    private _loginForm: FormGroup;
    
    //si se a usado el boton de logeo
    private _isOnLogin : boolean = false;

    private _nav : NavController;
    
    private _lang: string;

    constructor(private _langCheker : LangCheker,
        private _fb: FormBuilder, private nav : NavController,) {
        
        this._nav = nav;

        new Session(this._nav).internalControlForLogin();

        
        let regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;


        if(localStorage.getItem('user.token') !== null ){

        }else{
            localStorage.removeItem('user.name');
            localStorage.removeItem('user.token');
            localStorage.removeItem('user.email');
            localStorage.removeItem('user.rol');
        }

        this._loginForm = this._fb.group({
            email: ['', [Validators.required, Validators.pattern(regexEmail)]],
            password: ['', [Validators.required, Validators.minLength(4)]],
        });
    }//end constructor


    ngAfterViewInit() { 
        this._lang = this._langCheker.getLang();
    }


    public onLenguajeSelect(e) : void{
        let seleted = e.replace(/\s/g, "");
        this._langCheker.setLang(seleted);
        this._lang = this._langCheker.getLang();

    }


    public onLogin() :void  {
        if(!this._isOnLogin && !this._loginForm.valid){
            this._isOnLogin = true;
        }else{
            let userDao : UserDao = new UserDao();
            userDao.doLogig(this._loginForm.value.email,
                this._loginForm.value.password, this._nav );
        }
    }

    public goCreateUser($event) : void {
        this._nav.setRoot(CreateUserPage);
 
    }

    public goValidateUser($event): void{
        this._nav.setRoot(ValidateAccountPage);
    }

    public goForgotterPassword($event):void{
        this._nav.setRoot(ForgotterPasswordPage);
    }

    public goRecuperateAccount($event): void{
        this._nav.setRoot(RecuperateAccountPage);
    }


    public get lang() : string{
        return this._lang;
    }

    public get flangLang() : string{
        return (this._lang === 'es')? 'pe' : 'us';
    }

    public get loginForm(): FormGroup{
        return this._loginForm;
    }

    public get isOnLogin(){
        return this._isOnLogin;
    }

}
