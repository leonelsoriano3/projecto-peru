import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, 
  AlertController,ToastController, ViewController } from 'ionic-angular';
import {ProductsDao} from "../../api/dao/product-dao";


@Component({
  selector: 'page-modal-associate',
  templateUrl: 'modal-associate.html',
})
export class ModalAssociatePage {

    private _listData = new Array<any>();
    private _searchQuery : string;
    private _productDao: ProductsDao;
    private _pullRequest : number;

  constructor(private _navCtrl: NavController, private _navParams: NavParams,
   private _viewCtrl: ViewController) {
   	this._productDao = new ProductsDao();
   	this._pullRequest = 0;

   	this._makeRequestList(true,null);
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ModalAssociatePage');
  }


    /**
   * llamo a el recuest globalizado
   * @param loader: boolean si se quiere hacer la peticion con el loader o no
   * @param refresher : any si se quiere llemar la petion con el refresher de la vista
  */
  private _makeRequestList(loader : boolean, refresher : any){

    let self : ModalAssociatePage = this;

    let jsonData : any = {};

    if(this._searchQuery !== undefined &&
      this._searchQuery !== null && 
      this._searchQuery.length > 0){
      jsonData["textSearch"] = this._searchQuery;
    }
console.log(this._searchQuery);
    this._productDao.getAll(loader,this._pullRequest ,jsonData ,function(data){
      //console.log(data);
      if(data !== null){
        for(let i = 0; i < data.length; i++){
        	data[i]["checked"] = false;
          self._listData.unshift(data[i]); 
        }
         
      }
      if(refresher !== null){
        refresher.complete();
      }
    });
  
  }//makeRequestList


  public acceptModal():void{
  	this._viewCtrl.dismiss(this._listData.filter((item)=>{ return item.checked  }));
  }


  /**
   *  de la busqueda por texto
   * @param evento de js
   */
  public valuechangeSearch(e):void{
    //console.log(e);
    console.log(this._listData);
    if(e.keyCode === 13){
      this._pullRequest = 0;
      this._listData = new Array<any>()
      this._makeRequestList(true,null);
    }
  }



  /**
   * metodo del refres de ionic
   * @param refresher es el refresher inyectado de ionic 
   */
  public doRefresh(refresher){
    this._pullRequest++;
    this._makeRequestList(false,refresher);      
  };



  public closeDialog(): void{
  	this._viewCtrl.dismiss();
  }


  public get searchQuery(){
  	return this._searchQuery;
  }

  public set searchQuery(searchQuery : string){
  	this._searchQuery = searchQuery;
  }

  public get listData(){
  	return this._listData;
  }
}
