import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ParameterDao} from '../../api/dao/parameter-dao';
//AIzaSyBG0ImbMlNm3c2mHBBMIxDPDcck6Lp0dOQ
@IonicPage()
@Component({
  selector: 'page-parameter',
  templateUrl: 'parameter.html',
})
export class ParameterPage {

  private _listData = new Array<any>();


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  
    this._makeRequestList(true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParameterPage');
  }

  /**
   * llamo a el request globalizado
   * @param loader: boolean si se quiere hacer la peticion con el loader o no
   * @param refresher : any si se quiere llemar la petion con el refresher de la vista
  */
  private _makeRequestList(loader : boolean){

    let self : ParameterPage = this;

    let parameterDao = new  ParameterDao();

  
    parameterDao.getAll(loader ,function(data){
      if(data !== null){
        for(let i = 0; i < data.length; i++){
          self._listData.unshift(data[i]); 
        }
        console.log(self._listData);
      } 
    });
  
  }//makeRequestList


  public onParamStateSelect(event, objectValue):void{
    console.log(event);
    console.log(objectValue);

    
    
    let parameterDao = new  ParameterDao();

    let jsonObject: any = {};

    //json para el backend
    jsonObject.active = (objectValue.active === 1)? 'true' : 'false';



    parameterDao.update(objectValue.id, jsonObject, function(){
    });
  
  }

  

  //getter setter
  
  public get listData(){
    return this._listData;
  }


}
