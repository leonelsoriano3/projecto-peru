import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Session } from '../../injecter/session';


/**
 * Generated class for the PrivacyPolicyPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-privacy-policy',
  templateUrl: 'privacy-policy.html',
})
export class PrivacyPolicyPage {



  constructor(private _navCtrl: NavController, private _navParams: NavParams) {
  }

  ionViewDidLoad() {
    
  }

  public onReject(event){
     this._navCtrl.setRoot(LoginPage);
      new Session(this._navCtrl).rejectPolicy();
  }

  public onAcept(event){
    new Session(this._navCtrl).acceptPolicy();    
  }

}
