import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, 
  AlertController,ToastController } from 'ionic-angular';
import {ProductsDao} from "../../api/dao/product-dao";
import { CreateProductPage } from "../create-product/create-product";
import {EndPointConfig} from "../../config/endpoint-config";


@IonicPage()
@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
})
export class ProductListPage {

  private _searchQuery: string;
  private _pullRequest : number;
  private _productDao : ProductsDao;
  private _listData = new Array<any>();


  constructor(private _nav: NavController, private _navParams: NavParams,
    private _alertCtrl: AlertController, private _toastCtrl: ToastController) {
    
    let self : ProductListPage = this;
    
    this._productDao = new ProductsDao();

    this._pullRequest = 0;

    this._makeRequestList(true,null);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductListPage page');
  }

  /**
   * metodo es llamado siempre al crear tammbien si la pagina es cacheada
   */
  ionViewDidEnter(){
    if(this._navParams.get('reload') !== undefined){
      this._productDao = new ProductsDao();
      this._pullRequest = 0;
      this._makeRequestList(true,null);
    }
  }



  public itemPressed(itemData: any):void{
    let self : ProductListPage = this;

    let alert = this._alertCtrl.create();
    alert.setTitle('Activa/Desactiva usuario');
    alert.addInput({type: 'radio', label: 'Activar', value: '1', checked: true});
    alert.addInput({type: 'radio', label: 'Desactivar', value: '0'});
    alert.addButton('Cancelar');
    alert.addButton({
      text: 'OK',
      handler: data => {
        alert.dismiss();
         
        if( parseInt(itemData.active) === parseInt(data)){
          let msg : string;

          if(parseInt(itemData.active) === 1 ){
            msg = "El producto ya esta activo";
          }else{
            msg = "El producto ya esta desactivo";
          }
          
          let toas = self._toastCtrl.create({
            message:  msg,
            duration: 2000          
          });

          toas.present();
        }else{
          //hacer peticion de desactivate
          let activeString : string = (data === '0')? 'false' : 'true';
          let jsonData = {active : activeString}
          self._productDao.update(itemData.id ,jsonData ,function(){
            self._listData = new Array<any>();
            self._pullRequest = 0;
            self._makeRequestList(true,null);

          });
        }

        return false;
      }
    });
    alert.present();
  }




  /**
   * metodo del refres de ionic
   * @param refresher es el refresher inyectado de ionic 
   */
  public doRefresh(refresher){
    this._pullRequest++;
    this._makeRequestList(false,refresher);      
  };

  /**
   * llamo a el recuest globalizado
   * @param loader: boolean si se quiere hacer la peticion con el loader o no
   * @param refresher : any si se quiere llemar la petion con el refresher de la vista
  */
  private _makeRequestList(loader : boolean, refresher : any){

    let self : ProductListPage = this;

    let jsonData : any = {};

    if(this._searchQuery !== undefined &&
      this._searchQuery !== null && 
      this._searchQuery.length > 0){
      jsonData["textSearch"] = this._searchQuery;
    }
  
    this._productDao.getAll(loader,this._pullRequest ,jsonData ,function(data){
      if(data !== null){
        for(let i = 0; i < data.length; i++){
          self._listData.unshift(data[i]); 
        }
         
      }
      if(refresher !== null){
        refresher.complete();
      }
    });
  
  }//makeRequestList



  /**
   *  de la busqueda por texto
   * @param evento de js
   */
  public valuechangeSearch(e):void{
    //console.log(e);
    if(e.keyCode === 13){
      this._pullRequest = 0;
      this._listData = new Array<any>()
      this._makeRequestList(true,null);
    }
  }


  public itemTapped(itemData: any): void{
   
    let self : ProductListPage = this;

    let paramPopCallback = function(_params) {
       return new Promise((resolve, reject) => {
              if(_params){
                  self._listData = new Array<any>();
                  self._pullRequest = 0;
                  self._makeRequestList(true,null);
              } 
              resolve();
           });
    }

    this._nav.push(CreateProductPage, {
      data : itemData,
      callback : paramPopCallback
    });

  }

  public addResource():void{

    let self : ProductListPage = this;


    let paramPopCallback = function(_params) {
       return new Promise((resolve, reject) => {
              if(_params){
                  self._listData = new Array<any>();
                  self._pullRequest = 0;
                  self._makeRequestList(true,null);
              } 
              resolve();
           });
   }

    this._nav.push(CreateProductPage,{
      callback : paramPopCallback
    }); 
  
  }

  public get searchQuery(){
    return this._searchQuery;
  }

  public set searchQuery(searchQueryParam : string){
    this._searchQuery = searchQueryParam;
  }

  public get listData(){
    return this._listData;
  }

  public get defaulImg(){
    return EndPointConfig.DEFAULT_IMG;
  }

  public get baseImg(){
    return EndPointConfig.BASE_IMG;
  }

}
