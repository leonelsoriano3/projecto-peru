import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromoModalPage } from './promo-modal';

@NgModule({
  declarations: [
    PromoModalPage,
  ],
  imports: [
    IonicPageModule.forChild(PromoModalPage),
  ],
  exports: [
    PromoModalPage
  ]
})
export class PromoModalPageModule {}
