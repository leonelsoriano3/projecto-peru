import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,
  ViewController} from 'ionic-angular';

import {AssociateDao} from '../../api/dao/asocciate-dao';


@IonicPage()
@Component({
  selector: 'page-promo-modal',
  templateUrl: 'promo-modal.html',
})
export class PromoModalPage {

  private _pullRequest : number;
  private _listData = new Array<any>();

  constructor(private _navCtrl: NavController, private _navParams: NavParams,
    private _viewCtrl :ViewController) {

    this._pullRequest = 0;
    this._makeRequestList(true,null);
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad PromoModalPage');
  }


  public closeDialog(): void{
  	this._viewCtrl.dismiss();
  }


  /**
   * llamo a el request globalizado
   * @param loader: boolean si se quiere hacer la peticion con el loader o no
   * @param refresher : any si se quiere llemar la petion con el refresher de la vista
  */
  private _makeRequestList(loader : boolean, refresher : any){

    let self : PromoModalPage = this;

    let asocciateDao : AssociateDao = new AssociateDao();

    let jsonData : any = {};



    /*if(this._searchQuery !== undefined &&
      this._searchQuery !== null && 
      this._searchQuery.length > 0){
      jsonData.textSearch = this._searchQuery;
    }

    jsonData.stateEntity = this._entityState;

    jsonData.isFreeEntity = this._freeSending;
    */

  
    asocciateDao.getAll(loader,this._pullRequest ,jsonData ,function(data){
      if(data !== null){
        for(let i = 0; i < data.length; i++){
          self._listData.unshift(data[i]); 
        }
         
      }
      if(refresher !== null){
        refresher.complete();
      }
    });
  
  }//makeRequestList


  //getter setter

  public get listData(){
    return this._listData;
  }



}
