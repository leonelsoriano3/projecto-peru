import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecuperateAccountPage } from './recuperate-account';

@NgModule({
  declarations: [
    RecuperateAccountPage,
  ],
  imports: [
    IonicPageModule.forChild(RecuperateAccountPage),
  ],
  exports: [
    RecuperateAccountPage
  ]
})
export class RecuperateAccountPageModule {}
