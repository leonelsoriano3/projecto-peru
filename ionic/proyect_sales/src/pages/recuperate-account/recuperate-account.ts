import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LangCheker } from '../../injecter/lang-cheker';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserDao} from "../../api/dao/user-dao";
import {LoginPage} from '../login/login';
import {RecuperateAccountDtoIn} from '../../api/dto/recuperate-account-dto-in'


@IonicPage()
@Component({
  selector: 'page-recuperate-account',
  templateUrl: 'recuperate-account.html',
  providers: [LangCheker]
})
export class RecuperateAccountPage {

  private _formGroup : FormGroup;

  constructor(private _langCheker : LangCheker,
      public navCtrl: NavController, public navParams: NavParams, 
      private _fb: FormBuilder) {

    let regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;



    this._formGroup = this._fb.group({
      email: ['', [Validators.required, Validators.pattern(regexEmail)]],
      token: ['',[Validators.required, Validators.maxLength(10)]],
      password: ['', [Validators.required, Validators.minLength(4)]],
      c_password: ['', [Validators.required, Validators.minLength(4)]]
    });

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad RecuperateAccountPage');
  }


  public onSubmitForm(): void{
      
    let self : RecuperateAccountPage  = this;

    let recuperateAccountDtoIn : RecuperateAccountDtoIn = new RecuperateAccountDtoIn();
    recuperateAccountDtoIn.email = this._formGroup.get('email').value;
    recuperateAccountDtoIn.token = this._formGroup.get('token').value;
    recuperateAccountDtoIn.cPassword = this._formGroup.get('c_password').value;
    recuperateAccountDtoIn.password = this._formGroup.get('password').value;

    let userDao : UserDao = new UserDao();
    userDao.recuperateAccount(recuperateAccountDtoIn, function(){
      self.goLogin();
    });
  }


  public goLogin(): void{
    this.navCtrl.setRoot(LoginPage);
  }

  public get formGroup(){
    return this._formGroup;
  }

}
