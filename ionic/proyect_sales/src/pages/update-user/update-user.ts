import { Component } from '@angular/core';
import { LangCheker } from '../../injecter/lang-cheker';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { FileChooser } from '@ionic-native/file-chooser';
import {RegionDao} from '../../api/dao/region-dao';
import {UserDao} from "../../api/dao/user-dao";

@IonicPage()
@Component({
  selector: 'page-update-user',
  templateUrl: 'update-user.html',
   providers: [LangCheker],
})
export class UpdateUserPage {

  private _formGroup : FormGroup;
  private _resquestRegion : RegionDao;
  private _regions : any[];
  private _provinces : any[];

  private _base64 : string;

	/**
	* si este valor se cambia el formulario sabe que sera una actualizacion
	*/
  private _id : number = null;

  private _typeImg : string;

  private _validImagen : boolean;


  private _defaultValues : any;


  //este es para cambiarle el nombre del seleccioanda al componete :D
  private _nameFileUpdate : string = null;
 
  private _email : string;

  constructor(private _langCheker : LangCheker, private _navCtrl: NavController, 
  		private _navParams: NavParams,   private _fb: FormBuilder, private _fileChooser: FileChooser) {

  	let self : UpdateUserPage = this;

	//verdadero porque la imagen no es required  
    this._validImagen = true;

	let regexDni = "^\\d{8}(\\-)\\d{1}$";
	let regexPhone = "^\\+[1-9]{1}[0-9]{3,14}$";

    this._formGroup = this._fb.group({
   
	    name : ['',[Validators.required,Validators.minLength(3),
	      Validators.maxLength(254)]],
	    surname: ['',[Validators.required,Validators.minLength(4),
	        Validators.maxLength(254)]],
	    password: ['', [Validators.required, Validators.minLength(4)]],
	    c_password: ['', [Validators.required, Validators.minLength(4)]],
	    documentType: ['',Validators.required],
	    numberIdentification: ['',[Validators.required, Validators.pattern(regexDni)]],
	    province: [{disabled: true},[Validators.required]],
	    region : [{disabled: true}, [Validators.required]],
	    distrite: ['',[Validators.required ,Validators.maxLength(254), Validators.minLength(3)]],
	    phone: ['',[Validators.required, Validators.pattern(regexPhone)] ],

    });


    this._formGroup.get("province").disable();
    this._resquestRegion = new RegionDao();
    this._resquestRegion.getAllRegion(function(data){
        self._regions = data;
        self._formGroup.get("region").enable();        
        
        let dao = new UserDao();
        dao.getUser(null, false, function(data){
          
          let internalRegionDao = new RegionDao();
          self._defaultValues = data;

          internalRegionDao.getRegionByProvinceId(
              data.province.regions_id, function(dataRegion){    
              self._provinces = dataRegion;
              self._formGroup.get("province").enable();
              self._formGroup.get('province').setValue(data.province.id);

              self._id = data.id;
              self._email = data.email;
              self._formGroup.get('name').setValue(data.name);
              self._formGroup.get('surname').setValue(data.sur_name);
              self._formGroup.get('documentType').setValue(data.type_identification);
              self._formGroup.get('numberIdentification').setValue(data.identification);
              
              self._formGroup.get('region').setValue(data.province.regions_id);
              self._formGroup.get('distrite').setValue(data.distrito);
              self._formGroup.get('phone').setValue(data.phone);

 
              self._nameFileUpdate  = data.img;
              if(self._nameFileUpdate !== undefined && self._nameFileUpdate !== null &&
               self._nameFileUpdate !== ''){
                self._validImagen = true;
              }


          });//end callback


        });
        
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateUserPage');
  }

  onProvinceSelect($event):void{
    let self : UpdateUserPage = this;

    if(this._formGroup.get("region").value !== null){

        self._formGroup.get("province").disable(); 

        this._resquestRegion.getRegionByProvinceId(
            this._formGroup.get("region").value, function(data){    
            self._provinces = data;
            self._formGroup.get("province").enable();    
        });
    }

  }


    /**
   * accion al hacer submit en en formulario
  */
  public onFormAction(): void{
    let self : UpdateUserPage = this;

    let jsonObject : any = {};

    let dao = new UserDao();

    if(this._formGroup.get('name').value !== this._defaultValues.name){
    	jsonObject.name = this._formGroup.get('name').value;
    }

    

    if(this._formGroup.get('surname').value !== this._defaultValues.sur_name){
    	jsonObject.sur_name = this._formGroup.get('surname').value;
    }


    
    if(typeof this._formGroup.get('province').value !== 'object' &&
       typeof this._formGroup.get('province').value !== this._defaultValues.province.id){
    	jsonObject.provinces_id = String(this._formGroup.get('province').value);
    }

    if(this._base64 !== null){
      jsonObject.img = this._base64;
    }

    if(this._typeImg !== null){
      jsonObject.typeImg = this._typeImg;
    }


    if(this._formGroup.get('distrite').value !== this._defaultValues.distrito){
      jsonObject.distrito = this._formGroup.get('distrite').value;
    }
    

    if(this._formGroup.get('numberIdentification').value !== undefined &&
    	this._formGroup.get('numberIdentification').value !== this._defaultValues.identification){
		jsonObject.identification = this._formGroup.get('numberIdentification').value;
    }
    

    if(this._formGroup.get('phone').value !== this._defaultValues.phone){
		jsonObject.phone = this._formGroup.get('phone').value;
    }
    

    if(typeof this._formGroup.get('province').value !== 'object' &&
      this._formGroup.get('documentType').value !== this._defaultValues.type_identification){
    	jsonObject.type_identification = this._formGroup.get('documentType').value;
    }

    if(this._formGroup.get('password').value !== '' &&
    	this._formGroup.get('password').value !== ''){
 		jsonObject.password = this._formGroup.get('password').value;
 		jsonObject.c_password = this._formGroup.get('c_password').value;
    }


	  dao.update(null, jsonObject, function(){
	    alert("actualizado");

	  });


  }//end onformaction 


    /**
  * valida que se repita la contraseña
  */
  public validatePassword(): boolean{
    return (this._formGroup.get('password').value.length > 0 ||
     this._formGroup.get('c_password').value.length) && this._formGroup.get('c_password').value !==
                    this._formGroup.get('password').value;
  }


    /**
   * evento de seleccion de imagen
  */
  public valuesFileChooser(event) : void{
    if(event.base64){
      this._base64 = event.base64;
      this._typeImg = event.typeImagen;
      this._validImagen = true;
    }else if(event.error){
       this._validImagen = false; 
    }
  }

  public get formGroup(): FormGroup{
        return this._formGroup;
  }
  public get regions(){
    return this._regions;
  }

  public get provinces(){
    return this._provinces;
  }

  public get email(){
    return this._email;
  }

  public get nameFileUpdate(){
    return this._nameFileUpdate;
  }

}
