import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPurchaseModalItemCartPage } from './user-purchase-modal-item-cart';

@NgModule({
  declarations: [
    UserPurchaseModalItemCartPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPurchaseModalItemCartPage),
  ],
  exports: [
    UserPurchaseModalItemCartPage
  ]
})
export class UserPurchaseModalItemCartPageModule {}
