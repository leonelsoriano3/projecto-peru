import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, 
  ViewController } from 'ionic-angular';

import {EndPointConfig} from "../../config/endpoint-config";

import  {PoolCart}  from "../../cart/pool-cart";
import  {EntityPoket}  from "../../cart/entity-poket";


@IonicPage()
@Component({
  selector: 'page-user-purchase-modal-item-cart',
  templateUrl: 'user-purchase-modal-item-cart.html',
})
export class UserPurchaseModalItemCartPage {

  private _dataParam : any;

  /**
  * total de la venta
  */
  private _total : number;

  private _poolCart : PoolCart;

  private _id : string;

  private _amount : number;



  constructor(public navCtrl: NavController, public _navParams: NavParams,
   private _viewCtrl :ViewController ) {
    this._dataParam = {};


    if(_navParams.get('id') && _navParams.get('pocketCart') !== undefined){
      //this._dataParam = _navParams.get('data');
      this._poolCart = _navParams.get('pocketCart');

      this._id = _navParams.get('id');

      let parseAmount : number = 0 ;
      if( _navParams.get('pocketCart') === null ){
        parseAmount = 0;
      }else{
       // this._amount =  parseFloat( )_navParams.get('pocketCart').amount;
      }//end _navaParam.get pocketCart




      //this._amount = this._poolCart.getById(this._id).amount;

      let itemParam = this._poolCart.getById(this._id);

      if(itemParam !== null){
        this._amount = itemParam.amount;
        this._dataParam = itemParam.item;
      }
      
      this._total = this._poolCart.getTotalItem( this._id , this._amount);
      
      
    }//end get params

  }



  ionViewDidLoad() {
    
  }


  public closeDialog(): void{
  	this._viewCtrl.dismiss();
  }



  public addItemCart():void{

    let itemCart = {
      amount : this._total,
      item : this._navParams.get('pocketCart').item
    };

    //this._navParams.get('pocketCart').add();
    this._navParams.get('pocketCart').add(new EntityPoket(this._dataParam, this._amount));

    this._viewCtrl.dismiss(
      itemCart
    );

  }

  /**
  * parsea y saca el resultado de la cantidad comprada
  */
  public totalPurchase() : void{

   /* let parseAmount =  parseFloat( this._amount.replace(',','.'));
    let parsePrice = 0;


    if(this._dataParam.price !== undefined && this._dataParam.price !== null &&
        this._dataParam.price.amount !== undefined && this._dataParam.price.amount !== null
      ){
      let priceStr = this._dataParam.price.amount.toString();
      parsePrice = parseFloat(priceStr.replace(',','.'));
    }

    if(isNaN(parsePrice)){
      parsePrice = 0;
    }
    if(isNaN(parseAmount)){
      parseAmount = 0;
    }

    console.log();
    let total =  (parsePrice * parseAmount).toFixed(2);
 
    this._total = total.toString();*/

    this._total = this._poolCart.getTotalItem(this._id , this._amount, false);
  }


  /**
  * resuelve el path de la imagen
  */
  public resolveImagenProduct(image : string) : string{
    return (image === undefined   || image === null || image === '')? 
      EndPointConfig.DEFAULT_IMG :
      EndPointConfig.BASE_IMG + image;
  }

  public get amount(){
    return this._amount;
  }

  public set amount(amount : number){
    this._amount = amount;
  }



  public get total(){
    return this._total;
  }


  public get dataParam(){
      return this._dataParam;
  }



}//end of class
