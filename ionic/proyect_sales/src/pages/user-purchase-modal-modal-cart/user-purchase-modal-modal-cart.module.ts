import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPurchaseModalModalCartPage } from './user-purchase-modal-modal-cart';

@NgModule({
  declarations: [
    UserPurchaseModalModalCartPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPurchaseModalModalCartPage),
  ],
  exports: [
    UserPurchaseModalModalCartPage
  ]
})
export class UserPurchaseModalModalCartPageModule {}
