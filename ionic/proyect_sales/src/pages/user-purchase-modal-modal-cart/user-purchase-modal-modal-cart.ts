import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,
 ViewController, ModalController} from 'ionic-angular';
import {EntityPoket} from '../../cart/entity-poket';

 import {UserPurchasePaymentPage} from '../user-purchase-payment/user-purchase-payment';

import {EndPointConfig} from "../../config/endpoint-config";

@IonicPage()
@Component({
  selector: 'page-user-purchase-modal-modal-cart',
  templateUrl: 'user-purchase-modal-modal-cart.html',
})
export class UserPurchaseModalModalCartPage {


  private _dataList : Array<EntityPoket>;

  constructor(public navCtrl: NavController, private _navParams: NavParams,
    private _viewCtrl: ViewController, private _modalCtrl: ModalController ) {

   //console.log(_navParams.get('poolCart').data);
    if(_navParams.get('poolCart') !== null){

      this._dataList = _navParams.get('poolCart').data;
    }
  }

  ionViewDidLoad(){}


  public closeDialog(): void{

    let returnValues = {
      data : this._dataList
    }
  	this._viewCtrl.dismiss(

    );
  }

  /**
  * evento que abre la vista de pago del carrito
  */
  public onClickPayment():void{
    let modal = this._modalCtrl.create(UserPurchasePaymentPage, {
      totalProduct : this.totalPurchase
    });
    modal.present();
  }


  public get dataList(){
    return this._dataList;
  }


  public get totalPurchase(){
    let value = 0;

    for (var i = 0; i < this._dataList.length; ++i) {
      value += this._dataList[i].amount * this._dataList[i].item.price.amount;
    }

    return Math.round( value * 100)/100;
  }


  /**
  * resuelve el path de la imagen
  */
  public resolveImagenProduct(image : string) : string{
    return (image === undefined   || image === null || image === '')? 
      EndPointConfig.DEFAULT_IMG :
      EndPointConfig.BASE_IMG + image;
  }

  public onDeleteItem(data: any){
  // console.log("init " +  this._dataList.length);
    for (var i = 0; i < this._dataList.length; ++i) {

      //console.log(data.item.id  +"  " + this._dataList[i].item.id) 
      if(data.item.id === this._dataList[i].item.id){
        this._dataList.splice(i,1);
         //console.log("index " + i);
      }
    }
  }


}
