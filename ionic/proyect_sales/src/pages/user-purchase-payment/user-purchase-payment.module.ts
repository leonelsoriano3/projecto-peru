import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPurchasePaymentPage } from './user-purchase-payment';

@NgModule({
  declarations: [
    UserPurchasePaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPurchasePaymentPage),
  ],
  exports: [
    UserPurchasePaymentPage
  ]
})
export class UserPurchasePaymentPageModule {}
