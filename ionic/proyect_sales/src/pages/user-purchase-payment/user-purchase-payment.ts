import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ParameterDao } from '../../api/dao/parameter-dao';
import { ManagerCache  } from '../../api/manager-cache';
import {  GlobalConfig } from '../../config/global-config';
import { PaymentMethodDao } from '../../api/dao/payment-method-dao';


@IonicPage()
@Component({
  selector: 'page-user-purchase-payment',
  templateUrl: 'user-purchase-payment.html',
})
export class UserPurchasePaymentPage {

  private _totalProduct : string;

  private _listDataParameter : Array<any>;
  private _listDataPaymentMethod : Array<any>;

  constructor(public navCtrl: NavController, public _navParams: NavParams,
  	private _viewCtrl: ViewController) {

    this._totalProduct = this._navParams.get('totalProduct');
    this._listDataParameter = new Array();
    this._listDataPaymentMethod = new Array();


    this._makeRequestListCategory();
    this._makeRequestListPaymentMethod();
  }

  ionViewDidLoad() {}


  /**
   * llamo a el request para llenar el combo de categorias
  */
  private _makeRequestListCategory(){

    let self : UserPurchasePaymentPage = this;

    let parameterDao : ParameterDao = new ParameterDao();

    let managerCache = new ManagerCache();

    let cacheData = managerCache.resolveCache(GlobalConfig.PARAMTER_TAG);

    if( cacheData !== null){

       self._listDataParameter = cacheData;

    }else{

      //true loader, funtion callback  
      parameterDao.getAll(false, function(data){
        if(data !== null){
          
          managerCache.addCache(GlobalConfig.PARAMTER_TAG, data);
           self._listDataParameter  = data;
          /*for(let i = 0; i < data.length; i++){

            self._listDataParameter.push(data[i]); 
          }  */ 
        }
        
      });

    }//end else
  
  }//makeRequestList



  /**
   * llamo a el request globalizado
  */
  private _makeRequestListPaymentMethod(){

    let self : UserPurchasePaymentPage = this;

    let  paymentMethodDao = new PaymentMethodDao();

    let jsonData : any = {};

    let managerCache = new ManagerCache();
    let cacheData = managerCache.resolveCache(GlobalConfig.PAYMENT_METOD_TAG);

    if(cacheData !== null){
       self._listDataPaymentMethod = cacheData;
    }else{

      paymentMethodDao.getAll(true, null ,jsonData ,function(data){

        if(data !== null){
          managerCache.addCache(GlobalConfig.PAYMENT_METOD_TAG, data);
           
          for(let i = 0; i < data.length; i++){

            self._listDataPaymentMethod.unshift(data[i]); 
          }
           
        }
       
      });

    }
  
  }//makeRequestListPaymentMethod




  public closeDialog(): void{
  	this._viewCtrl.dismiss();
  }

  public get sendPay(): number{
    if(this._listDataParameter !== undefined && this._listDataParameter !== null){
      //console.log(this._listDataParameter);
      //this._listDataParameter[0].active
      if(this._listDataParameter.length > 0){
        return this._listDataParameter[0].active;
      }else{
        return 0;
      }
      
    }else{
      return 0;
    }
  }

  public get totalProduct() : string {
    return this._totalProduct;
  }

  public get listDataPaymentMethod() : Array<any> {
    return this._listDataPaymentMethod;
  }




}
