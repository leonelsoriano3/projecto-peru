import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPurchasePophoverPage } from './user-purchase-pophover';

@NgModule({
  declarations: [
    UserPurchasePophoverPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPurchasePophoverPage),
  ],
  exports: [
    UserPurchasePophoverPage
  ]
})
export class UserPurchasePophoverPageModule {}
