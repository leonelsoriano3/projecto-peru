import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Events } from 'ionic-angular';
import {CategoryDao} from '../../api/dao/category-dao';
import {ManagerCache} from '../../api/manager-cache';
import {GlobalConfig} from '../../config/global-config';


@IonicPage()
@Component({
  selector: 'page-user-purchase-pophover',
  templateUrl: 'user-purchase-pophover.html',
})
export class UserPurchasePophoverPage {

  private _listDataCategory = new Array<any>();
  private _searchQuery: string;
  private _itemCategory: string;

  constructor(private _events : Events,public navCtrl: NavController, private _navParams: NavParams) {
  	this._searchQuery = "";
  	this._itemCategory = "";

    this._makeRequestListCategory();
  }

  ionViewDidLoad() {}


  /**
   * llamo a el request para llenar el combo de categorias
  */
  private _makeRequestListCategory(){

    let self : UserPurchasePophoverPage = this;

    let categoryDao : CategoryDao = new CategoryDao();

    let managerCache = new ManagerCache();

    let cacheData = managerCache.resolveCache(GlobalConfig.CATEGORY_TAG);

    if( cacheData !== null){

       self._listDataCategory = cacheData;

    }else{

      //true loader, null refresher, {} jsonData, funtion callback  
      categoryDao.getAll(true, null ,{} ,function(data){
        if(data !== null){
          
          managerCache.addCache(GlobalConfig.CATEGORY_TAG, data);

          for(let i = 0; i < data.length; i++){

            self._listDataCategory.push(data[i]); 
          }   
        }
        
      });

    }//end else
  
  }//makeRequestList







  /**
   *  de la busqueda por texto
   * @param evento de js
   */
  public valuechangeSearch(e):void{
  	
    if(e.keyCode === 13){
    	this._events.publish('filterPagePayment', this._getdata());
    }
  }

  public onStateStateSelect(e): void{
  	this._events.publish('filterPagePayment', this._getdata());
  }

  private _getdata(): any{
  	return {
  		searchQuery: this._searchQuery,
  		itemCategory : this._itemCategory
  	};
  }



  public get searchQuery() : string {
  	return this._searchQuery;
  }

  public set searchQuery(query : string) {
  	this._searchQuery = query;
  }

  public get itemCategory() : string {
  	return this._itemCategory;
  }

  public set itemCategory(itemCategory : string) {
  	this._itemCategory = itemCategory;
  }

  public get listDataCategory() :  Array<any>{
    return this._listDataCategory;
  }

}
