import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPurchasePage } from './user-purchase';

@NgModule({
  declarations: [
    UserPurchasePage,
  ],
  imports: [
    IonicPageModule.forChild(UserPurchasePage),
  ],
  exports: [
    UserPurchasePage
  ]
})
export class UserPurchasePageModule {}
