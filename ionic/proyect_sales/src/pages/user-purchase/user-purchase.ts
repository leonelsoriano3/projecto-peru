import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { UserPurchaseModalItemCartPage } from '../user-purchase-modal-item-cart/user-purchase-modal-item-cart';
import { UserPurchaseModalModalCartPage } from '../user-purchase-modal-modal-cart/user-purchase-modal-modal-cart';

import { UserPurchasePophoverPage  } from "../user-purchase-pophover/user-purchase-pophover";

import { ModalController } from 'ionic-angular';

import {ProductsDao} from "../../api/dao/product-dao";
import {EndPointConfig} from "../../config/endpoint-config";

import  {PoolCart}  from "../../cart/pool-cart";
import  {EntityPoket}  from "../../cart/entity-poket";

import { Events } from 'ionic-angular';


import { PromoModalPage  } from "../promo-modal/promo-modal";



@IonicPage()
@Component({
  selector: 'page-user-purchase',
  templateUrl: 'user-purchase.html',
})
export class UserPurchasePage {

  private _listData = new Array<any>();
  private _pullRequest : number;


  /**
  * store que se debe de guardar
  */
  private _poolCart : PoolCart


  private _cartBadge : number;

  private _searchQuery : string;

  private _itemCategory : string;


  constructor(private _events: Events, public navCtrl: NavController, public navParams: NavParams,
    private _popoverCtrl: PopoverController,
   public _modalCtrl: ModalController) {
    
    this._cartBadge = 0;
    this._itemCategory = null;

    this._listData = new Array<any>();

    this._poolCart =  new PoolCart();

    this._pullRequest = 0;
    this._makeRequestList(true,null);

  }

  ionViewDidLoad() {
//    console.log('ionViewDidLoad UserPurchasePage');
  }


  filterPopover(event): void{

    this._events.subscribe('filterPagePayment',(pophoverListData)=>{

       this._searchQuery = pophoverListData.searchQuery;
       this._itemCategory = pophoverListData.itemCategory;
       this._listData =  new Array<any>();
       this._pullRequest = 0;
       this._makeRequestList(true,null);

    });


    let popover = this._popoverCtrl.create(UserPurchasePophoverPage,{
      listData: this._listData
    });
      
      popover.present({
         ev: event
      });


      popover.onDidDismiss(filter => {
           this._events.unsubscribe('filterPagePayment');    
      });
  }


  /**
   * evento para el icon item del carrito
   */
  public onItemcart(item):void{

    this._poolCart.add(new EntityPoket(item));

    let modal = this._modalCtrl.create(UserPurchaseModalItemCartPage,
     { id : item.id , pocketCart : this._poolCart });

    modal.onDidDismiss(data => {
      this._cartBadge = this._poolCart.getTotalSelected();

    });


    modal.present();
  }



  public onDropItem(event):void{
    console.log("aca");
  }


  public onClickOpenCart():void{

    let modal = this._modalCtrl.create(UserPurchaseModalModalCartPage,{
      poolCart : this._poolCart
    });

    modal.onDidDismiss(data => {
      //this._poolCart.data = data;
      this._cartBadge = this._poolCart.getTotalSelected();
    });
    modal.present();
  }


  public onClickGoPromo(): void{
    

    let modal = this._modalCtrl.create(PromoModalPage,{
      poolCart : this._poolCart
    });

    modal.onDidDismiss(data => {
      //this._poolCart.data = data;
      this._cartBadge = this._poolCart.getTotalSelected();
    });
    modal.present();
    
  }




  /**
   * llamo a el request globalizado
   * @param loader: boolean si se quiere hacer la peticion con el loader o no
   * @param refresher : any si se quiere llemar la petion con el refresher de la vista
  */
  private _makeRequestList(loader : boolean, refresher : any){

    let self : UserPurchasePage = this;

    let productDao : ProductsDao = new ProductsDao();

    let jsonData : any = {};


    if(this._searchQuery !== undefined &&
      this._searchQuery !== null && 
      this._searchQuery.length > 0){
      jsonData.textSearch = this._searchQuery;
    }

    //jsonData.stateEntity = this._entityState;

    //jsonData.isFreeEntity = this._freeSending;

    jsonData.active = true;
    
    if(this._itemCategory !== undefined  && this._itemCategory !== null){
      jsonData.category = this._itemCategory;
    }


    productDao.getAll(loader,this._pullRequest ,jsonData ,function(data){
      if(data !== null){
        for(let i = 0; i < data.length; i++){
          
          self._listData.unshift(data[i]); 
        }
         
      }
      if(refresher !== null){
        refresher.complete();
      }
    });
  
  }//makeRequestList

  /**
  * resuelve el path de la imagen
  */
  public resolveImagenProduct(image : string) : string{
    return (image === undefined   || image === null || image === '')? 
      EndPointConfig.DEFAULT_IMG :
      EndPointConfig.BASE_IMG + image
  }




  /**
  * metodo del refres de ionic
  * @param refresher es el refresher inyectado de ionic 
  */
  public doRefresh(refresher){
    this._pullRequest++;
    this._makeRequestList(false,refresher);      
  }


  /**
  * consigue el item en el carrito dependdiendo su id esto es 
  * para saber que ya esta seleccionado
  */
  /*public getItemInPocketById(id : number){
    for (var i = 0; i < this._pocketCart.lenght; i++) {
      if(this._pocketCart[i].item.id === id ){
        return this._pocketCart[i];
      }
    }
    return null;
  }*/


  //getter setter
  
  public get listData(){
    return this._listData;
  }


  public get cartBadge(){

    return Math.round((this._cartBadge)* 100)/100;
  }

  

}
