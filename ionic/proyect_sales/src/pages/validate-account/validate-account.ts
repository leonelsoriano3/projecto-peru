import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LangCheker } from '../../injecter/lang-cheker';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivateAcountInDto} from '../../api/dto/activate-acount-in-dto'; 
import {UserDao} from "../../api/dao/user-dao";
import {LoginPage} from '../login/login';


/**
 * Generated class for the ValidateAccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-validate-account',
  templateUrl: 'validate-account.html',
  providers: [LangCheker]
})
export class ValidateAccountPage {

  private _formGroup : FormGroup;


  constructor(private _langCheker : LangCheker,
      public navCtrl: NavController, public navParams: NavParams, 
      private _fb: FormBuilder) {

        let regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        this._formGroup = this._fb.group({
            email: ['', [Validators.required, Validators.pattern(regexEmail)]],
            token: ['',[Validators.required, Validators.minLength(6)]]
        
        });

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ValidateAccountPage');
  }

  public onValidateUser(){
      let self : ValidateAccountPage = this;

      let activateAcountInDto : ActivateAcountInDto = new ActivateAcountInDto();
      activateAcountInDto.email = this._formGroup.get('email').value;
      activateAcountInDto.token = this._formGroup.get('token').value;
    
      let userDao : UserDao = new UserDao();
      userDao.validateUser(activateAcountInDto, function(){
        self.goLogin();
      });

  } 

  public goLogin(): void{
    this.navCtrl.setRoot(LoginPage);
  }

  public get formGroup(){
    return this._formGroup;
  }

}
