import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { FormControl,  ValidatorFn } from '@angular/forms';

@Injectable()
export class PriceValidatorServiceProvider {

  constructor() {
  }

  static len(len: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {

  let val: string = control.value;

  if (control.pristine || control.pristine) {
    return null;
  }


  if ( val.length !== 0 && val.replace(".","").length < len ) {
    return null;
  }


  return { 'len': true };
  }
}

}
